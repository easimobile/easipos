package com.easipos.easipos.activities.add_edit_product

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.activities.add_edit_product.mvp.AddEditProductPresenter
import com.easipos.easipos.activities.add_edit_product.mvp.AddEditProductView
import com.easipos.easipos.activities.add_edit_product.navigation.AddEditProductNavigation
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.bundle.RequestCode
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.fragments.alert_dialog.DeleteProductConfirmationAlertDialog
import com.easipos.easipos.models.Cart
import com.easipos.easipos.models.Category
import com.easipos.easipos.models.Product
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.util.getCurrency
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.visible
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_add_edit_product.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class AddEditProductActivity : CustomBaseAppCompatActivity(), AddEditProductView {

    companion object {
        fun newIntent(context: Context, productCategoryPair: ProductCategoryPair?): Intent {
            return Intent(context, AddEditProductActivity::class.java).apply {
                productCategoryPair?.let {
                    this.putExtra(ParcelData.PRODUCT_CATEGORY_PAIR, productCategoryPair)
                }
            }
        }
    }

    //region Variables
    private val navigation: AddEditProductNavigation by instance()
    private val validator: Validator by instance()

    private val presenter by lazy { AddEditProductPresenter(application) }

    private val productCategoryPair by argument<ProductCategoryPair>(ParcelData.PRODUCT_CATEGORY_PAIR)

    private var currentProduct: Product = Product.getEmpty()
    private var currentCategory: Category? = null
    private var isStockPriceSet = false
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RequestCode.SCAN_BARCODE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    // Replace current product's barcode
                    currentProduct.barcode = data.getStringExtra(ParcelData.BARCODE_VALUE) ?: ""

                    setBarcodeTextView()
                }
            }

            RequestCode.PRODUCT_CATEGORY -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    data.getParcelableExtra<Category>(ParcelData.CATEGORY)?.let { category ->
                        // Replace exiting category
                        currentCategory = category

                        setProductCategoryTextView()
                    }
                }
            }

            RequestCode.PRODUCT_STOCK_PRICE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    data.getParcelableExtra<Product>(ParcelData.PRODUCT)?.let { product ->
                        // Replace current product
                        currentProduct = product
                        isStockPriceSet = true

                        setProductStockPriceTextView()
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_add_edit_product

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun removeProductFromCart(product: Product) {
        Cart.removeProduct(product)
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_view_product_category.gone()
        card_view_delete.gone()

        text_view_product_stock_price.text = getString(R.string.label_product_stock_price_default,
            getCurrency())

        productCategoryPair?.let { productCategoryPair ->
            currentProduct = productCategoryPair.product
            currentCategory = productCategoryPair.category
            isStockPriceSet = true

            text_view_title.setText(R.string.title_edit_product)
            text_input_edit_text_product_code.setText(productCategoryPair.product.productCode)
            text_input_edit_text_product_name.setText(productCategoryPair.product.productName)

            card_view_delete.visible()
        } ?: run {
            text_view_title.setText(R.string.title_add_product)
        }

        setBarcodeTextView()
        setProductCategoryTextView()
        setProductStockPriceTextView()
    }

    private fun setupListeners() {
        image_view_close.click {
            finish()
        }

        button_scan.click {
            navigation.navigateToScanBarcode(this)
        }

        button_save.click {
            attemptSaveOrUpdateProduct()
        }

        layout_product_category.click {
            navigation.navigateToProductCategory(this)
        }

        layout_product_stock_price.click {
            navigation.navigateToAddEditProductStockPrice(this, currentProduct)
        }

        card_view_delete.click {
            deleteProduct()
        }
    }

    private fun setBarcodeTextView() {
        text_view_barcode.text = currentProduct.barcode
    }

    private fun setProductCategoryTextView() {
        currentCategory?.let { category ->
            text_view_product_category.visible()
            text_view_product_category.text = category.name
        }
    }

    private fun setProductStockPriceTextView() {
        currentProduct.let { product ->
            if (product.enableStockTracking) {
                text_view_product_stock_price.text = getString(R.string.label_product_stock_price_placeholder,
                    product.availableStock, product.formatSellingPrice(this))
            } else {
                text_view_product_stock_price.text = getString(R.string.label_product_stock_price_no_tracking_placeholder,
                    product.formatSellingPrice(this))
            }
        }
    }

    private fun attemptSaveOrUpdateProduct() {
        if (currentProduct.barcode.isBlank()) {
            showErrorAlertDialog(getString(R.string.error_barcode_required))
            return
        }

        if (currentCategory == null) {
            showErrorAlertDialog(getString(R.string.error_category_required))
            return
        }

        if (isStockPriceSet.not()) {
            showErrorAlertDialog(getString(R.string.error_stock_price_required))
            return
        }

        val productCodeValidation = Validation(text_input_layout_product_code)
            .add(NotBlankRule(R.string.error_field_required))
        val productNameValidation = Validation(text_input_layout_product_name)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val productCode = values[0].trim()
                val productName = values[1].trim()

                currentProduct.productCode = productCode
                currentProduct.productName = productName

                saveOrUpdateProduct()
            }
        }).validate(productCodeValidation, productNameValidation)
    }

    private fun saveOrUpdateProduct() {
        if (productCategoryPair == null) { // New product
            currentCategory?.let { category ->
                presenter.doAddProduct(currentProduct, category)
            }
        } else { // Existing product & maybe category
            currentCategory?.let { category ->
                presenter.doUpdateProductAndCategory(currentProduct, productCategoryPair!!, category)
            }
        }
    }

    private fun deleteProduct() {
        DeleteProductConfirmationAlertDialog(this) {
            presenter.doDeleteProduct(currentProduct)
        }.show()
    }
    //endregion
}
