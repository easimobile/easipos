package com.easipos.easipos.activities.add_edit_product

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.models.Product
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_add_edit_product_stock_price.*
import org.kodein.di.generic.instance

class AddEditProductStockPriceActivity : CustomBaseAppCompatActivity() {

    companion object {
        fun newIntent(context: Context, product: Product?): Intent {
            return Intent(context, AddEditProductStockPriceActivity::class.java).apply {
                product?.let {
                    this.putExtra(ParcelData.PRODUCT, product)
                }
            }
        }
    }

    //region Variables
    private val validator: Validator by instance()

    private val product by argument<Product>(ParcelData.PRODUCT)
    //endregion

    //region Lifecycle
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_add_edit_product_stock_price

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        product?.let { product ->
            text_input_edit_text_capital_price.setText(product.capitalPrice.formatAmount("##0.00"))
            text_input_edit_text_selling_price.setText(product.sellingPrice.formatAmount("##0.00"))
            text_input_edit_text_total_stock.setText(product.availableStock.toString())
            text_input_edit_text_minimum_stock.setText(product.minimumStock.toString())

            switch_manage_stock.isChecked = product.enableStockTracking
            text_input_layout_total_stock.isEnabled = product.enableStockTracking
            text_input_layout_minimum_stock.isEnabled = product.enableStockTracking
        } ?: run {
            text_input_layout_total_stock.isEnabled = false
            text_input_layout_minimum_stock.isEnabled = false
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_save.click {
            attemptSaveStockPrice()
        }

        switch_manage_stock.setOnCheckedChangeListener { _, isChecked ->
            text_input_layout_total_stock.isEnabled = isChecked
            text_input_layout_minimum_stock.isEnabled = isChecked
        }
    }

    private fun attemptSaveStockPrice() {
        product?.let { product ->
            when (val capitalPrice = text_input_edit_text_capital_price.getDoubleOrNull()) {
                null -> {
                    text_input_layout_capital_price.isErrorEnabled = true
                    text_input_layout_capital_price.error =
                        getString(R.string.error_capital_price_wrong_format)
                    return
                }
                else -> {
                    text_input_layout_capital_price.isErrorEnabled = false
                    product.capitalPrice = capitalPrice
                }
            }

            when (val sellingPrice = text_input_edit_text_selling_price.getDoubleOrNull()) {
                null -> {
                    text_input_layout_selling_price.isErrorEnabled = true
                    text_input_layout_selling_price.error =
                        getString(R.string.error_selling_price_wrong_format)
                    return
                }
                0.0 -> {
                    text_input_layout_selling_price.isErrorEnabled = true
                    text_input_layout_selling_price.error =
                        getString(R.string.error_selling_price_required)
                    return
                }
                else -> {
                    text_input_layout_selling_price.isErrorEnabled = false
                    product.sellingPrice = sellingPrice
                }
            }

            val enableStockTracking = switch_manage_stock.isChecked
            product.enableStockTracking = enableStockTracking

            if (enableStockTracking) {
                val totalStockValidation = Validation(text_input_layout_total_stock)
                    .add(NotBlankRule(R.string.error_field_required))
                val minimumStockValidation = Validation(text_input_layout_minimum_stock)
                    .add(NotBlankRule(R.string.error_field_required))

                validator.setListener(object : Validator.OnValidateListener {
                    override fun onValidateFailed(errors: List<String>) {
                    }

                    override fun onValidateSuccess(values: List<String>) {
                        val availableStock = values[0].toLongOrNull() ?: 0L
                        val minimumStock = values[1].toLongOrNull() ?: 0L

                        product.availableStock = availableStock
                        product.minimumStock = minimumStock

                        setResult(Activity.RESULT_OK, Intent().apply {
                            this.putExtra(ParcelData.PRODUCT, product)
                        })
                        finish()
                    }
                }).validate(totalStockValidation, minimumStockValidation)
            } else {
                product.availableStock = 0
                product.minimumStock = 0

                setResult(Activity.RESULT_OK, Intent().apply {
                    this.putExtra(ParcelData.PRODUCT, product)
                })
                finish()
            }
        }
    }
    //endregion
}
