package com.easipos.easipos.activities.add_edit_product.mvp

import android.app.Application
import com.easipos.easipos.R
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.Category
import com.easipos.easipos.models.Product
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.use_cases.base.DefaultObserver
import com.easipos.easipos.use_cases.sales.AddProductUseCase
import com.easipos.easipos.use_cases.sales.DeleteProductUseCase
import com.easipos.easipos.use_cases.sales.UpdateProductCategoryUseCase
import com.easipos.easipos.use_cases.sales.UpdateProductUseCase
import com.easipos.easipos.util.ErrorUtil

class AddEditProductPresenter(application: Application)
    : Presenter<AddEditProductView>(application) {

    private val addProductUseCase by lazy { AddProductUseCase(kodein) }
    private val updateProductUseCase by lazy { UpdateProductUseCase(kodein) }
    private val updateProductCategoryUseCase by lazy { UpdateProductCategoryUseCase(kodein) }
    private val deleteProductUseCase by lazy { DeleteProductUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        addProductUseCase.dispose()
        updateProductUseCase.dispose()
        updateProductCategoryUseCase.dispose()
        deleteProductUseCase.dispose()
    }

    fun doAddProduct(product: Product, category: Category) {
        val pair = ProductCategoryPair(product, category)
        addProductUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.toastMessage(R.string.prompt_product_added_successfully)
                view?.finishScreen()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, AddProductUseCase.Params.createQuery(pair))
    }

    fun doUpdateProductAndCategory(product: Product, productCategoryPair: ProductCategoryPair, newCategory: Category) {
        updateProductUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                if (productCategoryPair.category.categoryId != newCategory.categoryId) {
                    doUpdateProductCategory(productCategoryPair, newCategory)
                } else {
                    view?.toastMessage(R.string.prompt_product_updated_successfully)
                    view?.finishScreen()
                }
            }
        }, UpdateProductUseCase.Params.createQuery(product))
    }

    fun doDeleteProduct(product: Product) {
        deleteProductUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.toastMessage(R.string.prompt_product_deleted_successfully)
                view?.removeProductFromCart(product)
                view?.finishScreen()
            }
        }, DeleteProductUseCase.Params.createQuery(product))
    }

    private fun doUpdateProductCategory(productCategoryPair: ProductCategoryPair, newCategory: Category) {
        updateProductCategoryUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.toastMessage(R.string.prompt_product_updated_successfully)
                view?.finishScreen()
            }
        }, UpdateProductCategoryUseCase.Params.createQuery(productCategoryPair, newCategory))
    }
}
