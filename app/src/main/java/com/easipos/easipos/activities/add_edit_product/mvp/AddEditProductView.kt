package com.easipos.easipos.activities.add_edit_product.mvp

import com.easipos.easipos.base.View
import com.easipos.easipos.models.Product

interface AddEditProductView : View {

    fun removeProductFromCart(product: Product)

    fun finishScreen()
}
