package com.easipos.easipos.activities.add_edit_product.navigation

import android.app.Activity
import com.easipos.easipos.models.Product

interface AddEditProductNavigation {

    fun navigateToScanBarcode(activity: Activity)

    fun navigateToProductCategory(activity: Activity)

    fun navigateToAddEditProductStockPrice(activity: Activity, product: Product?)
}