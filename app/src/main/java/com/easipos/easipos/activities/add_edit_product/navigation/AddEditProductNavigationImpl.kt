package com.easipos.easipos.activities.add_edit_product.navigation

import android.app.Activity
import com.easipos.easipos.activities.add_edit_product.AddEditProductStockPriceActivity
import com.easipos.easipos.activities.product_category.ProductCategoryActivity
import com.easipos.easipos.activities.scan_barcode.ScanBarcodeActivity
import com.easipos.easipos.bundle.RequestCode
import com.easipos.easipos.models.Product

class AddEditProductNavigationImpl : AddEditProductNavigation {

    override fun navigateToScanBarcode(activity: Activity) {
        activity.startActivityForResult(ScanBarcodeActivity.newIntent(activity),
            RequestCode.SCAN_BARCODE)
    }

    override fun navigateToProductCategory(activity: Activity) {
        activity.startActivityForResult(ProductCategoryActivity.newIntent(activity),
            RequestCode.PRODUCT_CATEGORY)
    }

    override fun navigateToAddEditProductStockPrice(activity: Activity, product: Product?) {
        activity.startActivityForResult(AddEditProductStockPriceActivity.newIntent(activity, product),
            RequestCode.PRODUCT_STOCK_PRICE)
    }
}