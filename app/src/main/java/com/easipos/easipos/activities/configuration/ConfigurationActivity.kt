package com.easipos.easipos.activities.configuration

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.easipos.easipos.R
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.constant.TaxType
import com.easipos.easipos.constant.parseTaxType
import com.easipos.easipos.tools.Preference
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.findColor
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_configuration.*
import org.jetbrains.anko.selector
import org.kodein.di.generic.instance

class ConfigurationActivity : CustomBaseAppCompatActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ConfigurationActivity::class.java)
        }
    }

    //region Variables
    private val validator: Validator by instance()

    private val currency = mutableListOf<String>().apply {
        this.add("Singapore Dollar ($)")
        this.add("Malaysia Ringgit (RM)")
    }

    private var isTaxExclusive = false
    private var selectedTaxType = 0
    //endregion

    //region Lifecycle
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_configuration

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        when (Preference.prefCurrency) {
            "$" -> text_input_edit_text_currency.setText(currency[0])
            "RM" -> text_input_edit_text_currency.setText(currency[1])
        }

        isTaxExclusive = Preference.prefIsTaxExclusive
        updateTaxStatusView()

        selectedTaxType = when (parseTaxType(Preference.prefTaxType)) {
            TaxType.GST -> 0
            TaxType.SST -> 1
            else -> 0
        }
        updateTaxTypeView()

        text_input_edit_text_tax.setText(Preference.prefTaxAmount.toInt().toString())
        text_input_edit_text_receipt_header.setText(Preference.prefReceiptHeader)

        switch_float_on_cash_declaration.isChecked = Preference.prefFloatOnCashDeclaration
    }

    private fun setupListeners() {
        image_view_close.click {
            finish()
        }

        button_choose_currency.click {
            showCurrencySelector()
        }

        button_tax_exclusive.click {
            if (isTaxExclusive.not()) {
                isTaxExclusive = true

                updateTaxStatusView()
            }
        }

        button_tax_inclusive.click {
            if (isTaxExclusive) {
                isTaxExclusive = false

                updateTaxStatusView()
            }
        }

        button_tax_gst.click {
            if (selectedTaxType != 0) {
                selectedTaxType = 0

                updateTaxTypeView()
            }
        }

        button_tax_sst.click {
            if (selectedTaxType != 1) {
                selectedTaxType = 1

                updateTaxTypeView()
            }
        }

        button_set_tax.click {
            attemptSetTaxAmount()
        }

        switch_float_on_cash_declaration.setOnCheckedChangeListener { _, isChecked ->
            Preference.prefFloatOnCashDeclaration = isChecked
        }

        text_input_edit_text_receipt_header.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Preference.prefReceiptHeader = s?.toString() ?: ""
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })
    }

    private fun showCurrencySelector() {
        selector(
            title = getString(R.string.label_select_currency),
            items = currency,
            onClick = { _, index ->
                Preference.prefCurrency = when (index) {
                    0 -> "$"
                    1 -> "RM"
                    else -> ""
                }

                text_input_edit_text_currency.setText(currency[index])
            }
        )
    }

    private fun updateTaxStatusView() {
        if (isTaxExclusive) { // Exclusive Tab
            button_tax_exclusive.setBackgroundResource(R.drawable.bg_two_selection_first_selected)
            button_tax_exclusive.setTextColor(Color.WHITE)

            button_tax_inclusive.setBackgroundResource(R.drawable.bg_two_selection_second_unselected)
            button_tax_inclusive.setTextColor(findColor(R.color.colorPrimary))

            Preference.prefIsTaxExclusive = true
        } else  { // Inclusive Tab
            button_tax_exclusive.setBackgroundResource(R.drawable.bg_two_selection_first_unselected)
            button_tax_exclusive.setTextColor(findColor(R.color.colorPrimary))

            button_tax_inclusive.setBackgroundResource(R.drawable.bg_two_selection_second_selected)
            button_tax_inclusive.setTextColor(Color.WHITE)

            Preference.prefIsTaxExclusive = false
        }
    }

    private fun updateTaxTypeView() {
        if (selectedTaxType == 0) { // GST Tab
            button_tax_gst.setBackgroundResource(R.drawable.bg_two_selection_first_selected)
            button_tax_gst.setTextColor(Color.WHITE)

            button_tax_sst.setBackgroundResource(R.drawable.bg_two_selection_second_unselected)
            button_tax_sst.setTextColor(findColor(R.color.colorPrimary))

            Preference.prefTaxType = TaxType.GST.toString()
        } else if (selectedTaxType == 1) { // SST Tab
            button_tax_gst.setBackgroundResource(R.drawable.bg_two_selection_first_unselected)
            button_tax_gst.setTextColor(findColor(R.color.colorPrimary))

            button_tax_sst.setBackgroundResource(R.drawable.bg_two_selection_second_selected)
            button_tax_sst.setTextColor(Color.WHITE)

            Preference.prefTaxType = TaxType.SST.toString()
        }
    }

    private fun attemptSetTaxAmount() {
        val taxAmountValidation = Validation(text_input_layout_tax)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val taxAmount = values[0].trim().toDoubleOrNull() ?: 0.0

                Preference.prefTaxAmount = taxAmount
            }
        }).validate(taxAmountValidation)
    }
    //endregion
}
