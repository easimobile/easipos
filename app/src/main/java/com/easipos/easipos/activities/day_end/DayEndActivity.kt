package com.easipos.easipos.activities.day_end

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.activities.day_end.mvp.DayEndPresenter
import com.easipos.easipos.activities.day_end.mvp.DayEndView
import com.easipos.easipos.activities.day_end.navigation.DayEndNavigation
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.RequestCode
import com.easipos.easipos.constant.PosLogStatus
import com.easipos.easipos.constant.PosLogType
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.fragments.alert_dialog.YesNoAlertDialog
import com.easipos.easipos.fragments.dialog_fragment.CashDeclarationDialogFragment
import com.easipos.easipos.fragments.dialog_fragment.OpenShiftDialogFragment
import com.easipos.easipos.models.DrawerCashAmount
import com.easipos.easipos.models.PosLog
import com.easipos.easipos.tools.Preference
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.TAG
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_day_end.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class DayEndActivity : CustomBaseAppCompatActivity(), DayEndView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, DayEndActivity::class.java)
        }
    }

    //region Variables
    private val navigation: DayEndNavigation by instance()

    private val presenter by lazy { DayEndPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCode.DAY_END_REPORT && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_day_end

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun showCashDeclaration(drawerCashAmount: DrawerCashAmount) {
        Logger.d(drawerCashAmount)
        val fragment = CashDeclarationDialogFragment.newInstance(drawerCashAmount)
        fragment.show(supportFragmentManager, CashDeclarationDialogFragment.TAG)
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    fun performOpenShift(currentFloat: Double) {
        Preference.prefCurrentFloat = currentFloat
        Preference.prefCurrentShift += 1
        Preference.prefCurrentShiftStatus = PosLogStatus.SHIFT_OPEN.string

        val floatLog = PosLog(
            type = PosLogType.ADD_FLOAT.toString(),
            shift = Preference.prefCurrentShift,
            amount = currentFloat
        )

        presenter.doAddLog(floatLog) {
            presenter.doAddOpenShiftLog(Preference.prefCurrentShift) {
                navigation.navigateToSales(this)
            }
        }
    }

    fun completeCashDeclaration(drawerAmount: Double, status: String) {
        Preference.prefIsCashDeclarationDone = true

        val cashDeclarationLog = PosLog(
            type = PosLogType.CASH_DECLARATION.toString(),
            shift = Preference.prefCurrentShift,
            amount = drawerAmount,
            status = status
        )

        presenter.doAddLog(cashDeclarationLog) {
            setCashDeclarationStatusTextView()
        }
    }

    private fun setupViews() {
        setShiftStatusTextView()
        setCashDeclarationStatusTextView()
    }

    private fun setupListeners() {
        image_view_close.click {
            finish()
        }

        card_view_open_close_shift.click {
            performOpenCloseShift()
        }

        card_view_cash_declaration.click {
            performCashDeclaration()
        }

        card_view_day_end.click {
            performDayEnd()
        }
    }

    private fun setShiftStatusTextView() {
        text_view_shift_status.text = getString(R.string.label_shift_status,
            Preference.prefCurrentShift, Preference.prefCurrentShiftStatus)
    }

    private fun setCashDeclarationStatusTextView() {
        text_view_cash_declaration_status.text = getString(R.string.label_cash_declaration_status,
            if (Preference.prefIsCashDeclarationDone) getString(R.string.label_completed)
            else getString(R.string.label_not_completed)
        )
    }

    private fun performOpenCloseShift() {
        if (Preference.prefCurrentShiftStatus == PosLogStatus.SHIFT_OPEN.string) {
            closeShift()
        } else {
            openShift()
        }
    }

    private fun openShift() {
        if (Preference.prefDayStartBusinessDate == 0L) {
            showErrorAlertDialog(getString(R.string.error_day_start_not_performed_open_shift))
            return
        }

        if (Preference.prefIsCashDeclarationDone.not()) {
            showErrorAlertDialog(getString(R.string.error_open_shift_cash_not_declare))
            return
        }

        val fragment = OpenShiftDialogFragment.newInstance()
        fragment.show(supportFragmentManager, OpenShiftDialogFragment.TAG)
    }

    private fun closeShift() {
        YesNoAlertDialog(this, getString(R.string.label_close_shift_alert), onYes = {
            Preference.prefCurrentShiftStatus = PosLogStatus.SHIFT_CLOSE.string

            presenter.doAddCloseShiftLog(Preference.prefCurrentShift) {
                navigation.navigateToShiftCloseReport(this)

                setShiftStatusTextView()
            }
        }, onNo = {
        }).show()
    }

    private fun performCashDeclaration() {
        if (Preference.prefCurrentShiftStatus == PosLogStatus.SHIFT_OPEN.string) {
            showErrorAlertDialog(getString(R.string.error_cash_declaration_shift_not_close))
            return
        }

       presenter.doGetTodaySalesCashAmount()
    }

    private fun performDayEnd() {
        if (Preference.prefCurrentShiftStatus == PosLogStatus.SHIFT_OPEN.string) {
            showErrorAlertDialog(getString(R.string.error_day_end_shift_not_close))
            return
        }

        if (Preference.prefIsCashDeclarationDone.not()) {
            showErrorAlertDialog(getString(R.string.error_day_end_cash_not_declare))
            return
        }

        navigation.navigateToDayEndReport(this)
    }
    //endregion
}
