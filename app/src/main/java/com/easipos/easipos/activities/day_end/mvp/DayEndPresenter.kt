package com.easipos.easipos.activities.day_end.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.DrawerCashAmount
import com.easipos.easipos.models.PosLog
import com.easipos.easipos.use_cases.base.DefaultObserver
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.log.AddCloseShiftLogUseCase
import com.easipos.easipos.use_cases.log.AddLogUseCase
import com.easipos.easipos.use_cases.log.AddOpenShiftLogUseCase
import com.easipos.easipos.use_cases.sales.GetTotalDrawerCashAmountByShift

class DayEndPresenter(application: Application)
    : Presenter<DayEndView>(application) {

    private val addLogUseCase by lazy { AddLogUseCase(kodein) }
    private val addOpenShiftLogUseCase by lazy { AddOpenShiftLogUseCase(kodein) }
    private val addCloseShiftLogUseCase by lazy { AddCloseShiftLogUseCase(kodein) }
    private val getTotalDrawerCashAmountByShift by lazy { GetTotalDrawerCashAmountByShift(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        addLogUseCase.dispose()
        addOpenShiftLogUseCase.dispose()
        addCloseShiftLogUseCase.dispose()
        getTotalDrawerCashAmountByShift.dispose()
    }

    fun doAddLog(posLog: PosLog, after: () -> Unit) {
        addLogUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                after()
            }
        }, AddLogUseCase.Params.createQuery(posLog))
    }

    fun doAddOpenShiftLog(shift: Int, after: () -> Unit) {
        addOpenShiftLogUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                after()
            }
        }, AddOpenShiftLogUseCase.Params.createQuery(shift))
    }

    fun doAddCloseShiftLog(shift: Int, after: () -> Unit) {
        addCloseShiftLogUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                after()
            }
        }, AddCloseShiftLogUseCase.Params.createQuery(shift))
    }

    fun doGetTodaySalesCashAmount() {
        getTotalDrawerCashAmountByShift.execute(object : DefaultSingleObserver<DrawerCashAmount>() {
            override fun onSuccess(value: DrawerCashAmount) {
                view?.showCashDeclaration(value)
            }
        }, GetTotalDrawerCashAmountByShift.Params.createQuery())
    }
}
