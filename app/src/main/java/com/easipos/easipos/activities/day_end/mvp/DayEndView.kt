package com.easipos.easipos.activities.day_end.mvp

import com.easipos.easipos.base.View
import com.easipos.easipos.models.DrawerCashAmount

interface DayEndView : View {

    fun showCashDeclaration(drawerCashAmount: DrawerCashAmount)
}
