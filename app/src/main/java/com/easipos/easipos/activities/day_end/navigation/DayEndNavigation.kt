package com.easipos.easipos.activities.day_end.navigation

import android.app.Activity

interface DayEndNavigation {

    fun navigateToSales(activity: Activity)

    fun navigateToShiftCloseReport(activity: Activity)

    fun navigateToDayEndReport(activity: Activity)
}