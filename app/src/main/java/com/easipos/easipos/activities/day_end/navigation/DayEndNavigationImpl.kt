package com.easipos.easipos.activities.day_end.navigation

import android.app.Activity
import com.easipos.easipos.activities.day_end_report.DayEndReportActivity
import com.easipos.easipos.activities.sales.SalesActivity
import com.easipos.easipos.activities.shift_close_report.ShiftCloseReportActivity
import com.easipos.easipos.bundle.RequestCode

class DayEndNavigationImpl : DayEndNavigation {

    override fun navigateToSales(activity: Activity) {
        activity.startActivity(SalesActivity.newIntent(activity))
        activity.finish()
    }

    override fun navigateToShiftCloseReport(activity: Activity) {
        activity.startActivity(ShiftCloseReportActivity.newIntent(activity))
    }

    override fun navigateToDayEndReport(activity: Activity) {
        activity.startActivityForResult(DayEndReportActivity.newIntent(activity),
            RequestCode.DAY_END_REPORT)
    }
}