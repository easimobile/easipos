package com.easipos.easipos.activities.day_end_report

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import com.easipos.easipos.BuildConfig
import com.easipos.easipos.R
import com.easipos.easipos.activities.day_end_report.mvp.DayEndReportPresenter
import com.easipos.easipos.activities.day_end_report.mvp.DayEndReportView
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.models.DayEndReport
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.util.saveCsv
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import de.siegmar.fastcsv.writer.CsvWriter
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_day_end_report.*
import org.jetbrains.anko.longToast

class DayEndReportActivity : CustomBaseAppCompatActivity(), DayEndReportView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, DayEndReportActivity::class.java)
        }
    }

    //endregion
    private val presenter by lazy { DayEndReportPresenter(application) }

    private var dayEndReport: DayEndReport? = null
    //region Variables

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_day_end_report

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()

        presenter.getDayEndReport()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun populateDayEndReport(report: DayEndReport) {
        dayEndReport = report

        val dashedLine = "-----------------------------------"

        var cashDeclarationString = ""
        report.cashCollections.forEach { cashCollection ->
            cashDeclarationString += "Cash    " +
                    "${cashCollection.shift}    " +
                    "${formatSpacing(6, cashCollection.formatTotalCollect())}\t\t" +
                    "${formatSpacing(6, cashCollection.formatDeclare())}\t\t" +
                    "${formatSpacing(6, cashCollection.formatDiff())}\n"
        }

        val reportInString = "" +
                Preference.prefReceiptHeader +
                "\n" +
                "\n" +
                "Business Date: ${report.formatBusinessDate()}\n" +
                "\n" +
                "Day End: ${report.formatDayEndDate()}\n" +
                "\n" +
                "$dashedLine\n" +
                "Gross Sales        +   ${report.getPaymentsSize()}${getSpacing(report.getPaymentsSize(), report.getGrossSalesAmount())}${report.getGrossSalesAmount()}\n" +
                "$dashedLine\n" +
                "Net Sales          +   ${report.getPaymentsSize()}${getSpacing(report.getPaymentsSize(), report.getNetSalesAmount())}${report.getNetSalesAmount()}\n" +
                "$dashedLine\n" +
                "Cash               +   ${report.getCashNumber()}${getSpacing(report.getCashNumber(), report.getCashAmount())}${report.getCashAmount()}\n" +
                "Grab Pay           +   ${report.getGrabPayNumber()}${getSpacing(report.getGrabPayNumber(), report.getGrabPayAmount())}${report.getGrabPayAmount()}\n" +
                "Wechat Pay         +   ${report.getWechatPayNumber()}${getSpacing(report.getWechatPayNumber(), report.getWechatPayAmount())}${report.getWechatPayAmount()}\n" +
                "Alipay             +   ${report.getAlipayNumber()}${getSpacing(report.getAlipayNumber(), report.getAlipayAmount())}${report.getAlipayAmount()}\n" +
                "$dashedLine\n" +
                "Voucher            +   ${report.getVoucherNumber()}${getSpacing(report.getVoucherNumber(), report.getVoucherAmount())}${report.getVoucherAmount()}\n" +
                "$dashedLine\n" +
                "Float              +   ${report.numberOfShift}${getSpacing(report.numberOfShift, report.formatTotalFloatAmount())}${report.formatTotalFloatAmount()}\n" +
                "Cash Sales         +   ${report.getCashNumber()}${getSpacing(report.getCashNumber(), report.getCashAmount())}${report.getCashAmount()}\n" +
                "Cashless Sales     +   ${report.getCashlessNumber()}${getSpacing(report.getCashlessNumber(), report.getCashlessAmount())}${report.getCashlessAmount()}\n" +
                "$dashedLine\n" +
                "Cash In Drawer     ==  1${getSpacing(1, report.getCashInDrawer())}${report.getCashInDrawer()}\n" +
                "$dashedLine\n" +
                "\n" +
                "Paymt   Shf  Collect\tDeclare\t Diff\n" +
                "$dashedLine\n" +
                cashDeclarationString +
                "$dashedLine\n" +
                "\t\t\t\t${report.getNetCashDeclarationStatus()}\t\t\t\t${report.formatNetCashDeclaration()}\n" +
                "$dashedLine\n" +
                "\n" +
                "\n" +
                "\n" +
                "${report.formatReportGenerateDate()}\n" +
                "\n" +
                "\n" +
                "\n"

        text_view_report.text = reportInString
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_view_report.movementMethod = ScrollingMovementMethod()
    }

    private fun setupListeners() {
        image_view_back.click {
            onBackPressed()
        }

        button_share.click {
            attemptExportReport()
        }
    }

    private fun getSpacing(left: Int, right: String): String {
        val amountOfSpacing = 12 - left.toString().length - right.length
        return if (amountOfSpacing < 1) {
            ""
        } else {
            var spacing = ""
            repeat(amountOfSpacing) {
                spacing += " "
            }
            spacing
        }
    }

    private fun formatSpacing(availableSpace: Int, value: String): String {
        val length = value.length
        if (length < availableSpace) {
            var spacing = ""
            repeat(availableSpace - length) {
                spacing += " "
            }
            return "$spacing$value"
        }
        return value
    }

    private fun attemptExportReport() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    exportReport()
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    showErrorAlertDialog(getString(R.string.error_permission_write_storage))
                }
            }).check()
    }

    private fun exportReport() {
        dayEndReport?.let { report ->
            val dashedLine = "-----------------------------------"
            val csvWriter = CsvWriter()
            val data = arrayListOf<Array<String>>().apply {
                val receiptHeaders = Preference.prefReceiptHeader.split("\n")
                receiptHeaders.forEach { header ->
                    this.add(arrayOf(header))
                }
                this.add(arrayOf(""))
                this.add(arrayOf("Business Date: ${report.formatBusinessDate()}"))
                this.add(arrayOf(""))
                this.add(arrayOf("Day End: ${report.formatDayEndDate()}"))
                this.add(arrayOf(""))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Gross Sales", "+", report.getPaymentsSize().toString(), report.getGrossSalesAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Net Sales", "+", report.getPaymentsSize().toString(), report.getNetSalesAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Cash", "+", report.getCashNumber().toString(), report.getCashAmount()))
                this.add(arrayOf("Grab Pay", "+", report.getGrabPayNumber().toString(), report.getGrabPayAmount()))
                this.add(arrayOf("Wechat Pay", "+", report.getWechatPayNumber().toString(), report.getWechatPayAmount()))
                this.add(arrayOf("Alipay", "+", report.getAlipayNumber().toString(), report.getAlipayAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Voucher", "+", report.getVoucherNumber().toString(), report.getVoucherAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Float", "+", report.numberOfShift.toString(), report.formatTotalFloatAmount()))
                this.add(arrayOf("Cash Sales", "+", report.getCashNumber().toString(), report.getCashAmount()))
                this.add(arrayOf("Cashless Sales", "+", report.getCashlessNumber().toString(), report.getCashlessAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Cash In Drawer", "==", "1", report.getCashInDrawer()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf(""))
                this.add(arrayOf("Paymt", "Shf", "Collect", "Declare", "Diff"))
                this.add(arrayOf(dashedLine))
                report.cashCollections.forEach { cashCollection ->
                    this.add(arrayOf("Cash", cashCollection.shift.toString(), cashCollection.formatTotalCollect(), cashCollection.formatDeclare(), cashCollection.formatDiff()))
                }
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("\t\t\t\t${report.getNetCashDeclarationStatus()}\t\t\t\t${report.formatNetCashDeclaration()}"))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(report.formatReportGenerateDate()))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
            }

            try {
                saveCsv(
                    "day-end-${report.formatReportGenerateDate2()}.csv",
                    csvWriter,
                    data
                )?.let { uri ->
                    val newUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", uri.toFile())
                    val intent = Intent(Intent.ACTION_SEND).apply {
                        this.type = "text/csv"
                        this.putExtra(Intent.EXTRA_STREAM, newUri)
                        this.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    }

                    startActivity(Intent.createChooser(intent, "Share day end report"))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                showErrorAlertDialog(ex.localizedMessage?.toString() ?: "")
            }
        }
    }
    //endregion
}
