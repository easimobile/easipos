package com.easipos.easipos.activities.day_end_report.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.constant.PosLogStatus
import com.easipos.easipos.models.DayEndReport
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.use_cases.base.DefaultObserver
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.report.GetDayEndReportUseCase
import com.easipos.easipos.use_cases.sales.UpdateTodaySalesDayEndedUseCase

class DayEndReportPresenter(application: Application)
    : Presenter<DayEndReportView>(application) {

    private val getDayEndReportUseCase by lazy { GetDayEndReportUseCase(kodein) }
    private val updateTodaySalesDayEndedUseCase by lazy { UpdateTodaySalesDayEndedUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        getDayEndReportUseCase.dispose()
        updateTodaySalesDayEndedUseCase.dispose()
    }

    fun getDayEndReport() {
        getDayEndReportUseCase.execute(object : DefaultSingleObserver<DayEndReport>() {
            override fun onSuccess(value: DayEndReport) {
                view?.populateDayEndReport(value)

                doUpdateTodaySalesDayEnded()
            }
        }, GetDayEndReportUseCase.Params.createQuery())
    }

    fun doUpdateTodaySalesDayEnded() {
        updateTodaySalesDayEndedUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                Preference.prefDayStartBusinessDate = 0L
                Preference.prefCurrentFloat = 0.0
                Preference.prefCurrentShift = 0
                Preference.prefCurrentShiftStatus = PosLogStatus.SHIFT_OPEN.string
                Preference.prefIsCashDeclarationDone = false
            }
        }, UpdateTodaySalesDayEndedUseCase.Params.createQuery())
    }
}
