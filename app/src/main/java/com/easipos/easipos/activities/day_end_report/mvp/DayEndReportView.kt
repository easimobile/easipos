package com.easipos.easipos.activities.day_end_report.mvp

import com.easipos.easipos.base.View
import com.easipos.easipos.models.DayEndReport

interface DayEndReportView : View {

    fun populateDayEndReport(report: DayEndReport)
}
