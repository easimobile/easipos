package com.easipos.easipos.activities.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.activities.login.mvp.LoginPresenter
import com.easipos.easipos.activities.login.mvp.LoginView
import com.easipos.easipos.activities.login.navigation.LoginNavigation
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.md5
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import java.util.*

class LoginActivity : CustomBaseAppCompatActivity(), LoginView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    //region Variables
    private val navigation: LoginNavigation by instance()
    private val validator: Validator by instance()

    private val presenter by lazy { LoginPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_login

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {

    }

    private fun setupListeners() {
        button_login.click {
            attemptLogin()
        }

        button_register.click {
            navigation.navigateToRegister(this)
        }
    }

    private fun attemptLogin() {
        val usernameValidation = Validation(text_input_layout_username)
            .add(NotBlankRule(R.string.error_field_required))
        val passwordValidation = Validation(text_input_layout_password)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val username = values[0].trim().toLowerCase(Locale.getDefault())
                val password = values[1].trim().toLowerCase(Locale.getDefault())
                val hashedPassword = password.md5()

                presenter.doLogin(username, hashedPassword)
            }
        }).validate(usernameValidation, passwordValidation)
    }
    //endregion
}
