package com.easipos.easipos.activities.login.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.User
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.user.LoginUseCase
import com.easipos.easipos.util.ErrorUtil

class LoginPresenter(application: Application)
    : Presenter<LoginView>(application) {

    private val loginUseCase by lazy { LoginUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        loginUseCase.dispose()
    }

    fun doLogin(username: String, password: String) {
        view?.setLoadingIndicator(true)
        loginUseCase.execute(object : DefaultSingleObserver<User>() {
            override fun onSuccess(value: User) {
                view?.setLoadingIndicator(false)

                Preference.prefUserId = value.userId
                Preference.prefIsLoggedIn = true

                view?.navigateToMain()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, LoginUseCase.Params.createQuery(username, password))
    }
}
