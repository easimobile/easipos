package com.easipos.easipos.activities.login.mvp

import com.easipos.easipos.base.View

interface LoginView : View {

    fun navigateToMain()
}
