package com.easipos.easipos.activities.login.navigation

import android.app.Activity

interface LoginNavigation {

    fun navigateToMain(activity: Activity)

    fun navigateToRegister(activity: Activity)
}