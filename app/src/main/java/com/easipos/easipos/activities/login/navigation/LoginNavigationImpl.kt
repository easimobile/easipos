package com.easipos.easipos.activities.login.navigation

import android.app.Activity
import com.easipos.easipos.activities.main.MainActivity
import com.easipos.easipos.activities.register.RegisterActivity

class LoginNavigationImpl : LoginNavigation {

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }

    override fun navigateToRegister(activity: Activity) {
        activity.startActivity(RegisterActivity.newIntent(activity))
    }
}