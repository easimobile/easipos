package com.easipos.easipos.activities.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.activities.main.mvp.MainPresenter
import com.easipos.easipos.activities.main.mvp.MainView
import com.easipos.easipos.activities.main.navigation.MainNavigation
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.constant.PosLogStatus
import com.easipos.easipos.constant.PosLogType
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.fragments.dialog_fragment.DayStartDialogFragment
import com.easipos.easipos.managers.FcmManager
import com.easipos.easipos.managers.PushNotificationManager
import com.easipos.easipos.models.Cart
import com.easipos.easipos.models.PosLog
import com.easipos.easipos.tools.Preference
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.TAG
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.compareTwoDates
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import java.util.*

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    private val navigation: MainNavigation by instance()
    private val fcmManager: FcmManager by instance()
    private val pushNotificationManager: PushNotificationManager by instance()

    private val presenter by lazy { MainPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        registerPushTokenIfPossible()
        processPayload()
        tryAddDefaultCategory()
        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun navigateToLogin() {
        navigation.navigateToLogin(this)
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    fun processPayload() {
        Logger.d(pushNotificationManager.payload)
        pushNotificationManager.payload?.let { _ ->
//            navigateToNotification()
            pushNotificationManager.removePayload()
        }
    }

    fun performDayStart(businessDate: Calendar, currentFloat: Double) {
        presenter.doClearLog {
            Preference.prefDayStartBusinessDate = businessDate.timeInMillis
            Preference.prefCurrentFloat = currentFloat
            Preference.prefCurrentShift = 1
            Preference.prefCurrentShiftStatus = PosLogStatus.SHIFT_OPEN.string
            Preference.prefIsCashDeclarationDone = false

            val floatLog = PosLog(
                type = PosLogType.ADD_FLOAT.toString(),
                shift = 1,
                amount = currentFloat
            )

            presenter.doAddLog(floatLog) {
                presenter.doAddOpenShiftLog(1) {
                    navigation.navigateToSales(this)
                }
            }
        }
    }

    private fun registerPushTokenIfPossible() {
        fcmManager.service.registerFcmToken()
    }

    private fun tryAddDefaultCategory() {
        presenter.doAddDefaultCategory()
    }

    private fun setupViews() {
    }

    private fun setupListeners() {
        image_view_logout.click {
            presenter.doLogout()
        }

        card_view_open_business_date.click {
            openBusinessDate()
        }

        card_view_day_end.click {
            openDayEnd()
        }

        card_view_sales.click {
            openSales()
        }

        card_view_configuration.click {
            navigation.navigateToConfiguration(this)
        }

        card_view_transaction.click {
            navigation.navigateToTransaction(this)
        }

        card_view_discount.click {
            navigation.navigateToDiscount(this)
        }

        card_view_report.click {
            navigation.navigateToReport(this)
        }

        card_view_customer.click {
            navigation.navigateToCustomer(this)
        }
    }

    private fun openBusinessDate() {
        if (Preference.prefDayStartBusinessDate != 0L) {
            showErrorAlertDialog(getString(R.string.error_day_end_not_performed))
            return
        }

        val fragment = DayStartDialogFragment.newInstance()
        fragment.show(supportFragmentManager, DayStartDialogFragment.TAG)
    }

    private fun openDayEnd() {
        if (Preference.prefDayStartBusinessDate == 0L) {
            showErrorAlertDialog(getString(R.string.error_day_start_not_performed_day_end))
            return
        }

        navigation.navigateToDayEnd(this)
    }

    private fun openSales() {
        if (Preference.prefDayStartBusinessDate == 0L) {
            showErrorAlertDialog(getString(R.string.error_day_start_not_performed_sales))
            return
        }

        val businessDate = Calendar.getInstance().apply {
            this.timeInMillis = Preference.prefDayStartBusinessDate
        }.time
        val today = Calendar.getInstance().time

        if (compareTwoDates(today, businessDate) == 1) {
            showErrorAlertDialog(getString(R.string.error_after_business_date))
            return
        }

        if (Preference.prefCurrentShiftStatus == PosLogStatus.SHIFT_CLOSE.string) {
            showErrorAlertDialog(getString(R.string.error_shift_closed))
            return
        }

        // Clear cart before proceed to sales
        Cart.clearCart()
        navigation.navigateToSales(this)
    }

    //endregion
}
