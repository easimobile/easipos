package com.easipos.easipos.activities.main.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.PosLog
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.use_cases.base.DefaultObserver
import com.easipos.easipos.use_cases.log.AddLogUseCase
import com.easipos.easipos.use_cases.log.AddOpenShiftLogUseCase
import com.easipos.easipos.use_cases.log.ClearLogUseCase
import com.easipos.easipos.use_cases.sales.AddDefaultCategoryUseCase

class MainPresenter(application: Application)
    : Presenter<MainView>(application) {

    private val addDefaultCategoryUseCase by lazy { AddDefaultCategoryUseCase(kodein) }
    private val addLogUseCase by lazy { AddLogUseCase(kodein) }
    private val addOpenShiftLogUseCase by lazy { AddOpenShiftLogUseCase(kodein) }
    private val clearLogUseCase by lazy { ClearLogUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        addDefaultCategoryUseCase.dispose()
        addLogUseCase.dispose()
        addOpenShiftLogUseCase.dispose()
    }

    fun doLogout() {
        Preference.logout()
        view?.navigateToLogin()
    }

    fun doAddDefaultCategory() {
        addDefaultCategoryUseCase.execute(object : DefaultObserver<Void>() {
        }, AddDefaultCategoryUseCase.Params.createQuery())
    }

    fun doAddLog(posLog: PosLog, after: () -> Unit) {
        addLogUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                after()
            }
        }, AddLogUseCase.Params.createQuery(posLog))
    }

    fun doAddOpenShiftLog(shift: Int, after: () -> Unit) {
        addOpenShiftLogUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                after()
            }
        }, AddOpenShiftLogUseCase.Params.createQuery(shift))
    }

    fun doClearLog(after: () -> Unit) {
        clearLogUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                after()
            }
        }, ClearLogUseCase.Params.createQuery())
    }
}
