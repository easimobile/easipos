package com.easipos.easipos.activities.main.mvp

import com.easipos.easipos.base.View

interface MainView : View {

    fun navigateToLogin()
}
