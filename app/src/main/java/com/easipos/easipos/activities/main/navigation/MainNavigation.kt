package com.easipos.easipos.activities.main.navigation

import android.app.Activity

interface MainNavigation {

    fun navigateToLogin(activity: Activity)

    fun navigateToDayEnd(activity: Activity)

    fun navigateToSales(activity: Activity)

    fun navigateToConfiguration(activity: Activity)

    fun navigateToTransaction(activity: Activity)

    fun navigateToDiscount(activity: Activity)

    fun navigateToReport(activity: Activity)

    fun navigateToCustomer(activity: Activity)
}