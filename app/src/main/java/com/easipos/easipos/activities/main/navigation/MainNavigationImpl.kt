package com.easipos.easipos.activities.main.navigation

import android.app.Activity
import com.easipos.easipos.activities.configuration.ConfigurationActivity
import com.easipos.easipos.activities.day_end.DayEndActivity
import com.easipos.easipos.activities.login.LoginActivity
import com.easipos.easipos.activities.report.ReportActivity
import com.easipos.easipos.activities.sales.SalesActivity

class MainNavigationImpl : MainNavigation {

    override fun navigateToLogin(activity: Activity) {
        activity.startActivity(LoginActivity.newIntent(activity))
        activity.finishAffinity()
    }

    override fun navigateToDayEnd(activity: Activity) {
        activity.startActivity(DayEndActivity.newIntent(activity))
    }

    override fun navigateToSales(activity: Activity) {
        activity.startActivity(SalesActivity.newIntent(activity))
    }

    override fun navigateToConfiguration(activity: Activity) {
        activity.startActivity(ConfigurationActivity.newIntent(activity))
    }

    override fun navigateToTransaction(activity: Activity) {
    }

    override fun navigateToDiscount(activity: Activity) {
    }

    override fun navigateToReport(activity: Activity) {
        activity.startActivity(ReportActivity.newIntent(activity))
    }

    override fun navigateToCustomer(activity: Activity) {
    }
}