package com.easipos.easipos.activities.order_details

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easipos.R
import com.easipos.easipos.activities.order_details.mvp.OrderDetailsPresenter
import com.easipos.easipos.activities.order_details.mvp.OrderDetailsView
import com.easipos.easipos.activities.order_details.navigation.OrderDetailsNavigation
import com.easipos.easipos.adapters.ProductCartAdapter
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.RequestCode
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.fragments.alert_dialog.ProductCartAlertDialog
import com.easipos.easipos.models.Cart
import com.easipos.easipos.models.ProductCart
import com.easipos.easipos.tools.EqualSpacingItemDecoration
import com.easipos.easipos.tools.SwipeHelper
import com.easipos.easipos.tools.SwipeHelper.UnderlayButtonClickListener
import com.easipos.easipos.util.getCurrency
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.visible
import kotlinx.android.synthetic.main.activity_order_details.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class OrderDetailsActivity : CustomBaseAppCompatActivity(), OrderDetailsView,
    ProductCartAdapter.OnGestureDetectedListener {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, OrderDetailsActivity::class.java)
        }
    }

    //region Variables
    private val navigation: OrderDetailsNavigation by instance()

    private val presenter by lazy { OrderDetailsPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RequestCode.PAYMENT -> {
                if (resultCode == Activity.RESULT_OK) {
                    finish()
                }
            }
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_order_details

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(position: Int, item: ProductCart) {
        ProductCartAlertDialog(this, item, onRemove = {
            Cart.removeProductCart(position)
        }, onConfirm = { quantity ->
            Cart.changeProductCartQuantity(position, quantity)
        }).show()
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        // Observe changes
        Cart.cartLiveData.observe(this, Observer { cart ->
            populateCart(cart)
        })

        // Set initial data
        Cart.cartLiveData.value?.let { cart ->
            populateCart(cart)
        }

        setupRecyclerView()
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_add_product.click {
            finish()
        }

        card_view_clear_order.click {
            clearOrder()
        }

        button_pay.click {
            navigation.navigateToPayment(this)
        }
    }

    private fun setupRecyclerView() {
        val adapter = ProductCartAdapter(this, this)
        recycler_view_product_cart.apply {
            this.adapter = adapter
            this.layoutManager = LinearLayoutManager(this@OrderDetailsActivity)
            this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))
            object : SwipeHelper(this@OrderDetailsActivity, this) {
                override fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder?, underlayButtons: MutableList<UnderlayButton>?) {
                    underlayButtons?.add(UnderlayButton(
                        getString(R.string.action_remove),
                        Color.parseColor("#FF1A2C"),
                        UnderlayButtonClickListener { position ->
                            Cart.removeProductCart(position)
                        }
                    ))
                }
            }
        }
    }

    private fun populateCart(cart: Cart) {
        (recycler_view_product_cart.adapter as? ProductCartAdapter)?.let { adapter ->
            adapter.items = cart.products.toMutableList()
        }

        text_view_subtotal.text = getString(R.string.label_amount_placeholder, getCurrency(),  cart.getSubtotalSellingPrice().formatAmount())
        text_view_total_amount.text = getString(R.string.label_amount_placeholder, getCurrency(), cart.getTotalSellingPrice().formatAmount())

        if (cart.hasProducts()) {
            card_view_clear_order.visible()
        } else {
            card_view_clear_order.gone()
        }

        scrollView.post {
            scrollView.fullScroll(View.FOCUS_DOWN)
        }
    }

    private fun clearOrder() {
        Cart.clearCart()
    }
    //endregion
}
