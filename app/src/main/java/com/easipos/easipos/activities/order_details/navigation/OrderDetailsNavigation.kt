package com.easipos.easipos.activities.order_details.navigation

import android.app.Activity

interface OrderDetailsNavigation {

    fun navigateToPayment(activity: Activity)
}