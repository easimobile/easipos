package com.easipos.easipos.activities.order_details.navigation

import android.app.Activity
import com.easipos.easipos.activities.payment.PaymentActivity
import com.easipos.easipos.bundle.RequestCode

class OrderDetailsNavigationImpl : OrderDetailsNavigation {

    override fun navigateToPayment(activity: Activity) {
        activity.startActivityForResult(PaymentActivity.newIntent(activity),
            RequestCode.PAYMENT)
    }
}