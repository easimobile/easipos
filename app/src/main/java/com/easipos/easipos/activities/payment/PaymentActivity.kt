package com.easipos.easipos.activities.payment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.recyclerview.widget.GridLayoutManager
import com.easipos.easipos.R
import com.easipos.easipos.activities.payment.mvp.PaymentPresenter
import com.easipos.easipos.activities.payment.mvp.PaymentView
import com.easipos.easipos.activities.payment.navigation.PaymentNavigation
import com.easipos.easipos.adapters.DenominationSuggestionAdapter
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.RequestCode
import com.easipos.easipos.constant.CashlessType
import com.easipos.easipos.constant.PaymentMethod
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.models.Cart
import com.easipos.easipos.models.Payment
import com.easipos.easipos.models.Tax
import com.easipos.easipos.models.Voucher
import com.easipos.easipos.tools.GridSpacingItemDecoration
import com.easipos.easipos.util.getCurrency
import com.easipos.easipos.util.gson
import io.github.anderscheow.library.kotlinExt.*
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_payment.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import java.util.*
import kotlin.math.ceil

class PaymentActivity : CustomBaseAppCompatActivity(), PaymentView,
    DenominationSuggestionAdapter.OnGestureDetectedListener {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, PaymentActivity::class.java)
        }
    }

    //region Variables
    private val navigation: PaymentNavigation by instance()
    private val validator: Validator by instance()

    private val presenter by lazy { PaymentPresenter(application) }

    private var selectPaymentMethod = PaymentMethod.CASH
    private var selectCashlessType: CashlessType? = null
    private var currentVoucher: Voucher? = null
    private val currentTax = Tax.getTax()
    private var currentTaxAmount: Double = 0.0
    private var payableAmount: Double = 0.0
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RequestCode.PAYMENT_RESULT -> {
                if (resultCode == Activity.RESULT_OK) {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_payment

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun navigateToPaymentResult(payment: Payment) {
        setResult(Activity.RESULT_OK) // To finish Order Details screen
        navigation.navigateToPaymentResult(this, payment)
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(item: Double) {
        text_input_edit_text_money_receive.setText(item.formatAmount())
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        layout_cash.visible()
        layout_cashless.gone()
        image_view_grab_pay_tick.gone()
        image_view_wechat_pay_tick.gone()
        image_view_alipay_tick.gone()
        button_apply_voucher.visible()
        button_remove_voucher.gone()
        text_view_changes.gone()
        text_view_voucher_label.gone()
        text_view_voucher_amount.gone()

        text_input_edit_text_money_receive.setText("")
        text_view_tax_label.text = getString(R.string.label_tax_placeholder,
            currentTax.taxType, currentTax.percent.toInt())

        setupDenominationSuggestionRecyclerView()

        // Set initial data
        calculateAmount()
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        text_input_edit_text_money_receive.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                text_input_layout_money_receive.isErrorEnabled = false
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                calculateChanges()
            }
        })

        button_cash.click {
            if (selectPaymentMethod != PaymentMethod.CASH) {
                selectPaymentMethod(PaymentMethod.CASH)
            }
        }

        button_cashless.click {
            if (selectPaymentMethod != PaymentMethod.CASHLESS) {
                selectPaymentMethod(PaymentMethod.CASHLESS)
            }
        }

        button_grab_pay.click {
            if (selectCashlessType != CashlessType.GRAB_PAY) {
                selectCashlessType(CashlessType.GRAB_PAY)
            }
        }

        button_wechat_pay.click {
            if (selectCashlessType != CashlessType.WECHAT_PAY) {
                selectCashlessType(CashlessType.WECHAT_PAY)
            }
        }

        button_alipay.click {
            if (selectCashlessType != CashlessType.ALIPAY) {
                selectCashlessType(CashlessType.ALIPAY)
            }
        }

        button_apply_voucher.click {
            attemptApplyVoucher()
        }

        button_remove_voucher.click {
            removeVoucher()
        }

        button_pay.click {
            attemptMakePayment()
        }
    }

    private fun setupDenominationSuggestionRecyclerView() {
        recycler_view_denomination_suggestion.apply {
            this.adapter = DenominationSuggestionAdapter(this@PaymentActivity, this@PaymentActivity)
            this.layoutManager = GridLayoutManager(this@PaymentActivity, 2)
            this.addItemDecoration(GridSpacingItemDecoration(2, 16, false))
        }
    }

    private fun selectPaymentMethod(paymentMethod: PaymentMethod) {
        selectPaymentMethod = paymentMethod
        selectCashlessType = null

        text_input_layout_voucher.isErrorEnabled = false

        when (selectPaymentMethod) {
            PaymentMethod.CASH -> {
                button_cash.alpha = 1.0f
                button_cashless.alpha = 0.6f

                layout_cash.visible()
                layout_cashless.gone()
            }

            PaymentMethod.CASHLESS -> {
                button_cash.alpha = 0.6f
                button_cashless.alpha = 1.0f

                layout_cash.gone()
                layout_cashless.visible()
            }
        }
    }

    private fun selectCashlessType(cashlessType: CashlessType) {
        selectCashlessType = cashlessType

        selectCashlessType?.let { selectCashlessType ->
            when (selectCashlessType) {
                CashlessType.GRAB_PAY -> {
                    image_view_grab_pay_tick.visible()
                    image_view_wechat_pay_tick.gone()
                    image_view_alipay_tick.gone()
                }

                CashlessType.WECHAT_PAY -> {
                    image_view_grab_pay_tick.gone()
                    image_view_wechat_pay_tick.visible()
                    image_view_alipay_tick.gone()
                }

                CashlessType.ALIPAY -> {
                    image_view_grab_pay_tick.gone()
                    image_view_wechat_pay_tick.gone()
                    image_view_alipay_tick.visible()
                }
            }
        }
    }

    private fun attemptApplyVoucher() {
        val voucherValidation = Validation(text_input_layout_voucher)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val voucherCode = values[0].toUpperCase(Locale.getDefault()).trim()

                applyVoucher(voucherCode)
            }
        }).validate(voucherValidation)
    }

    private fun applyVoucher(voucherCode: String) {
        if (voucherCode == Voucher.voucher10.voucherCode) {
            currentVoucher = Voucher.voucher10

            text_input_layout_voucher.isEnabled = false
            text_input_edit_text_voucher.setText(getString(R.string.label_amount_placeholder,
                getCurrency(), currentVoucher?.voucherAmount?.formatAmount()))

            button_apply_voucher.gone()
            button_remove_voucher.visible()

            calculateAmount()
        } else {
            showErrorAlertDialog(getString(R.string.error_invalid_voucher)) {
                text_input_edit_text_voucher.setText("")
            }
        }
    }

    private fun removeVoucher() {
        currentVoucher = null

        text_input_layout_voucher.isEnabled = true
        text_input_edit_text_voucher.setText("")

        button_apply_voucher.visible()
        button_remove_voucher.gone()

        calculateAmount()
    }

    private fun calculateAmount() {
        Cart.cartLiveData.value?.let { cart ->
            currentTaxAmount = cart.getTotalTaxAmount()
            payableAmount = cart.getTotalPrice()

            currentVoucher?.let { voucher ->
                text_view_voucher_label.visible()
                text_view_voucher_amount.visible()
                text_view_voucher_amount.text = getString(R.string.label_negative_amount_placeholder,
                    getCurrency(), voucher.voucherAmount.formatAmount())

                if (voucher.voucherAmount > payableAmount) {
                    payableAmount = 0.0
                } else {
                    payableAmount -= voucher.voucherAmount
                }
            }

            text_view_subtotal_amount.text = getString(
                R.string.label_amount_placeholder,
                getCurrency(),
                cart.getSubtotalPrice().formatAmount()
            )
            text_view_tax_amount.text = getString(
                R.string.label_amount_placeholder,
                getCurrency(),
                currentTaxAmount.formatAmount()
            )
            text_view_payable_amount.text = getString(
                R.string.label_amount_placeholder,
                getCurrency(),
                payableAmount.formatAmount()
            )

            estimatePaidAmount()
            calculateChanges()
        }
    }

    private fun estimatePaidAmount() {
        val mainDenominations = listOf(1.0, 5.0, 10.0, 50.0, 100.0)
        val suggestions = ArrayList<Double>()

        if (payableAmount > 0.0) {
            if (mainDenominations.contains(payableAmount)) {
                mainDenominations.forEach { denomination ->
                    if (denomination >= payableAmount) {
                        addSuggestions(suggestions, denomination)
                    }
                }
            } else {
                addSuggestions(suggestions, payableAmount)

                var dollar = payableAmount.toLong()
                val cent = payableAmount - dollar

                if (cent > 0.0) {
                    val ceilMoney = ceil(payableAmount) // Round to the next positive integer
                    addSuggestions(suggestions, ceilMoney)

                    while (suggestions.size < 5) {
                        val nextSuggestion = dollar + 1
                        val nextSuggestionWithCent = nextSuggestion + cent
                        val difference = nextSuggestionWithCent - payableAmount

                        if (mainDenominations.contains(difference)) {
                            addSuggestions(suggestions, nextSuggestion.toDouble())
                            addSuggestions(suggestions, nextSuggestionWithCent)
                        }

                        dollar = nextSuggestion
                    }
                } else {
                    val lastTwoDigit = if (dollar < 100) dollar else dollar % 100
                    val difference = dollar - lastTwoDigit

                    when {
                        lastTwoDigit < 5 -> {
                            addSuggestions(suggestions, difference + 5.0)
                            addSuggestions(suggestions, difference + 10.0)
                            addSuggestions(suggestions, difference + 50.0)
                            addSuggestions(suggestions, difference + 100.0)
                        }
                        lastTwoDigit < 10 -> {
                            addSuggestions(suggestions, difference + 10.0)
                            addSuggestions(suggestions, difference + 50.0)
                            addSuggestions(suggestions, difference + 100.0)
                        }
                        lastTwoDigit < 50 -> {
                            addSuggestions(suggestions, difference + 50.0)
                            addSuggestions(suggestions, difference + 100.0)
                        }
                        lastTwoDigit < 100 -> {
                            addSuggestions(suggestions, difference + 100.0)
                        }
                    }
                }
            }
        }

        (recycler_view_denomination_suggestion.adapter as? DenominationSuggestionAdapter)?.items = suggestions.toMutableList()
    }

    private fun addSuggestions(suggestions: ArrayList<Double>, toAddSuggestion: Double) {
        if (suggestions.size < 5 && suggestions.contains(toAddSuggestion).not()) {
            suggestions.add(toAddSuggestion)
        }
    }

    private fun calculateChanges() {
        if (selectPaymentMethod == PaymentMethod.CASH) {
            val moneyReceive = text_input_edit_text_money_receive.getDoubleOrNull() ?: 0.0

            if (moneyReceive > payableAmount) {
                val diff = moneyReceive - payableAmount

                text_view_changes.visible()
                text_view_changes.text = getString(R.string.label_changes_placeholder,
                    getCurrency(), diff.formatAmount())
            } else {
                text_view_changes.gone()
            }
        }
    }

    private fun attemptMakePayment() {
        Cart.cartLiveData.value?.let { cart ->
            when (selectPaymentMethod) {
                PaymentMethod.CASH -> {
                    makeCashPayment(cart)
                }

                PaymentMethod.CASHLESS -> {
                    makeCashlessPayment(cart)
                }
            }
        }
    }

    private fun makeCashPayment(cart: Cart) {
        val moneyReceive = text_input_edit_text_money_receive.getDoubleOrNull()
        if (moneyReceive == null) {
            text_input_layout_money_receive.isErrorEnabled = true
            text_input_layout_money_receive.error =
                getString(R.string.error_money_receive_wrong_format)
            return
        }

        val payment = Payment(
            products = gson.toJson(cart.products),
            discount = cart.discount,
            customerInfo = cart.customerInfo,
            tax = currentTax,
            taxAmount = currentTaxAmount,
            subtotal = cart.getSubtotalPrice(),
            total = cart.getTotalPrice(),
            paymentMethod = selectPaymentMethod,
            cashlessType = selectCashlessType,
            payableAmount = payableAmount,
            amountPaid = moneyReceive,
            voucher = currentVoucher
        )

        if (payment.isAmountPaidLargerOrEqualTotalAmountWithTax()) {
            presenter.doPayment(payment)
        } else {
            showErrorAlertDialog(getString(R.string.error_money_receive_not_sufficient))
        }
    }

    private fun makeCashlessPayment(cart: Cart) {
        if (selectCashlessType == null) {
            showErrorAlertDialog(getString(R.string.error_cashless_type_required))
            return
        }

        val payment = Payment(
            products = gson.toJson(cart.products),
            discount = cart.discount,
            customerInfo = cart.customerInfo,
            tax = currentTax,
            taxAmount = currentTaxAmount,
            subtotal = cart.getSubtotalPrice(),
            total = cart.getTotalPrice(),
            paymentMethod = selectPaymentMethod,
            cashlessType = selectCashlessType,
            payableAmount = payableAmount,
            amountPaid = payableAmount,
            voucher = currentVoucher
        )

        presenter.doPayment(payment)
    }
    //endregion
}
