package com.easipos.easipos.activities.payment.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.Cart
import com.easipos.easipos.models.Payment
import com.easipos.easipos.use_cases.base.DefaultObserver
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.sales.AddPaymentUseCase
import com.easipos.easipos.use_cases.sales.UpdateProductsAvailableStockUseCase
import com.orhanobut.logger.Logger

class PaymentPresenter(application: Application)
    : Presenter<PaymentView>(application) {

    private val updateProductsAvailableStockUseCase by lazy { UpdateProductsAvailableStockUseCase(kodein) }
    private val addPaymentUseCase by lazy { AddPaymentUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        addPaymentUseCase.dispose()
        updateProductsAvailableStockUseCase.dispose()
    }

    fun doPayment(payment: Payment) {
        Logger.d(payment)
        view?.setLoadingIndicator(true)
        doAddPayment(payment)
        // Todo: Other payment stuffs
    }

    private fun doAddPayment(payment: Payment) {
        addPaymentUseCase.execute(object : DefaultSingleObserver<Long>() {
            override fun onSuccess(value: Long) {
                doUpdateProductsAvailableStock(payment)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
            }
        }, AddPaymentUseCase.Params.createQuery(payment))
    }

    private fun doUpdateProductsAvailableStock(payment: Payment) {
        updateProductsAvailableStockUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.setLoadingIndicator(false)

                Cart.clearCart()

                view?.navigateToPaymentResult(payment)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
            }
        }, UpdateProductsAvailableStockUseCase.Params.createQuery(payment.getProductCarts()))
    }
}
