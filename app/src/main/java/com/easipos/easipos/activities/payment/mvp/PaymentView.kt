package com.easipos.easipos.activities.payment.mvp

import com.easipos.easipos.base.View
import com.easipos.easipos.models.Payment

interface PaymentView : View {

    fun navigateToPaymentResult(payment: Payment)
}
