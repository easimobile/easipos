package com.easipos.easipos.activities.payment.navigation

import android.app.Activity
import com.easipos.easipos.models.Payment

interface PaymentNavigation {

    fun navigateToPaymentResult(activity: Activity, payment: Payment)
}