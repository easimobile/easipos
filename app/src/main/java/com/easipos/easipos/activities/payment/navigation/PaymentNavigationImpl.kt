package com.easipos.easipos.activities.payment.navigation

import android.app.Activity
import com.easipos.easipos.activities.payment_result.PaymentResultActivity
import com.easipos.easipos.models.Payment

class PaymentNavigationImpl : PaymentNavigation {

    override fun navigateToPaymentResult(activity: Activity, payment: Payment) {
        activity.startActivity(PaymentResultActivity.newIntent(activity, payment))
        activity.finish()
    }
}