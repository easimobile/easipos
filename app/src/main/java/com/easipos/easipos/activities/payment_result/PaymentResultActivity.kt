package com.easipos.easipos.activities.payment_result

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.activities.payment_result.mvp.PaymentResultPresenter
import com.easipos.easipos.activities.payment_result.mvp.PaymentResultView
import com.easipos.easipos.activities.payment_result.navigation.PaymentResultNavigation
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.models.Payment
import com.easipos.easipos.util.getCurrency
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.activity_payment_result.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import java.util.*

class PaymentResultActivity : CustomBaseAppCompatActivity(), PaymentResultView {

    companion object {
        fun newIntent(context: Context, payment: Payment): Intent {
            return Intent(context, PaymentResultActivity::class.java).apply {
                this.putExtra(ParcelData.PAYMENT, payment)
            }
        }
    }

    //region Variables
    private val navigation: PaymentResultNavigation by instance()

    private val presenter by lazy { PaymentResultPresenter(application) }

    private val payment by argument<Payment>(ParcelData.PAYMENT)
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_payment_result

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_view_changes.gone()

        payment?.let { payment ->
            text_view_total_paid.text = getString(R.string.label_amount_placeholder,
                getCurrency(),  payment.amountPaid.formatAmount())
            text_view_date.text = Calendar.getInstance().timeInMillis.formatDate("dd MMM yyyy")

            if (payment.isChangeRequired()) {
                text_view_changes.visible()
                text_view_changes.text = getString(R.string.label_changes_placeholder,
                    getCurrency(), payment.getChange().formatAmount())
            }
        }
    }

    private fun setupListeners() {
        image_view_close.click {
            finish()
        }

        button_done.click {
            finish()
        }

        button_send_receipt.click {
            payment?.let { payment ->
                navigation.navigateToPaymentShareReceipt(this, payment)
            }
        }

        button_print_receipt.click {
            payment?.let { payment ->
                navigation.navigateToPaymentPrintReceipt(this, payment)
            }
        }
    }
    //endregion
}
