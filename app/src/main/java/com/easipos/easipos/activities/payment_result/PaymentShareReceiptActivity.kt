package com.easipos.easipos.activities.payment_result

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Build
import android.os.Bundle
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.easipos.BuildConfig
import com.easipos.easipos.R
import com.easipos.easipos.adapters.ProductCartReceiptAdapter
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.constant.PaymentMethod
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.models.Payment
import com.easipos.easipos.tools.EqualSpacingItemDecoration
import com.easipos.easipos.util.getCurrency
import com.easipos.easipos.util.saveBitmap
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.activity_payment_share_receipt.*
import java.util.*

class PaymentShareReceiptActivity : CustomBaseAppCompatActivity() {

    companion object {
        fun newIntent(context: Context, payment: Payment): Intent {
            return Intent(context, PaymentShareReceiptActivity::class.java).apply {
                this.putExtra(ParcelData.PAYMENT, payment)
            }
        }
    }

    //region Variables
    private val payment by argument<Payment>(ParcelData.PAYMENT)
    //endregion

    //region Lifecycle
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_payment_share_receipt

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_view_voucher_label.gone()
        text_view_voucher.gone()
        text_view_changes_label.gone()
        text_view_changes.gone()
        text_view_transaction_date_time.gone()
        text_view_approval_code.gone()
        text_view_status.gone()

        setupRecyclerView()

        payment?.let { payment ->
            val calendar = Calendar.getInstance().timeInMillis
            text_view_transaction_time.text = calendar.formatDate("hh:mm a")
            text_view_transaction_date.text = calendar.formatDate("dd/MM/yyyy")
            text_view_subtotal.text = getString(R.string.label_amount_placeholder,
                getCurrency(),  payment.subtotal.formatAmount())
            text_view_total.text = getString(R.string.label_amount_placeholder,
                getCurrency(), payment.total.formatAmount())
            text_view_payable_amount.text = getString(R.string.label_amount_placeholder,
                getCurrency(), payment.payableAmount.formatAmount())
            text_view_amount_paid.text = getString(R.string.label_amount_placeholder,
                getCurrency(), payment.amountPaid.formatAmount())

            payment.voucher?.let { voucher ->
                text_view_voucher_label.visible()
                text_view_voucher.visible()

                text_view_voucher_label.text = getString(R.string.label_voucher_placeholder,
                    voucher.voucherCode)
                text_view_voucher.text = getString(R.string.label_negative_amount_placeholder,
                    getCurrency(), voucher.voucherAmount.formatAmount())
            }

            payment.tax?.let { tax ->
                text_view_tax_label.text = getString(R.string.label_tax_placeholder,
                    tax.taxType, tax.percent.toInt())
                text_view_tax.text = getString(R.string.label_amount_placeholder,
                    getCurrency(),  payment.taxAmount.formatAmount())
            }

            if (payment.isChangeRequired()) {
                text_view_changes_label.visible()
                text_view_changes.visible()

                text_view_changes.text = getString(R.string.label_amount_placeholder,
                    getCurrency(), payment.getChange().formatAmount())
            }

            when (payment.paymentMethod) {
                PaymentMethod.CASH -> {
                    text_view_payment_method.setText(R.string.label_payment_method_cash)
                }

                PaymentMethod.CASHLESS -> {
                    payment.cashlessType?.let { cashlessType ->
                        text_view_payment_method.text = getString(R.string.label_payment_method_cashless_placeholder,
                            cashlessType.toString())
                    }
                    text_view_transaction_date_time.text = getString(R.string.label_transaction_date_time_placeholder,
                        calendar.formatDate("dd-MMM-yyyy hh:mm:ss"))
                    text_view_transaction_date_time.visible()
                    text_view_approval_code.visible()
                    text_view_status.visible()
                }
            }

            (recycler_view_product_cart_receipt.adapter as? ProductCartReceiptAdapter)?.items =
                payment.getProductCarts().toMutableList()
        }
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_send_receipt.click {
            attemptSendReceipt()
        }
    }

    private fun setupRecyclerView() {
        val adapter = ProductCartReceiptAdapter(this)
        recycler_view_product_cart_receipt.apply {
            this.adapter = adapter
            this.layoutManager = LinearLayoutManager(this@PaymentShareReceiptActivity)
            this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))
        }
    }

    private fun attemptSendReceipt() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    sendReceipt()
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    showErrorAlertDialog(getString(R.string.error_permission_write_storage))
                }
            }).check()
    }

    private fun sendReceipt() {
        try {
            val bitmap = screenshotReceipt()
            saveBitmap("receipt-12345.jpg", "image/jpeg", bitmap)?.let { uri ->
                val intent = Intent(Intent.ACTION_SEND).apply {
                    this.type = "image/*"
                    this.putExtra(Intent.EXTRA_STREAM, uri)
                    this.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                }

                startActivity(Intent.createChooser(intent, "Share receipt"))
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun screenshotReceipt(): Bitmap {
        val bitmap = Bitmap.createBitmap(layout_receipt.width, layout_receipt.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        layout_receipt.draw(canvas)

        return bitmap
    }

    private fun showErrorAlertDialog(message: CharSequence, action: (() -> Unit)? = null) {
        CommonAlertDialog(this, message, action).show()
    }
    //endregion
}
