package com.easipos.easipos.activities.payment_result.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter

class PaymentResultPresenter(application: Application)
    : Presenter<PaymentResultView>(application)
