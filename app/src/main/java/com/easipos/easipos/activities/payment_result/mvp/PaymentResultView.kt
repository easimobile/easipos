package com.easipos.easipos.activities.payment_result.mvp

import com.easipos.easipos.base.View
import com.easipos.easipos.models.Payment

interface PaymentResultView : View
