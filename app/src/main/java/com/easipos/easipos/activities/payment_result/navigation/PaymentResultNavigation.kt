package com.easipos.easipos.activities.payment_result.navigation

import android.app.Activity
import com.easipos.easipos.models.Payment

interface PaymentResultNavigation {

    fun navigateToPaymentShareReceipt(activity: Activity, payment: Payment)

    fun navigateToPaymentPrintReceipt(activity: Activity, payment: Payment)
}