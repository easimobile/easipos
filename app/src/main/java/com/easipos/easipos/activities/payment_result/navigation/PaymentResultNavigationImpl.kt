package com.easipos.easipos.activities.payment_result.navigation

import android.app.Activity
import com.easipos.easipos.activities.payment_result.PaymentShareReceiptActivity
import com.easipos.easipos.models.Payment

class PaymentResultNavigationImpl : PaymentResultNavigation {

    override fun navigateToPaymentShareReceipt(activity: Activity, payment: Payment) {
        activity.startActivity(PaymentShareReceiptActivity.newIntent(activity, payment))
    }

    override fun navigateToPaymentPrintReceipt(activity: Activity, payment: Payment) {
    }
}