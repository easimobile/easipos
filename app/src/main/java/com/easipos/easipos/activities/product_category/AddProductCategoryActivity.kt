package com.easipos.easipos.activities.product_category

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.models.Category
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.showKeyboard
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_add_product_category.*
import org.kodein.di.generic.instance

class AddProductCategoryActivity : CustomBaseAppCompatActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, AddProductCategoryActivity::class.java)
        }
    }

    //region Variables
    private val validator: Validator by instance()
    //endregion

    //region Lifecycle
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_add_product_category

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_input_edit_text_product_category_name.post {
            text_input_edit_text_product_category_name.requestFocus()
            showKeyboard(text_input_edit_text_product_category_name)
        }
    }

    private fun setupListeners() {
        image_view_close.click {
            finish()
        }

        button_add_category.click {
            attemptAddCategory()
        }
    }

    private fun attemptAddCategory() {
        val categoryNameValidation = Validation(text_input_layout_product_category_name)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val categoryName = values[0].trim()
                val category = Category(name = categoryName)

                setResult(Activity.RESULT_OK, Intent().apply {
                    this.putExtra(ParcelData.CATEGORY, category)
                })
                finish()
            }
        }).validate(categoryNameValidation)
    }
    //endregion
}
