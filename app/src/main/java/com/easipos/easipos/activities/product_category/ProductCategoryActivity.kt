package com.easipos.easipos.activities.product_category

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.easipos.R
import com.easipos.easipos.activities.product_category.mvp.ProductCategoryPresenter
import com.easipos.easipos.activities.product_category.mvp.ProductCategoryView
import com.easipos.easipos.activities.product_category.navigation.ProductCategoryNavigation
import com.easipos.easipos.activities.product_category.viewmodel.ProductCategoryViewModel
import com.easipos.easipos.activities.product_category.viewmodel.ProductCategoryViewModelFactory
import com.easipos.easipos.adapters.CategoryAdapter
import com.easipos.easipos.base.CustomLifecycleActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.bundle.RequestCode
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.models.Category
import com.easipos.easipos.tools.EqualSpacingItemDecoration
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_product_category.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class ProductCategoryActivity : CustomLifecycleActivity<ProductCategoryViewModel>(),
    ProductCategoryView, CategoryAdapter.OnGestureDetectedListener {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ProductCategoryActivity::class.java)
        }
    }

    //region Variables
    private val navigation: ProductCategoryNavigation by instance()

    private val presenter by lazy { ProductCategoryPresenter(application) }
    private val factory by lazy { ProductCategoryViewModelFactory(application) }
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCode.ADD_PRODUCT_CATEGORY && resultCode == Activity.RESULT_OK && data != null) {
            data.getParcelableExtra<Category>(ParcelData.CATEGORY)?.let { category ->
                addCategory(category)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getCategories()
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_product_category

    override fun setupViewModel(): ProductCategoryViewModel {
        return ViewModelProvider(this, factory)
            .get(ProductCategoryViewModel::class.java)
    }

    override fun setupViewModelObserver() {

    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
        getCategories()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun refreshCategories() {
        getCategories()
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(item: Category) {
        setResult(Activity.RESULT_OK, Intent().apply {
            this.putExtra(ParcelData.CATEGORY, item)
        })
        finish()
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        setupRecyclerView()
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_add_category.click {
            navigation.navigateToAddCategory(this)
        }
    }

    private fun setupRecyclerView() {
        val adapter = CategoryAdapter(this, this)
        recycler_view_category.apply {
            this.adapter = adapter
            this.layoutManager = LinearLayoutManager(this@ProductCategoryActivity)
            this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))
        }
    }

    private fun getCategories() {
        viewModel.start()
    }

    private fun addCategory(category: Category) {
        presenter.doAddCategory(category)
    }
    //endregion
}
