package com.easipos.easipos.activities.product_category.mvp

import android.app.Application
import com.easipos.easipos.R
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.Category
import com.easipos.easipos.use_cases.base.DefaultObserver
import com.easipos.easipos.use_cases.sales.AddCategoryUseCase

class ProductCategoryPresenter(application: Application)
    : Presenter<ProductCategoryView>(application) {

    private val addCategoryUseCase by lazy { AddCategoryUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        addCategoryUseCase.dispose()
    }

    fun doAddCategory(category: Category) {
        addCategoryUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.toastMessage(R.string.prompt_category_added_successfully)
                view?.refreshCategories()
            }
        }, AddCategoryUseCase.Params.createQuery(category))
    }
}
