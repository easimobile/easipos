package com.easipos.easipos.activities.product_category.mvp

import com.easipos.easipos.base.View

interface ProductCategoryView : View {

    fun refreshCategories()
}
