package com.easipos.easipos.activities.product_category.navigation

import android.app.Activity

interface ProductCategoryNavigation {

    fun navigateToAddCategory(activity: Activity)
}