package com.easipos.easipos.activities.product_category.navigation

import android.app.Activity
import com.easipos.easipos.activities.product_category.AddProductCategoryActivity
import com.easipos.easipos.bundle.RequestCode

class ProductCategoryNavigationImpl : ProductCategoryNavigation {

    override fun navigateToAddCategory(activity: Activity) {
        activity.startActivityForResult(AddProductCategoryActivity.newIntent(activity),
            RequestCode.ADD_PRODUCT_CATEGORY)
    }
}