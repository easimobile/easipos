package com.easipos.easipos.activities.product_category.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import com.easipos.easipos.base.CustomBaseAndroidViewModel
import com.easipos.easipos.models.Category
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.sales.GetCategoriesUseCase
import com.easipos.easipos.util.ErrorUtil

class ProductCategoryViewModel(application: Application)
    : CustomBaseAndroidViewModel<Void>(application) {

    //region Observable Field
    val categories = ObservableField<List<Category>>()
    //endregion

    //region LiveData
    //endregion

    private val getCategoriesUseCase by lazy { GetCategoriesUseCase(kodein) }

    override fun onCleared() {
        super.onCleared()
        getCategoriesUseCase.dispose()
    }

    override fun start(args: Void?) {
        getCategories()
    }

    private fun getCategories() {
        setIsLoading(true)
        getCategoriesUseCase.execute(object : DefaultSingleObserver<List<Category>>() {
            override fun onSuccess(value: List<Category>) {
                setIsLoading(false)
                setCategories(value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                setIsLoading(false)
                showToast(ErrorUtil.parseException(error))
            }
        }, GetCategoriesUseCase.Params.createQuery())
    }

    private fun setCategories(categories: List<Category>) {
        this.categories.set(categories)
        this.categories.notifyChange()
    }
}