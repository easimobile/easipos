package com.easipos.easipos.activities.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.activities.register.mvp.RegisterPresenter
import com.easipos.easipos.activities.register.mvp.RegisterView
import com.easipos.easipos.activities.register.navigation.RegisterNavigation
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.models.User
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.md5
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.EqualRule
import io.github.anderscheow.validator.rules.common.MinRule
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import java.util.*

class RegisterActivity : CustomBaseAppCompatActivity(), RegisterView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, RegisterActivity::class.java)
        }
    }

    //region Variables
    private val navigation: RegisterNavigation by instance()
    private val validator: Validator by instance()

    private val presenter by lazy { RegisterPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_register

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {

    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_register.click {
            attemptRegister()
        }
    }

    private fun attemptRegister() {
        val nameValidation = Validation(text_input_layout_name)
            .add(NotBlankRule(R.string.error_field_required))
        val usernameValidation = Validation(text_input_layout_username)
            .add(NotBlankRule(R.string.error_field_required))
        val passwordValidation = Validation(text_input_layout_password)
            .add(NotBlankRule(R.string.error_field_required))
            .add(MinRule(6, R.string.error_password_min_length))
        val confirmPasswordValidation = Validation(text_input_layout_confirm_password)
            .add(NotBlankRule(R.string.error_field_required))
            .add(MinRule(6, R.string.error_password_min_length))
            .add(EqualRule(text_input_edit_text_password.text?.toString() ?: "", R.string.error_password_not_match))
        val shopNameValidation = Validation(text_input_layout_shop_name)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val name = values[0].trim()
                val username = values[1].trim().toLowerCase(Locale.getDefault())
                val password = values[2].trim().toLowerCase(Locale.getDefault())
                val shopName = values[4]
                val hashedPassword = password.md5()

                val user = User(
                    name = name,
                    username = username,
                    password = hashedPassword,
                    shopName = shopName
                )

                presenter.doRegister(user)
            }
        }).validate(nameValidation, usernameValidation, passwordValidation,
            confirmPasswordValidation, shopNameValidation)
    }
    //endregion
}
