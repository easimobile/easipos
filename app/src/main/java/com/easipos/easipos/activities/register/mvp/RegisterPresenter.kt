package com.easipos.easipos.activities.register.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.User
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.user.RegisterUseCase
import com.easipos.easipos.util.ErrorUtil

class RegisterPresenter(application: Application)
    : Presenter<RegisterView>(application) {

    private val registerUseCase by lazy { RegisterUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        registerUseCase.dispose()
    }

    fun doRegister(user: User) {
        view?.setLoadingIndicator(true)
        registerUseCase.execute(object : DefaultSingleObserver<Long>() {
            override fun onSuccess(value: Long) {
                view?.setLoadingIndicator(false)

                Preference.prefUserId = value
                Preference.prefIsLoggedIn = true

                view?.navigateToMain()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, RegisterUseCase.Params.createQuery(user))
    }
}
