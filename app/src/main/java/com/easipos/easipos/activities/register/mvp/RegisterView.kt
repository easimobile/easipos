package com.easipos.easipos.activities.register.mvp

import com.easipos.easipos.base.View

interface RegisterView : View {

    fun navigateToMain()
}
