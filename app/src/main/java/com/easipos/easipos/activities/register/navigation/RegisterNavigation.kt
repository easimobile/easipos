package com.easipos.easipos.activities.register.navigation

import android.app.Activity

interface RegisterNavigation {

    fun navigateToMain(activity: Activity)
}