package com.easipos.easipos.activities.register.navigation

import android.app.Activity
import com.easipos.easipos.activities.main.MainActivity

class RegisterNavigationImpl : RegisterNavigation {

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}