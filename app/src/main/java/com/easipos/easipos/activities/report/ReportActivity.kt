package com.easipos.easipos.activities.report

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.activities.report.navigation.ReportNavigation
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_report.*
import org.kodein.di.generic.instance

class ReportActivity : CustomBaseAppCompatActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ReportActivity::class.java)
        }
    }

    //region Variables
    private val navigation: ReportNavigation by instance()
    //endregion

    //region Lifecycle
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_report

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
    }

    private fun setupListeners() {
        image_view_close.click {
            finish()
        }

        card_view_sales_report.click {
            navigation.navigateToSalesReport(this)
        }
    }
    //endregion
}
