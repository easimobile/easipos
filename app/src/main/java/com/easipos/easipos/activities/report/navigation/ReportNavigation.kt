package com.easipos.easipos.activities.report.navigation

import android.app.Activity

interface ReportNavigation {

    fun navigateToSalesReport(activity: Activity)
}