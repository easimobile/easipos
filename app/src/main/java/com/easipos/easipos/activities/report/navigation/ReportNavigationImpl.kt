package com.easipos.easipos.activities.report.navigation

import android.app.Activity
import com.easipos.easipos.activities.sales_report.SalesReportActivity

class ReportNavigationImpl : ReportNavigation {

    override fun navigateToSalesReport(activity: Activity) {
        activity.startActivity(SalesReportActivity.newIntent(activity))
    }
}