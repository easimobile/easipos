package com.easipos.easipos.activities.sales

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.easipos.R
import com.easipos.easipos.activities.sales.mvp.SalesPresenter
import com.easipos.easipos.activities.sales.mvp.SalesView
import com.easipos.easipos.activities.sales.navigation.SalesNavigation
import com.easipos.easipos.activities.sales.viewmodel.SalesViewModel
import com.easipos.easipos.activities.sales.viewmodel.SalesViewModelFactory
import com.easipos.easipos.adapters.ProductAdapter
import com.easipos.easipos.base.CustomLifecycleActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.bundle.RequestCode
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.fragments.alert_dialog.ManualAddProductAlertDialog
import com.easipos.easipos.fragments.dialog_fragment.SearchProductDialogFragment
import com.easipos.easipos.models.Cart
import com.easipos.easipos.models.Category
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.tools.EqualSpacingItemDecoration
import com.easipos.easipos.util.getCurrency
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.activity_sales.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class SalesActivity : CustomLifecycleActivity<SalesViewModel>(), SalesView,
    ProductAdapter.OnGestureDetectedListener {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SalesActivity::class.java)
        }
    }

    //region Variables
    private val navigation: SalesNavigation by instance()

    private val presenter by lazy { SalesPresenter(application) }
    private val factory by lazy { SalesViewModelFactory(application) }
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RequestCode.SCAN_BARCODE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val barcode = data.getStringExtra(ParcelData.BARCODE_VALUE) ?: ""

                    getProductByBarcode(barcode)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getProducts()
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_sales

    override fun setupViewModel(): SalesViewModel {
        return ViewModelProvider(this, factory)
            .get(SalesViewModel::class.java)
    }

    override fun setupViewModelObserver() {
        Cart.cartLiveData.observe(this, Observer { cart ->
            processCartLayout(cart)
        })
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
        getProducts()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun populateProduct(productCategoryPair: ProductCategoryPair) {
        addProductToCart(productCategoryPair)
    }

    override fun populateCategories(attachedView: View, categories: List<Category>) {
        val popupMenu = PopupMenu(this, attachedView).apply {
            categories.forEachIndexed { index, category ->
                this.menu.add(0, index, index, category.name)
            }
            this.setOnMenuItemClickListener { menuItem ->
                val category = categories[menuItem.itemId]

                setCategoryTextView(category)
                getProductByCategory(category)

                true
            }
        }

        popupMenu.show()
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(item: ProductCategoryPair) {
        Cart.insertProductToCart(item)
    }

    override fun onLongSelectItem(item: ProductCategoryPair) {
        navigation.navigateToAddEditProduct(this, item)
    }
    //endregion

    //region Action Methods
    fun addProductToCart(productCategoryPair: ProductCategoryPair) {
        Cart.insertProductToCart(productCategoryPair)
    }

    private fun setupViews() {
        text_view_amount.text = ""  // To fix not able to show amount when add to cart
        layout_cart.gone()

        setupRecyclerView()
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        image_view_search.click {
            val fragment = SearchProductDialogFragment.newInstance()
            fragment.show(supportFragmentManager, SearchProductDialogFragment.TAG)
        }

        card_view_manual_add_product.click {
            ManualAddProductAlertDialog(this) { barcode ->
                getProductByBarcode(barcode)
            }.show()
        }

        card_view_scan_barcode.click {
            navigation.navigateToScanBarcode(this)
        }

        text_view_category.click {
            presenter.doGetCategories(it)
        }

        button_new_product.click {
            navigation.navigateToAddEditProduct(this, null)
        }

        layout_cart.click {
            navigation.navigateToOrderDetails(this)
        }
    }

    private fun setupRecyclerView() {
        val adapter = ProductAdapter(this, this)
        recycler_view_product.apply {
            this.adapter = adapter
            this.layoutManager = LinearLayoutManager(this@SalesActivity)
            this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))
        }
    }

    private fun setCategoryTextView(category: Category) {
        text_view_category.text = category.name
    }

    private fun getProducts() {
        viewModel.start()
    }

    private fun getProductByBarcode(barcode: String) {
        presenter.doGetProduct(barcode)
    }

    private fun getProductByCategory(category: Category) {
        viewModel.getProductsByCategory(category)
    }

    private fun processCartLayout(cart: Cart) {
        if (cart.hasProducts()) {
            layout_cart.visible()

            text_view_products.text = resources.getQuantityString(R.plurals.label_product_placeholder,
                cart.getTotalQuantity(), cart.getTotalQuantity())
            text_view_amount.text = getString(R.string.label_amount_placeholder,
                getCurrency(), cart.getTotalSellingPrice().formatAmount())
        } else {
            layout_cart.gone()
        }
    }
    //endregion
}
