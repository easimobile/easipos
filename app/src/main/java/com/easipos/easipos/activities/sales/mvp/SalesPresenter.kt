package com.easipos.easipos.activities.sales.mvp

import android.app.Application
import android.view.View
import com.easipos.easipos.R
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.Category
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.sales.GetCategoriesUseCase
import com.easipos.easipos.use_cases.sales.GetProductByBarcodeUseCase

class SalesPresenter(application: Application)
    : Presenter<SalesView>(application) {

    private val getProductByBarcodeUseCase by lazy { GetProductByBarcodeUseCase(kodein) }
    private val getCategoriesUseCase by lazy { GetCategoriesUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        getProductByBarcodeUseCase.dispose()
        getCategoriesUseCase.dispose()
    }
    fun doGetProduct(barcode: String) {
        getProductByBarcodeUseCase.execute(object : DefaultSingleObserver<ProductCategoryPair>() {
            override fun onSuccess(value: ProductCategoryPair) {
                view?.toastMessage(R.string.prompt_added_to_cart_successfully)
                view?.populateProduct(value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.showErrorAlertDialog(application.getString(R.string.error_product_not_found))
            }
        }, GetProductByBarcodeUseCase.Params.createQuery(barcode))
    }

    fun doGetCategories(attachedView: View) {
        getCategoriesUseCase.execute(object : DefaultSingleObserver<List<Category>>() {
            override fun onSuccess(value: List<Category>) {
                val mutableCategories = value.toMutableList()
                mutableCategories.add(0, Category.getAllProducts())

                view?.populateCategories(attachedView, mutableCategories)
            }
        }, GetCategoriesUseCase.Params.createQuery())
    }
}
