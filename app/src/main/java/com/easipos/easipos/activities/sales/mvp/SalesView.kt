package com.easipos.easipos.activities.sales.mvp

import com.easipos.easipos.base.View
import com.easipos.easipos.models.Category
import com.easipos.easipos.models.ProductCategoryPair

interface SalesView : View {

    fun populateProduct(productCategoryPair: ProductCategoryPair)

    fun populateCategories(attachedView: android.view.View, categories: List<Category>)
}
