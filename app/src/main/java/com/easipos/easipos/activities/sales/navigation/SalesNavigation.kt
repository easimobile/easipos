package com.easipos.easipos.activities.sales.navigation

import android.app.Activity
import com.easipos.easipos.models.ProductCategoryPair

interface SalesNavigation {

    fun navigateToAddEditProduct(activity: Activity, productCategoryPair: ProductCategoryPair?)

    fun navigateToScanBarcode(activity: Activity)

    fun navigateToOrderDetails(activity: Activity)
}