package com.easipos.easipos.activities.sales.navigation

import android.app.Activity
import com.easipos.easipos.activities.add_edit_product.AddEditProductActivity
import com.easipos.easipos.activities.order_details.OrderDetailsActivity
import com.easipos.easipos.activities.scan_barcode.ScanBarcodeActivity
import com.easipos.easipos.bundle.RequestCode
import com.easipos.easipos.models.ProductCategoryPair

class SalesNavigationImpl : SalesNavigation {

    override fun navigateToAddEditProduct(activity: Activity, productCategoryPair: ProductCategoryPair?) {
        activity.startActivity(AddEditProductActivity.newIntent(activity, productCategoryPair))
    }

    override fun navigateToScanBarcode(activity: Activity) {
        activity.startActivityForResult(ScanBarcodeActivity.newIntent(activity),
            RequestCode.SCAN_BARCODE)
    }

    override fun navigateToOrderDetails(activity: Activity) {
        activity.startActivity(OrderDetailsActivity.newIntent(activity))
    }
}