package com.easipos.easipos.activities.sales.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import com.easipos.easipos.base.CustomBaseAndroidViewModel
import com.easipos.easipos.models.Category
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.sales.GetProductsByCategoryUseCase
import com.easipos.easipos.use_cases.sales.GetProductsUseCase
import com.easipos.easipos.util.ErrorUtil

class SalesViewModel(application: Application)
    : CustomBaseAndroidViewModel<Void>(application) {

    //region Observable Field
    val products = ObservableField<List<ProductCategoryPair>>()
    //endregion

    //region LiveData
    //endregion

    private val getProductsUseCase by lazy { GetProductsUseCase(kodein) }
    private val getProductsByCategoryUseCase by lazy { GetProductsByCategoryUseCase(kodein) }

    private var category: Category? = null

    override fun onCleared() {
        super.onCleared()
        getProductsUseCase.dispose()
        getProductsByCategoryUseCase.dispose()
    }

    override fun start(args: Void?) {
        getProducts()
    }

    override fun onRefresh() {
        if (category == null) {
            getProducts()
        } else {
            getProductsByCategory(category!!)
        }
    }

    fun getProductsByCategory(category: Category) {
        this.category = category

        if (category.categoryId == -1L) {
            getProducts()
            return
        }

        setIsLoading(true)
        getProductsByCategoryUseCase.execute(object : DefaultSingleObserver<List<ProductCategoryPair>>() {
            override fun onSuccess(value: List<ProductCategoryPair>) {
                setIsLoading(false)
                setProducts(value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                setIsLoading(false)
                showToast(ErrorUtil.parseException(error))
            }
        }, GetProductsByCategoryUseCase.Params.createQuery(category))
    }

    private fun getProducts() {
        setIsLoading(true)
        getProductsUseCase.execute(object : DefaultSingleObserver<List<ProductCategoryPair>>() {
            override fun onSuccess(value: List<ProductCategoryPair>) {
                setIsLoading(false)
                setProducts(value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                setIsLoading(false)
                showToast(ErrorUtil.parseException(error))
            }
        }, GetProductsUseCase.Params.createQuery())
    }

    private fun setProducts(products: List<ProductCategoryPair>) {
        this.products.set(products)
        this.products.notifyChange()
    }
}