package com.easipos.easipos.activities.sales_report

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CalendarView
import android.widget.PopupWindow
import com.easipos.easipos.R
import com.easipos.easipos.activities.sales_report.mvp.SalesReportPresenter
import com.easipos.easipos.activities.sales_report.mvp.SalesReportView
import com.easipos.easipos.activities.sales_report.navigation.SalesReportNavigation
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.models.SalesReport
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.dimBehind
import io.github.anderscheow.library.kotlinExt.formatDate
import io.github.anderscheow.library.kotlinExt.generatePopupWindow
import kotlinx.android.synthetic.main.activity_sales_report.*
import org.kodein.di.generic.instance
import java.util.*

class SalesReportActivity : CustomBaseAppCompatActivity(), SalesReportView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SalesReportActivity::class.java)
        }
    }

    //endregion
    private val navigation: SalesReportNavigation by instance()

    private val presenter by lazy { SalesReportPresenter(application) }

    private var popupWindow: PopupWindow? = null
    private var selectedFromDate: Calendar = Calendar.getInstance()
    private var selectedToDate: Calendar = Calendar.getInstance()
    //region Variables

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_sales_report

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun navigateToSalesReportDetails(salesReport: SalesReport) {
        navigation.navigateToSalesReportDetails(this, salesReport)
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_view_date_from.text = selectedFromDate.timeInMillis.formatDate("dd/MM/yyyy")
        text_view_date_to.text = selectedToDate.timeInMillis.formatDate("dd/MM/yyyy")
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        card_view_date_from.click {
            showDateSelector(it, 0)
        }

        card_view_date_to.click {
            showDateSelector(it, 1)
        }

        button_search.click {
            attemptGetSalesReport()
        }
    }

    private fun showDateSelector(attachedView: View, dateType: Int) {
        popupWindow = generatePopupWindow(this, R.layout.popup_select_date) { view ->
            val calendarView = view.findViewById<CalendarView>(R.id.calendar_view)

            if (dateType == 0) {
                // From Date
                calendarView.date = selectedFromDate.timeInMillis
                calendarView.maxDate = selectedToDate.timeInMillis
            } else {
                // To Date
                calendarView.date = selectedToDate.timeInMillis
                calendarView.minDate = selectedFromDate.timeInMillis
                calendarView.maxDate = Calendar.getInstance().timeInMillis
            }

            calendarView.setOnDateChangeListener { _, year, month, dayOfMonth ->
                val calendar = Calendar.getInstance().apply {
                    this.set(year, month, dayOfMonth)
                }
                val dateInString = calendar.timeInMillis.formatDate("dd/MM/yyyy")

                if (dateType == 0) {
                    selectedFromDate = calendar
                    text_view_date_from.text = dateInString
                } else {
                    selectedToDate = calendar
                    text_view_date_to.text = dateInString
                }

                popupWindow?.dismiss()
            }
        }

        popupWindow?.showAsDropDown(attachedView, 0, attachedView.height / 2)
        popupWindow?.dimBehind()
    }

    private fun attemptGetSalesReport() {
        val fromDate = selectedFromDate.timeInMillis.formatDate("yyyy-MM-dd")
        val toDate = selectedToDate.timeInMillis.formatDate("yyyy-MM-dd")

        presenter.getDayEndReport(fromDate, toDate)
    }
    //endregion
}
