package com.easipos.easipos.activities.sales_report

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import com.easipos.easipos.BuildConfig
import com.easipos.easipos.R
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.models.SalesReport
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.util.saveCsv
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.orhanobut.logger.Logger
import de.siegmar.fastcsv.writer.CsvWriter
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_sales_report_details.*
import kotlin.math.max

class SalesReportDetailsActivity : CustomBaseAppCompatActivity() {

    companion object {
        fun newIntent(context: Context, salesReport: SalesReport): Intent {
            return Intent(context, SalesReportDetailsActivity::class.java).apply {
                this.putExtra(ParcelData.SALES_REPORT, salesReport)
            }
        }
    }

    //endregion
    private val salesReport by argument<SalesReport>(ParcelData.SALES_REPORT)
    //region Variables

    //region Lifecycle
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_sales_report_details

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods

    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        salesReport?.let { report ->
            val dashedLine = "------------------------------------------------------"

            var productsInString = ""
            report.groupProducts.keys.forEach { pair ->
                productsInString += "${formatMaxString(pair.first, 7)}  " +
                        "${formatMaxString(pair.second, 20)}  " +
                        "${formatMaxString(report.getProductSoldQuantity(pair).toString(), 5)}  " +
                        "${formatSpacing(15, report.getProductSoldTotalAmount(pair))}\n"
            }

            val reportInString = "" +
                    Preference.prefReceiptHeader +
                    "\n" +
                    "\n" +
                    "Sales Report\n" +
                    "${report.formatFromDate()} - ${report.formatToDate()}" +
                    "\n" +
                    "\n" +
                    "$dashedLine\n" +
                    "Code     Name                  Qty          Total\n" +
                    "$dashedLine\n" +
                    productsInString +
                    "$dashedLine\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "${report.formatReportGenerateDate()}\n" +
                    "\n" +
                    "\n" +
                    "\n"

            text_view_report.text = reportInString
        }
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_share.click {
            attemptExportReport()
        }
    }

    private fun getSpacing(left: Int, right: String): String {
        val amountOfSpacing = 12 - left.toString().length - right.length
        return if (amountOfSpacing < 1) {
            ""
        } else {
            var spacing = ""
            repeat(amountOfSpacing) {
                spacing += " "
            }
            spacing
        }
    }

    private fun formatSpacing(availableSpace: Int, value: String): String {
        val length = value.length
        if (length < availableSpace) {
            var spacing = ""
            repeat(availableSpace - length) {
                spacing += " "
            }
            return "$spacing$value"
        }
        return value
    }

    private fun formatMaxString(value: String, maxLength: Int): String {
        return if (value.length >= maxLength) {
            value.substring(0, maxLength + 1)
        } else {
            var spacing = ""
            repeat(maxLength - value.length) {
                spacing += " "
            }
            "$value$spacing"
        }
    }

    private fun attemptExportReport() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    exportReport()
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    showErrorAlertDialog(getString(R.string.error_permission_write_storage))
                }
            }).check()
    }

    private fun exportReport() {
        salesReport?.let { report ->
            val dashedLine = "------------------------------------------------------"
            val csvWriter = CsvWriter()
            val data = arrayListOf<Array<String>>().apply {
                val receiptHeaders = Preference.prefReceiptHeader.split("\n")
                receiptHeaders.forEach { header ->
                    this.add(arrayOf(header))
                }
                this.add(arrayOf(""))
                this.add(arrayOf("Sales Report"))
                this.add(arrayOf("${report.formatFromDate()} - ${report.formatToDate()}"))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Code", "Name", "Qty", "Total"))
                this.add(arrayOf(dashedLine))
                report.groupProducts.keys.forEach { pair ->
                    this.add(arrayOf(pair.first, pair.second, report.getProductSoldQuantity(pair).toString(), report.getProductSoldTotalAmount(pair)))
                }
                this.add(arrayOf(dashedLine))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(report.formatReportGenerateDate()))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
            }

            try {
                saveCsv(
                    "sales-${report.fromDate}-${report.toDate}-${report.formatReportGenerateDate2()}.csv",
                    csvWriter,
                    data
                )?.let { uri ->
                    val newUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", uri.toFile())
                    val intent = Intent(Intent.ACTION_SEND).apply {
                        this.type = "text/csv"
                        this.putExtra(Intent.EXTRA_STREAM, newUri)
                        this.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    }

                    startActivity(Intent.createChooser(intent, "Share sales report"))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                showErrorAlertDialog(ex.localizedMessage?.toString() ?: "")
            }
        }
    }

    private fun showErrorAlertDialog(message: CharSequence) {
        CommonAlertDialog(this, message).show()
    }
    //endregion
}
