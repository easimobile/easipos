package com.easipos.easipos.activities.sales_report.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.SalesReport
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.report.GetSalesReportUseCase

class SalesReportPresenter(application: Application)
    : Presenter<SalesReportView>(application) {

    private val getSalesReportUseCase by lazy { GetSalesReportUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        getSalesReportUseCase.dispose()
    }

    fun getDayEndReport(fromDate: String, toDate: String) {
        getSalesReportUseCase.execute(object : DefaultSingleObserver<SalesReport>() {
            override fun onSuccess(value: SalesReport) {
                view?.navigateToSalesReportDetails(value)
            }
        }, GetSalesReportUseCase.Params.createQuery(fromDate, toDate))
    }
}
