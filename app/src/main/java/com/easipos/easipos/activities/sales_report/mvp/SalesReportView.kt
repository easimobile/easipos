package com.easipos.easipos.activities.sales_report.mvp

import com.easipos.easipos.base.View
import com.easipos.easipos.models.Payment
import com.easipos.easipos.models.SalesReport
import com.easipos.easipos.models.ShiftCloseReport

interface SalesReportView : View {

    fun navigateToSalesReportDetails(salesReport: SalesReport)
}
