package com.easipos.easipos.activities.sales_report.navigation

import android.app.Activity
import com.easipos.easipos.models.SalesReport

interface SalesReportNavigation {

    fun navigateToSalesReportDetails(activity: Activity, salesReport: SalesReport)
}