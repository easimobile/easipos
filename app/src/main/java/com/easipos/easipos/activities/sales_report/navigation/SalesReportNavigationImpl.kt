package com.easipos.easipos.activities.sales_report.navigation

import android.app.Activity
import com.easipos.easipos.activities.sales_report.SalesReportActivity
import com.easipos.easipos.activities.sales_report.SalesReportDetailsActivity
import com.easipos.easipos.models.SalesReport

class SalesReportNavigationImpl : SalesReportNavigation {

    override fun navigateToSalesReportDetails(activity: Activity, salesReport: SalesReport) {
        activity.startActivity(SalesReportDetailsActivity.newIntent(activity, salesReport))
    }
}