package com.easipos.easipos.activities.scan_barcode

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.easipos.easipos.R
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.google.zxing.Result
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.findColor
import kotlinx.android.synthetic.main.activity_scan_barcode.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScanBarcodeActivity : CustomBaseAppCompatActivity(), ZXingScannerView.ResultHandler {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ScanBarcodeActivity::class.java)
        }
    }

    //region Variables
    private val mScannerView by lazy { findViewById<ZXingScannerView>(R.id.layout_barcode) }
    //endregion

    override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this)
        attemptStartCamera()
    }

    override fun onPause() {
        mScannerView.stopCamera()
        super.onPause()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_scan_barcode

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    //endregion

    //region Interface Methods
    override fun handleResult(rawResult: Result?) {
        rawResult?.let { result ->
            setResult(Activity.RESULT_OK, Intent().apply {
                this.putExtra(ParcelData.BARCODE_VALUE, result.text)
            })
            finish()
        }
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        mScannerView.apply {
            this.setBorderCornerRadius(4)
            this.setIsBorderCornerRounded(true)
            this.setBorderColor(findColor(R.color.colorAccent))
            this.setMaskColor(Color.parseColor("#66000000"))
        }
    }

    private fun setupListeners() {
        image_view_close.click {
            finish()
        }
    }

    private fun attemptStartCamera() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    mScannerView.startCamera()
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                    Logger.d("onPermissionRationaleShouldBeShown")
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    showErrorAlertDialog(getString(R.string.error_permission_camera)) {
                        finish()
                    }
                }
            }).check()
    }

    private fun showErrorAlertDialog(message: CharSequence, action: (() -> Unit)? = null) {
        CommonAlertDialog(this, message, action).show()
    }
    //endregion
}
