package com.easipos.easipos.activities.shift_close_report

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import com.easipos.easipos.BuildConfig
import com.easipos.easipos.R
import com.easipos.easipos.activities.shift_close_report.mvp.ShiftCloseReportPresenter
import com.easipos.easipos.activities.shift_close_report.mvp.ShiftCloseReportView
import com.easipos.easipos.base.CustomBaseAppCompatActivity
import com.easipos.easipos.fragments.alert_dialog.CommonAlertDialog
import com.easipos.easipos.models.ShiftCloseReport
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.util.saveCsv
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import de.siegmar.fastcsv.writer.CsvWriter
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_shift_close_report.*
import org.jetbrains.anko.longToast

class ShiftCloseReportActivity : CustomBaseAppCompatActivity(), ShiftCloseReportView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ShiftCloseReportActivity::class.java)
        }
    }

    //endregion
    private val presenter by lazy { ShiftCloseReportPresenter(application) }

    private var shiftCloseReport: ShiftCloseReport? = null
    //region Variables

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_shift_close_report

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()

        presenter.getDayEndReport()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun populateShiftCloseReport(report: ShiftCloseReport) {
        shiftCloseReport = report

        val dashedLine = "-----------------------------------"
        val reportInString = "" +
                Preference.prefReceiptHeader +
                "\n" +
                "\n" +
                "Business Date: ${report.formatBusinessDate()}\n" +
                "\n" +
                "X READ: SHIFT ${report.shift}\n" +
                "\n" +
                "$dashedLine\n" +
                "Gross Sales        +   ${report.getPaymentsSize()}${getSpacing(report.getPaymentsSize(), report.getGrossSalesAmount())}${report.getGrossSalesAmount()}\n" +
                "$dashedLine\n" +
                "Net Sales          +   ${report.getPaymentsSize()}${getSpacing(report.getPaymentsSize(), report.getNetSalesAmount())}${report.getNetSalesAmount()}\n" +
                "$dashedLine\n" +
                "Cash               +   ${report.getCashNumber()}${getSpacing(report.getCashNumber(), report.getCashAmount())}${report.getCashAmount()}\n" +
                "Grab Pay           +   ${report.getGrabPayNumber()}${getSpacing(report.getGrabPayNumber(), report.getGrabPayAmount())}${report.getGrabPayAmount()}\n" +
                "Wechat Pay         +   ${report.getWechatPayNumber()}${getSpacing(report.getWechatPayNumber(), report.getWechatPayAmount())}${report.getWechatPayAmount()}\n" +
                "Alipay             +   ${report.getAlipayNumber()}${getSpacing(report.getAlipayNumber(), report.getAlipayAmount())}${report.getAlipayAmount()}\n" +
                "$dashedLine\n" +
                "Voucher            +   ${report.getVoucherNumber()}${getSpacing(report.getVoucherNumber(), report.getVoucherAmount())}${report.getVoucherAmount()}\n" +
                "$dashedLine\n" +
                "Float              +   1${getSpacing(1, report.formatFloatAmount())}${report.formatFloatAmount()}\n" +
                "$dashedLine\n" +
                "Cash In Drawer     ==  1${getSpacing(1, report.getCashInDrawer())}${report.getCashInDrawer()}\n" +
                "$dashedLine\n" +
                "\n" +
                "\n" +
                "\n" +
                "${report.formatReportGenerateDate()}\n" +
                "Shift: SHIFT ${report.shift}\n" +
                "\n" +
                "\n" +
                "\n"
        text_view_report.text = reportInString
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_view_report.movementMethod = ScrollingMovementMethod()
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_share.click {
            attemptExportReport()
        }
    }

    private fun getSpacing(left: Int, right: String): String {
        val amountOfSpacing = 12 - left.toString().length - right.length
        return if (amountOfSpacing < 1) {
            ""
        } else {
            var spacing = ""
            repeat(amountOfSpacing) {
                spacing += " "
            }
            spacing
        }
    }

    private fun attemptExportReport() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    exportReport()
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    showErrorAlertDialog(getString(R.string.error_permission_write_storage))
                }
            }).check()
    }

    private fun exportReport() {
        shiftCloseReport?.let { report ->
            val dashedLine = "-----------------------------------"
            val csvWriter = CsvWriter()
            val data = arrayListOf<Array<String>>().apply {
                val receiptHeaders = Preference.prefReceiptHeader.split("\n")
                receiptHeaders.forEach { header ->
                    this.add(arrayOf(header))
                }
                this.add(arrayOf(""))
                this.add(arrayOf("Business Date: ${report.formatBusinessDate()}"))
                this.add(arrayOf(""))
                this.add(arrayOf("X READ: SHIFT ${report.shift}"))
                this.add(arrayOf(""))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Gross Sales", "+", report.getPaymentsSize().toString(), report.getGrossSalesAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Net Sales", "+", report.getPaymentsSize().toString(), report.getNetSalesAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Cash", "+", report.getCashNumber().toString(), report.getCashAmount()))
                this.add(arrayOf("Grab Pay", "+", report.getGrabPayNumber().toString(), report.getGrabPayAmount()))
                this.add(arrayOf("Wechat Pay", "+", report.getWechatPayNumber().toString(), report.getWechatPayAmount()))
                this.add(arrayOf("Alipay", "+", report.getAlipayNumber().toString(), report.getAlipayAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Voucher", "+", report.getVoucherNumber().toString(), report.getVoucherAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Float", "+", "1", report.formatFloatAmount()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf("Cash In Drawer", "==", "1", report.getCashInDrawer()))
                this.add(arrayOf(dashedLine))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(report.formatReportGenerateDate()))
                this.add(arrayOf("Shift: SHIFT ${report.shift}"))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
                this.add(arrayOf(""))
            }

            try {
                saveCsv(
                    "shift-close-${report.formatReportGenerateDate2()}-shift${report.shift}.csv",
                    csvWriter,
                    data
                )?.let { uri ->
                    val newUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", uri.toFile())
                    val intent = Intent(Intent.ACTION_SEND).apply {
                        this.type = "text/csv"
                        this.putExtra(Intent.EXTRA_STREAM, newUri)
                        this.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    }

                    startActivity(Intent.createChooser(intent, "Share shift close report"))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                showErrorAlertDialog(ex.localizedMessage?.toString() ?: "")
            }
        }
    }
    //endregion
}
