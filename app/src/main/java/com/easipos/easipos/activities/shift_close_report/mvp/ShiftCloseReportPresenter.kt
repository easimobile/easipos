package com.easipos.easipos.activities.shift_close_report.mvp

import android.app.Application
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.models.ShiftCloseReport
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.report.GetShiftCloseReportUseCase

class ShiftCloseReportPresenter(application: Application)
    : Presenter<ShiftCloseReportView>(application) {

    private val getShiftCloseReportUseCase by lazy { GetShiftCloseReportUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        getShiftCloseReportUseCase.dispose()
    }

    fun getDayEndReport() {
        getShiftCloseReportUseCase.execute(object : DefaultSingleObserver<ShiftCloseReport>() {
            override fun onSuccess(value: ShiftCloseReport) {
                view?.populateShiftCloseReport(value)
            }
        }, GetShiftCloseReportUseCase.Params.createQuery())
    }
}
