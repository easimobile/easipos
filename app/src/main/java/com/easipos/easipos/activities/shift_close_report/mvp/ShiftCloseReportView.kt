package com.easipos.easipos.activities.shift_close_report.mvp

import com.easipos.easipos.base.View
import com.easipos.easipos.models.ShiftCloseReport

interface ShiftCloseReportView : View {

    fun populateShiftCloseReport(report: ShiftCloseReport)
}
