package com.easipos.easipos.activities.splash.mvp

import android.app.Application
import com.easipos.easipos.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easipos.base.Presenter
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.precheck.CheckVersionUseCase

class SplashPresenter(application: Application)
    : Presenter<SplashView>(application) {

    private val checkVersionUseCase by lazy { CheckVersionUseCase(kodein) }

    fun checkVersion() {
        checkVersionUseCase.execute(object : DefaultSingleObserver<Boolean>() {
            override fun onSuccess(value: Boolean) {
                super.onSuccess(value)
                if (value) {
                    view?.showUpdateAppDialog()
                } else {
                    checkIsAuthenticated()
                }
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                checkIsAuthenticated()
            }
        }, CheckVersionUseCase.Params.createQuery(CheckVersionRequestModel()))
    }

    fun checkIsAuthenticated() {
        // Todo: Remove checking on token, because it's offline
        if (Preference.prefIsLoggedIn && Preference.prefUserId != -1L/* && UserManager.token != null*/) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}
