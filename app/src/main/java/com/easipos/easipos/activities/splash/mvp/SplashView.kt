package com.easipos.easipos.activities.splash.mvp

import com.easipos.easipos.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
