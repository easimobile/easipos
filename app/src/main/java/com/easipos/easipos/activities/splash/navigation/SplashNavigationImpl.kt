package com.easipos.easipos.activities.splash.navigation

import android.app.Activity
import com.easipos.easipos.activities.login.LoginActivity
import com.easipos.easipos.activities.main.MainActivity

class SplashNavigationImpl : SplashNavigation {

    override fun navigateToLogin(activity: Activity) {
        activity.startActivity(LoginActivity.newIntent(activity))
        activity.finishAffinity()
    }

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}