package com.easipos.easipos.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easipos.R
import com.easipos.easipos.databinding.ViewCategoryBinding
import com.easipos.easipos.models.Category
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class CategoryAdapter(context: Context,
                      private val listener: OnGestureDetectedListener)
    : BaseRecyclerViewAdapter<Category>(context) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: Category)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewCategoryBinding>(
            layoutInflater, R.layout.view_category, parent, false)
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CategoryViewHolder) {
            val item = items[holder.adapterPosition]
            holder.bind(item)
        }
    }

    inner class CategoryViewHolder(private val binding: ViewCategoryBinding)
        : BaseViewHolder<Category>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Category) {
        }

        override fun onClick(view: View, item: Category?) {
            item?.let { item ->
                listener.onSelectItem(item)
            }
        }
    }
}