package com.easipos.easipos.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easipos.R
import com.easipos.easipos.databinding.ViewDenominationSuggestionBinding
import com.easipos.easipos.util.getCurrency
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_denomination_suggestion.*

class DenominationSuggestionAdapter(context: Context,
                                    private val listener: OnGestureDetectedListener)
    : BaseRecyclerViewAdapter<Double>(context) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: Double)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewDenominationSuggestionBinding>(
            layoutInflater, R.layout.view_denomination_suggestion, parent, false)
        return DenominationSuggestionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DenominationSuggestionViewHolder) {
            val item = items[holder.adapterPosition]
            holder.bind(item)
        }
    }

    inner class DenominationSuggestionViewHolder(private val binding: ViewDenominationSuggestionBinding)
        : BaseViewHolder<Double>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Double) {
            button_denomination.click {
                listener.onSelectItem(item)
            }

            button_denomination.text =
                context.getString(R.string.label_amount_placeholder, getCurrency(), item.formatAmount())
        }

        override fun onClick(view: View, item: Double?) {
        }
    }
}