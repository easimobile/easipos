package com.easipos.easipos.adapters

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easipos.R
import com.easipos.easipos.databinding.ViewProductBinding
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.tools.TextDrawable
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.findColor
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product.*
import org.jetbrains.anko.textColor

class ProductAdapter(context: Context,
                     private val listener: OnGestureDetectedListener)
    : BaseRecyclerViewAdapter<ProductCategoryPair>(context) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: ProductCategoryPair)
        fun onLongSelectItem(item: ProductCategoryPair)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductBinding>(
            layoutInflater, R.layout.view_product, parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductViewHolder) {
            val item = items[holder.adapterPosition]
            holder.bind(item)
        }
    }

    inner class ProductViewHolder(private val binding: ViewProductBinding)
        : BaseViewHolder<ProductCategoryPair>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: ProductCategoryPair) {
            layout_content.click {
                listener.onSelectItem(item)
            }

            layout_content.setOnLongClickListener {
                listener.onLongSelectItem(item)
                true
            }

            item.product.let { product ->
                if (product.enableStockTracking) {
                    if (product.availableStock > 0) {
                        text_view_stock.text = context.getString(R.string.label_stock_placeholder, product.availableStock.toString())
                        text_view_stock.textColor = context.findColor(R.color.colorTVSecondary)
                    } else {
                        text_view_stock.setText(R.string.label_out_of_stock)
                        text_view_stock.textColor = context.findColor(android.R.color.holo_red_light)
                    }
                } else {
                    text_view_stock.text = context.getString(R.string.label_stock_placeholder, "∞")
                    text_view_stock.textColor = context.findColor(R.color.colorTVSecondary)
                }

                val textDrawable = TextDrawable.builder()
                    .buildRound(product.productName.substring(0, 1), Color.parseColor("#CDCED8"))
                image_view_product.isDisableCircularTransformation = false
                image_view_product.setImageDrawable(textDrawable)
            }
        }

        override fun onClick(view: View, item: ProductCategoryPair?) {
        }
    }
}