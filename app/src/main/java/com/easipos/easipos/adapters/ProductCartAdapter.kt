package com.easipos.easipos.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easipos.R
import com.easipos.easipos.databinding.ViewProductCartBinding
import com.easipos.easipos.models.ProductCart
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product_cart.*

class ProductCartAdapter(context: Context,
                         private val listener: OnGestureDetectedListener)
    : BaseRecyclerViewAdapter<ProductCart>(context) {

    interface OnGestureDetectedListener {
        fun onSelectItem(position: Int, item: ProductCart)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductCartBinding>(
            layoutInflater, R.layout.view_product_cart, parent, false)
        return ProductCartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductCartViewHolder) {
            val item = items[holder.adapterPosition]
            holder.bind(item)
        }
    }

    inner class ProductCartViewHolder(private val binding: ViewProductCartBinding)
        : BaseViewHolder<ProductCart>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: ProductCart) {
            layout_content.click {
                listener.onSelectItem(adapterPosition, item)
            }
        }

        override fun onClick(view: View, item: ProductCart?) {
        }
    }
}