package com.easipos.easipos.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easipos.R
import com.easipos.easipos.databinding.ViewProductCartReceiptBinding
import com.easipos.easipos.models.ProductCart
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class ProductCartReceiptAdapter(context: Context)
    : BaseRecyclerViewAdapter<ProductCart>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductCartReceiptBinding>(
            layoutInflater, R.layout.view_product_cart_receipt, parent, false)
        return ProductCartReceiptHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductCartReceiptHolder) {
            val item = items[holder.adapterPosition]
            holder.bind(item)
        }
    }

    inner class ProductCartReceiptHolder(private val binding: ViewProductCartReceiptBinding)
        : BaseViewHolder<ProductCart>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: ProductCart) {
        }

        override fun onClick(view: View, item: ProductCart?) {
        }
    }
}