package com.easipos.easipos.api

object ApiEndpoint {

    const val CHECK_VERSION = "ddcard/version/check"

    const val REGISTER_REMOVE_JPUSH_REG_ID = "ddcard/apptoken"
}
