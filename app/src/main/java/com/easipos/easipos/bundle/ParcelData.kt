package com.easipos.easipos.bundle

object ParcelData {

    const val CLEAR_DB = "clear_db"
    const val URL = "url"
    const val BARCODE_VALUE = "BARCODE_VALUE"
    const val PRODUCT_CATEGORY_PAIR = "PRODUCT_CATEGORY_PAIR"
    const val CATEGORY = "CATEGORY"
    const val PRODUCT = "PRODUCT"
    const val PAYMENT = "PAYMENT"
    const val DRAWER_CASH_AMOUNT = "DRAWER_CASH_AMOUNT"
    const val SALES_REPORT = "SALES_REPORT"
}