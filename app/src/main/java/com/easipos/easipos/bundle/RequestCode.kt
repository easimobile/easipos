package com.easipos.easipos.bundle

object RequestCode {

    const val SCAN_BARCODE = 1001
    const val PRODUCT_CATEGORY = 1002
    const val ADD_PRODUCT_CATEGORY = 1003
    const val PRODUCT_STOCK_PRICE = 1004
    const val PAYMENT = 1005
    const val PAYMENT_RESULT = 1006
    const val DAY_END_REPORT = 1007
}