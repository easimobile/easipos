package com.easipos.easipos.constant

enum class CashlessType {
    GRAB_PAY,
    WECHAT_PAY,
    ALIPAY
}

fun parseCashlessType(value: String): CashlessType? {
    return when (value) {
        "GRAB_PAY" -> CashlessType.GRAB_PAY
        "WECHAT_PAY" -> CashlessType.WECHAT_PAY
        "ALIPAY" -> CashlessType.ALIPAY
        else -> null
    }
}