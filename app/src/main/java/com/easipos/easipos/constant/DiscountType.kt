package com.easipos.easipos.constant

enum class DiscountType {
    CURRENCY,
    PERCENT
}

fun parseDiscountType(value: String): DiscountType? {
    return when (value) {
        "CURRENCY" -> DiscountType.CURRENCY
        "PERCENT" -> DiscountType.PERCENT
        else -> null
    }
}