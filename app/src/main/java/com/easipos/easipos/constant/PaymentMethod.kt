package com.easipos.easipos.constant

enum class PaymentMethod {
    CASH,
    CASHLESS
}

fun parsePaymentMethod(value: String): PaymentMethod? {
    return when (value) {
        "CASH" -> PaymentMethod.CASH
        "CASHLESS" -> PaymentMethod.CASHLESS
        else -> null
    }
}