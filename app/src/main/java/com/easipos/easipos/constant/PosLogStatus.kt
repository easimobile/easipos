package com.easipos.easipos.constant

enum class PosLogStatus(val string: String) {
    // For shift
    SHIFT_OPEN("OPEN"),
    SHIFT_CLOSE("CLOSE"),

    // For cash declaration
    CASH_DECLARATION_TALLY("TALLY"),
    CASH_DECLARATION_SHORT("SHORT"),
    CASH_DECLARATION_EXCESS("EXCESS")
}