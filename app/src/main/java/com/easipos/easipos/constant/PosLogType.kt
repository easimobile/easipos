package com.easipos.easipos.constant

enum class PosLogType {
    ADD_FLOAT,
    SHIFT,
    CASH_DECLARATION
}