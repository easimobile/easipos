package com.easipos.easipos.constant

enum class TaxType {
    SST,
    GST
}

fun parseTaxType(value: String): TaxType? {
    return when (value) {
        "SST" -> TaxType.SST
        "GST" -> TaxType.GST
        else -> null
    }
}