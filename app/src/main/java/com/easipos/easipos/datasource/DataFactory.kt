package com.easipos.easipos.datasource

import android.app.Application
import com.easipos.easipos.api.services.Api
import com.easipos.easipos.datasource.log.LogDataSource
import com.easipos.easipos.datasource.log.LogDataStore
import com.easipos.easipos.datasource.notification.NotificationDataSource
import com.easipos.easipos.datasource.notification.NotificationDataStore
import com.easipos.easipos.datasource.precheck.PrecheckDataSource
import com.easipos.easipos.datasource.precheck.PrecheckDataStore
import com.easipos.easipos.datasource.report.ReportDataSource
import com.easipos.easipos.datasource.report.ReportDataStore
import com.easipos.easipos.datasource.sales.SalesDataSource
import com.easipos.easipos.datasource.sales.SalesDataStore
import com.easipos.easipos.datasource.user.UserDataSource
import com.easipos.easipos.datasource.user.UserDataStore
import com.easipos.easipos.executor.PostExecutionThread
import com.easipos.easipos.executor.ThreadExecutor
import com.easipos.easipos.room.RoomService

class DataFactory(private val application: Application,
                  private val api: Api,
                  private val roomService: RoomService,
                  private val threadExecutor: ThreadExecutor,
                  private val postExecutionThread: PostExecutionThread) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api, threadExecutor, postExecutionThread)

    fun createUserDataSource(): UserDataStore =
        UserDataSource(api, roomService, threadExecutor, postExecutionThread)

    fun createSalesDataSource(): SalesDataStore =
        SalesDataSource(api, roomService, threadExecutor, postExecutionThread)

    fun createLogDataSource(): LogDataStore =
        LogDataSource(api, roomService, threadExecutor, postExecutionThread)

    fun createReportDataSource(): ReportDataStore =
        ReportDataSource(api, roomService, threadExecutor, postExecutionThread)

    fun createNotificationDataSource(): NotificationDataStore =
        NotificationDataSource(api, threadExecutor, postExecutionThread)
}
