package com.easipos.easipos.datasource.log

import android.os.AsyncTask
import com.easipos.easipos.api.services.Api
import com.easipos.easipos.constant.PosLogStatus
import com.easipos.easipos.constant.PosLogType
import com.easipos.easipos.executor.PostExecutionThread
import com.easipos.easipos.executor.ThreadExecutor
import com.easipos.easipos.models.PosLog
import com.easipos.easipos.room.RoomService
import com.easipos.easipos.use_cases.complete
import io.reactivex.Observable

class LogDataSource(private val api: Api,
                    private val roomService: RoomService,
                    private val threadExecutor: ThreadExecutor,
                    private val postExecutionThread: PostExecutionThread) : LogDataStore {

    override fun addLog(posLog: PosLog): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        this.posLogDao().insertPosLog(
                            posLog = posLog
                        )

                        emitter.complete()
                    }
                }
            }
        }

    override fun addOpenShiftLog(shift: Int): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val existingPosLog = this.posLogDao().findPosLogByShift(shift)
                        if (existingPosLog == null) {
                            val newPosLog = PosLog(
                                type = PosLogType.SHIFT.toString(),
                                shift = shift,
                                status = PosLogStatus.SHIFT_OPEN.string
                            )

                            this.posLogDao().insertPosLog(
                                posLog = newPosLog
                            )
                        }

                        emitter.complete()
                    }
                }
            }
        }

    override fun addCloseShiftLog(shift: Int): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        this.posLogDao().findPosLogByShift(shift)?.let { posLog ->
                            posLog.status = PosLogStatus.SHIFT_CLOSE.string

                            this.posLogDao().updatePosLog(posLog)
                        }

                        emitter.complete()
                    }
                }
            }
        }

    override fun clearLog(): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        this.posLogDao().clearLog()

                        emitter.complete()
                    }
                }
            }
        }
}
