package com.easipos.easipos.datasource.log

import com.easipos.easipos.models.PosLog
import io.reactivex.Observable

interface LogDataStore {

    fun addLog(posLog: PosLog): Observable<Void>

    fun addOpenShiftLog(shift: Int): Observable<Void>

    fun addCloseShiftLog(shift: Int): Observable<Void>

    fun clearLog(): Observable<Void>
}
