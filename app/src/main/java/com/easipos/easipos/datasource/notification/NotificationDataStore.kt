package com.easipos.easipos.datasource.notification

import com.easipos.easipos.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.easipos.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationDataStore {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
