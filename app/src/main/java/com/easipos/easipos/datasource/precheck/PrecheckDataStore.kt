package com.easipos.easipos.datasource.precheck

import com.easipos.easipos.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckDataStore {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
