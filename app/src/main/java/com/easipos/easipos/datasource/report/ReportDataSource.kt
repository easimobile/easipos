package com.easipos.easipos.datasource.report

import android.os.AsyncTask
import com.easipos.easipos.api.services.Api
import com.easipos.easipos.constant.PaymentMethod
import com.easipos.easipos.executor.PostExecutionThread
import com.easipos.easipos.executor.ThreadExecutor
import com.easipos.easipos.models.*
import com.easipos.easipos.room.RoomService
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.use_cases.success
import io.github.anderscheow.library.kotlinExt.formatDate
import io.reactivex.Single
import java.util.*

class ReportDataSource(private val api: Api,
                       private val roomService: RoomService,
                       private val threadExecutor: ThreadExecutor,
                       private val postExecutionThread: PostExecutionThread) : ReportDataStore {

    override fun getShiftCloseReport(): Single<ShiftCloseReport> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val shift = Preference.prefCurrentShift
                        val businessDate = Calendar.getInstance().apply {
                            this.timeInMillis = Preference.prefDayStartBusinessDate
                        }
                        val businessDateInString = businessDate.timeInMillis.formatDate("yyyy-MM-dd")
                        val payments = this.paymentDao().findTodayPaymentsByShiftAndNotDayEnded(
                            shift = shift,
                            businessDate = businessDateInString
                        )
                        val float = Preference.prefCurrentFloat

                        val report = ShiftCloseReport(
                            businessDate = businessDate.timeInMillis,
                            payments = payments,
                            float = float,
                            shift = shift
                        )

                        emitter.success(report)
                    }
                }
            }
        }

    override fun getDayEndReport(): Single<DayEndReport> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val businessDate = Calendar.getInstance().apply {
                            this.timeInMillis = Preference.prefDayStartBusinessDate
                        }
                        val businessDateInString = businessDate.timeInMillis.formatDate("yyyy-MM-dd")
                        val payments = this.paymentDao().findTodayPaymentsNotDayEnded(
                            businessDate = businessDateInString
                        )

                        val totalFloat = this.posLogDao().findTotalFloatByAddFloatType()
                        val totalShift = this.posLogDao().findTotalShiftByShiftType()

                        val report = DayEndReport(
                            businessDate = businessDate.timeInMillis,
                            payments = payments,
                            totalFloat = totalFloat,
                            numberOfShift = totalShift,
                            cashCollections = this.posLogDao().findPosLogByCashDeclarationType()
                                .map { log ->
                                    val cashPayments = payments.filter {
                                        it.paymentMethod == PaymentMethod.CASH && it.shift == log.shift
                                    }

                                    val float = this.posLogDao().findFloatAmountByShiftAndAddFloatType(
                                        shift = log.shift ?: 0
                                    )

                                    val collect = if (cashPayments.isNotEmpty()) {
                                        cashPayments
                                            .map { it.payableAmount }
                                            .reduce { acc, d -> acc + d }
                                    } else 0.0

                                    CashCollection(
                                        shift = log.shift ?: 0,
                                        collect = collect,
                                        float = float,
                                        declare = log.amount ?: 0.0
                                    )
                                }
                        )

                        emitter.success(report)
                    }
                }
            }
        }

    override fun getSalesReport(fromDate: String, toDate: String): Single<SalesReport> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val payments = this.paymentDao().findPaymentsByPaymentDateTimeRange(
                            fromDate = fromDate,
                            toDate = toDate
                        )

                        val salesReport = SalesReport(
                            fromDate = fromDate,
                            toDate = toDate,
                            payments = payments
                        )

                        emitter.success(salesReport)
                    }
                }
            }
        }
}
