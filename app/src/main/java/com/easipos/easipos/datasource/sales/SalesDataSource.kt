package com.easipos.easipos.datasource.sales

import android.os.AsyncTask
import com.easipos.easipos.api.services.Api
import com.easipos.easipos.executor.PostExecutionThread
import com.easipos.easipos.executor.ThreadExecutor
import com.easipos.easipos.models.*
import com.easipos.easipos.room.RoomService
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.use_cases.complete
import com.easipos.easipos.use_cases.error
import com.easipos.easipos.use_cases.success
import io.github.anderscheow.library.kotlinExt.formatDate
import io.reactivex.Observable
import io.reactivex.Single
import java.util.*

class SalesDataSource(private val api: Api,
                      private val roomService: RoomService,
                      private val threadExecutor: ThreadExecutor,
                      private val postExecutionThread: PostExecutionThread) : SalesDataStore {

    override fun addDefaultCategory(): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        if (this.categoryDao().getDefaultCategoryCount() < 1) {
                            this.categoryDao().insertCategory(
                                category = Category(
                                    categoryId = 1,
                                    name = "General",
                                    isRemovable = false
                                )
                            )
                        }

                        emitter.complete()
                    }
                }
            }
        }

    override fun addCategory(category: Category): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        this.categoryDao().insertCategory(
                            category = category
                        )

                        emitter.complete()
                    }
                }
            }
        }

    override fun getCategories(): Single<List<Category>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val categories = this.categoryDao().findCategories()

                        emitter.success(categories)
                    }
                }
            }
        }

    override fun removeCategory(category: Category): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        this.productCategoryDao().updateProductCategoryToDefault(
                            categoryId = category.categoryId
                        )
                        this.categoryDao().deleteCategory(category)

                        emitter.complete()
                    }
                }
            }
        }

    override fun addProduct(productCategoryPair: ProductCategoryPair): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val existingProduct = this.productDao().findProductByProductCode(
                            productCode = productCategoryPair.product.productCode
                        )

                        if (existingProduct != null) {
                            emitter.error(Throwable("Product code existed."))
                            return@runInTransaction
                        }

                        val productId = this.productDao().insertProduct(
                            product = productCategoryPair.product
                        )
                        val categoryId = productCategoryPair.category.categoryId
                        val ref = ProductCategoryRef(productId, categoryId)

                        this.productCategoryDao().insert(
                            ref = ref
                        )

                        emitter.complete()
                    }
                }
            }
        }

    override fun getProducts(): Single<List<ProductCategoryPair>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val products = this.productCategoryDao().findProducts()

                        emitter.success(products)
                    }
                }
            }
        }

    override fun getProductsByCategory(category: Category): Single<List<ProductCategoryPair>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val categoryProductPair = this.productCategoryDao().findProductsByCategory(
                            categoryId = category.categoryId
                        )

                        if (categoryProductPair == null) {
                            emitter.success(emptyList())
                        } else {
                            val products = this.productCategoryDao().findProductByProductIds(
                                productIds = categoryProductPair.products.map { it.productId }
                            )

                            emitter.success(products)
                        }
                    }
                }
            }
        }

    override fun getProductsBySearch(searchable: String): Single<List<ProductCategoryPair>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val products = this.productCategoryDao().findProductsBySearch(searchable)

                        emitter.success(products)
                    }
                }
            }
        }

    override fun getProductByBarcode(barcode: String): Single<ProductCategoryPair> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val product = this.productCategoryDao().findProductByBarcode(
                            barcode = barcode
                        )

                        if (product == null) {
                            emitter.error(Throwable("Product not found"))
                        } else {
                            emitter.success(product)
                        }
                    }
                }
            }
        }

    override fun updateProduct(product: Product): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        this.productDao().updateProduct(
                            product = product
                        )

                        emitter.complete()
                    }
                }
            }
        }

    override fun updateProductCategory(productCategoryPair: ProductCategoryPair, category: Category): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        this.productCategoryDao().updateProductCategory(
                            productId = productCategoryPair.product.productId,
                            originalCategoryId = productCategoryPair.category.categoryId,
                            toChangeCategoryId = category.categoryId
                        )

                        emitter.complete()
                    }
                }
            }
        }

    override fun updateProductsAvailableStock(productCarts: List<ProductCart>): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        productCarts.forEach { productCart ->
                            val productId = productCart.product.product.productId
                            val product = this.productDao().findProductByProductId(productId)

                            if (product != null) {
                                val newAvailableStock = product.availableStock - productCart.quantity
                                this.productDao().updateProductAvailableStock(productId, newAvailableStock)
                            }
                        }

                        emitter.complete()
                    }
                }
            }
        }

    override fun deleteProduct(product: Product): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        this.productCategoryDao().removeProductById(
                            productId = product.productId
                        )
                        this.productDao().deleteProduct(product)

                        emitter.complete()
                    }
                }
            }
        }

    override fun addPayment(payment: Payment): Single<Long> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val paymentId = this.paymentDao().insertPayment(
                            payment = payment
                        )

                        emitter.success(paymentId)
                    }
                }
            }
        }

    override fun getPayment(paymentId: Long): Single<Payment> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val payment = this.paymentDao().findPaymentByPaymentId(
                            paymentId = paymentId
                        )

                        emitter.success(payment)
                    }
                }
            }
        }

    override fun getTodaySales(): Single<List<Payment>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val businessDate = Calendar.getInstance().apply {
                            this.timeInMillis = Preference.prefDayStartBusinessDate
                        }
                        val businessDateInString = businessDate.timeInMillis.formatDate("yyyy-MM-dd")
                        val payments = this.paymentDao().findTodayPaymentsNotDayEnded(
                            businessDate = businessDateInString
                        )

                        emitter.success(payments)
                    }
                }
            }
        }

    override fun getTodaySalesByShift(): Single<List<Payment>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val businessDate = Calendar.getInstance().apply {
                            this.timeInMillis = Preference.prefDayStartBusinessDate
                        }
                        val businessDateInString = businessDate.timeInMillis.formatDate("yyyy-MM-dd")
                        val payments = this.paymentDao().findTodayPaymentsByShiftAndNotDayEnded(
                            shift = Preference.prefCurrentShift,
                            businessDate = businessDateInString
                        )

                        emitter.success(payments)
                    }
                }
            }
        }

    override fun getTotalDrawerCashAmountByShift(): Single<DrawerCashAmount> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val businessDate = Calendar.getInstance().apply {
                            this.timeInMillis = Preference.prefDayStartBusinessDate
                        }
                        val businessDateInString = businessDate.timeInMillis.formatDate("yyyy-MM-dd")
                        val totalCashAmount = this.paymentDao().findTodayPaymentsByShiftAndCashPaymentMethodAndNotDayEnded(
                            shift = Preference.prefCurrentShift,
                            businessDate = businessDateInString
                        )
                        val shiftFloat = this.posLogDao().findFloatAmountByShiftAndAddFloatType(
                            shift = Preference.prefCurrentShift
                        )

                        val drawerCashAmount = DrawerCashAmount(
                            cashAmount = totalCashAmount,
                            float = shiftFloat,
                            shift = Preference.prefCurrentShift
                        )

                        emitter.success(drawerCashAmount)
                    }
                }
            }
        }

    override fun updateTodaySalesDayEnded(): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val businessDate = Calendar.getInstance().apply {
                            this.timeInMillis = Preference.prefDayStartBusinessDate
                        }
                        val businessDateInString = businessDate.timeInMillis.formatDate("yyyy-MM-dd")
                        this.paymentDao().updatePaymentIsDayEnded(businessDateInString)

                        emitter.complete()
                    }
                }
            }
        }
}
