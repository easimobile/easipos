package com.easipos.easipos.datasource.sales

import com.easipos.easipos.models.*
import io.reactivex.Observable
import io.reactivex.Single

interface SalesDataStore {

    fun addDefaultCategory(): Observable<Void>

    fun addCategory(category: Category): Observable<Void>

    fun getCategories(): Single<List<Category>>

    fun removeCategory(category: Category): Observable<Void>

    fun addProduct(productCategoryPair: ProductCategoryPair): Observable<Void>

    fun getProducts(): Single<List<ProductCategoryPair>>

    fun getProductsByCategory(category: Category): Single<List<ProductCategoryPair>>

    fun getProductsBySearch(searchable: String): Single<List<ProductCategoryPair>>

    fun getProductByBarcode(barcode: String): Single<ProductCategoryPair>

    fun updateProduct(product: Product): Observable<Void>

    fun updateProductCategory(productCategoryPair: ProductCategoryPair, category: Category): Observable<Void>

    fun updateProductsAvailableStock(productCarts: List<ProductCart>): Observable<Void>

    fun deleteProduct(product: Product): Observable<Void>

    fun addPayment(payment: Payment): Single<Long>

    fun getPayment(paymentId: Long): Single<Payment>

    fun getTodaySales(): Single<List<Payment>>

    fun getTodaySalesByShift(): Single<List<Payment>>

    fun getTotalDrawerCashAmountByShift(): Single<DrawerCashAmount>

    fun updateTodaySalesDayEnded(): Observable<Void>
}
