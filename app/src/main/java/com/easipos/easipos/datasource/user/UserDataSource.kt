package com.easipos.easipos.datasource.user

import android.os.AsyncTask
import com.easipos.easipos.api.services.Api
import com.easipos.easipos.executor.PostExecutionThread
import com.easipos.easipos.executor.ThreadExecutor
import com.easipos.easipos.models.User
import com.easipos.easipos.room.RoomService
import com.easipos.easipos.use_cases.error
import com.easipos.easipos.use_cases.success
import io.reactivex.Single

class UserDataSource(private val api: Api,
                     private val roomService: RoomService,
                     private val threadExecutor: ThreadExecutor,
                     private val postExecutionThread: PostExecutionThread) : UserDataStore {

    override fun login(username: String, password: String): Single<User> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val user = this.userDao().findUser(
                            username = username,
                            password = password
                        )

                        if (user == null) {
                            emitter.error(Throwable("Invalid username or password."))
                            return@runInTransaction
                        }

                        emitter.success(user)
                    }
                }
            }
        }

    override fun register(user: User): Single<Long> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.apply {
                    this.runInTransaction {
                        val existingUser = this.userDao().findUser(
                            username = user.username
                        )

                        if (existingUser != null) {
                            emitter.error(Throwable("Username has already been used."))
                            return@runInTransaction
                        }

                        val userId = this.userDao().insertUser(user)

                        emitter.success(userId)
                    }
                }
            }
        }
}
