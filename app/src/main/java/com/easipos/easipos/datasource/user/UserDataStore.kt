package com.easipos.easipos.datasource.user

import com.easipos.easipos.models.User
import io.reactivex.Single

interface UserDataStore {

    fun login(username: String, password: String): Single<User>

    fun register(user: User): Single<Long>
}
