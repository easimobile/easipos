package com.easipos.easipos.di.modules

import com.easipos.easipos.activities.add_edit_product.navigation.AddEditProductNavigation
import com.easipos.easipos.activities.add_edit_product.navigation.AddEditProductNavigationImpl
import com.easipos.easipos.activities.day_end.navigation.DayEndNavigation
import com.easipos.easipos.activities.day_end.navigation.DayEndNavigationImpl
import com.easipos.easipos.activities.login.navigation.LoginNavigation
import com.easipos.easipos.activities.login.navigation.LoginNavigationImpl
import com.easipos.easipos.activities.main.navigation.MainNavigation
import com.easipos.easipos.activities.main.navigation.MainNavigationImpl
import com.easipos.easipos.activities.order_details.navigation.OrderDetailsNavigation
import com.easipos.easipos.activities.order_details.navigation.OrderDetailsNavigationImpl
import com.easipos.easipos.activities.payment.navigation.PaymentNavigation
import com.easipos.easipos.activities.payment.navigation.PaymentNavigationImpl
import com.easipos.easipos.activities.payment_result.navigation.PaymentResultNavigation
import com.easipos.easipos.activities.payment_result.navigation.PaymentResultNavigationImpl
import com.easipos.easipos.activities.product_category.navigation.ProductCategoryNavigation
import com.easipos.easipos.activities.product_category.navigation.ProductCategoryNavigationImpl
import com.easipos.easipos.activities.register.navigation.RegisterNavigation
import com.easipos.easipos.activities.register.navigation.RegisterNavigationImpl
import com.easipos.easipos.activities.report.navigation.ReportNavigation
import com.easipos.easipos.activities.report.navigation.ReportNavigationImpl
import com.easipos.easipos.activities.sales.navigation.SalesNavigation
import com.easipos.easipos.activities.sales.navigation.SalesNavigationImpl
import com.easipos.easipos.activities.sales_report.navigation.SalesReportNavigation
import com.easipos.easipos.activities.sales_report.navigation.SalesReportNavigationImpl
import com.easipos.easipos.activities.splash.navigation.SplashNavigation
import com.easipos.easipos.activities.splash.navigation.SplashNavigationImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

fun provideActivityModule() = Kodein.Module("activityModule") {
    bind<SplashNavigation>() with provider { SplashNavigationImpl() }
    bind<LoginNavigation>() with provider { LoginNavigationImpl() }
    bind<RegisterNavigation>() with provider { RegisterNavigationImpl() }
    bind<MainNavigation>() with provider { MainNavigationImpl() }
    bind<DayEndNavigation>() with provider { DayEndNavigationImpl() }
    bind<SalesNavigation>() with provider { SalesNavigationImpl() }
    bind<AddEditProductNavigation>() with provider { AddEditProductNavigationImpl() }
    bind<ProductCategoryNavigation>() with provider { ProductCategoryNavigationImpl() }
    bind<OrderDetailsNavigation>() with provider { OrderDetailsNavigationImpl() }
    bind<PaymentNavigation>() with provider { PaymentNavigationImpl() }
    bind<PaymentResultNavigation>() with provider { PaymentResultNavigationImpl() }
    bind<ReportNavigation>() with provider { ReportNavigationImpl() }
    bind<SalesReportNavigation>() with provider { SalesReportNavigationImpl() }
}
