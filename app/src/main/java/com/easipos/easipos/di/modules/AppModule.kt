package com.easipos.easipos.di.modules

import android.app.Application
import androidx.room.Room
import com.easipos.easipos.Easi
import com.easipos.easipos.api.misc.AuthInterceptor
import com.easipos.easipos.api.misc.AuthOkhttpClient
import com.easipos.easipos.api.misc.TokenAuthenticator
import com.easipos.easipos.api.services.Api
import com.easipos.easipos.datasource.DataFactory
import com.easipos.easipos.executor.*
import com.easipos.easipos.managers.FcmManager
import com.easipos.easipos.managers.PushNotificationManager
import com.easipos.easipos.repositories.log.LogDataRepository
import com.easipos.easipos.repositories.log.LogRepository
import com.easipos.easipos.repositories.notification.NotificationDataRepository
import com.easipos.easipos.repositories.notification.NotificationRepository
import com.easipos.easipos.repositories.precheck.PrecheckDataRepository
import com.easipos.easipos.repositories.precheck.PrecheckRepository
import com.easipos.easipos.repositories.report.ReportDataRepository
import com.easipos.easipos.repositories.report.ReportRepository
import com.easipos.easipos.repositories.sales.SalesDataRepository
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.repositories.user.UserDataRepository
import com.easipos.easipos.repositories.user.UserRepository
import com.easipos.easipos.room.*
import com.easipos.easipos.services.FcmService
import com.easipos.easipos.services.PushNotificationService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import io.github.anderscheow.validator.Validator
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.eagerSingleton
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

fun provideCommonModule(easi: Easi) = Kodein.Module("commonModule") {
    bind<Easi>() with singleton { instance<Application>() as Easi }
    bind<Validator>() with singleton { Validator.with(easi) }
    bind<PushNotificationManager>() with singleton { PushNotificationManager(PushNotificationService()) }
    bind<FcmManager>() with singleton { FcmManager(FcmService(easi)) }
    bind<ThreadExecutor>() with singleton { JobExecutor() }
    bind<PostExecutionThread>() with singleton { UIThread() }
    bind<DiskIOExecutor>() with singleton { IOExecutor() }

    bind<DataFactory>() with singleton {
        DataFactory(instance(), instance(), instance(), instance(), instance())
    }

    bind<PrecheckRepository>() with singleton { PrecheckDataRepository(instance()) }
    bind<UserRepository>() with singleton { UserDataRepository(instance()) }
    bind<SalesRepository>() with singleton { SalesDataRepository(instance()) }
    bind<LogRepository>() with singleton { LogDataRepository(instance()) }
    bind<ReportRepository>() with singleton { ReportDataRepository(instance()) }
    bind<NotificationRepository>() with singleton {
        NotificationDataRepository(instance())
    }
}

fun provideApiModule(userAgent: String, endpoint: String, authorisation: String) = Kodein.Module("apiModule") {
    bind<AuthInterceptor>() with singleton { AuthInterceptor(userAgent, authorisation) }
    bind<TokenAuthenticator>() with singleton { TokenAuthenticator(instance()) }
    bind<AuthOkhttpClient>() with singleton { AuthOkhttpClient(instance(), instance()) }
    bind<RxJava2CallAdapterFactory>() with singleton { RxJava2CallAdapterFactory.create() }
    bind<GsonConverterFactory>() with singleton {
        val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
            .create()
        GsonConverterFactory.create(gson)
    }
    bind<Api>() with singleton {
        Retrofit.Builder()
            .baseUrl(endpoint)
            .client(instance<AuthOkhttpClient>().getAuthOkhttpClient())
            .addCallAdapterFactory(instance())
            .addConverterFactory(instance())
            .build()
            .create(Api::class.java)
    }
}

fun provideDatabaseModule(easi: Easi, dbName: String) = Kodein.Module("databaseModule") {
    bind<RoomService>() with eagerSingleton {
        Room.databaseBuilder(easi.applicationContext,
            RoomService::class.java, dbName)
            .addMigrations(MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5, MIGRATION_5_6,
                MIGRATION_6_7, MIGRATION_7_8, MIGRATION_8_9)
            .build()
    }
}