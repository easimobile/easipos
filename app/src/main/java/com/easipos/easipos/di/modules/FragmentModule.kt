package com.easipos.easipos.di.modules

import org.kodein.di.Kodein

fun provideFragmentModule() = Kodein.Module("fragmentModule") {
}
