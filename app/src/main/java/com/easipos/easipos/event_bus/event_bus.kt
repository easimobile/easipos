package com.easipos.easipos.event_bus

data class NotificationCount(val count: Int)