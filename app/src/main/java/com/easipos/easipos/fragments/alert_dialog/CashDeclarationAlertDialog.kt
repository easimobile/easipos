package com.easipos.easipos.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog
import com.easipos.easipos.R
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_cash_declaration.*

class CashDeclarationAlertDialog(context: Context,
                                 private val onRetry: () -> Unit,
                                 private val onPost: () -> Unit) : AlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_cash_declaration)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        button_retry.click {
            onRetry()
            dismiss()
        }

        button_post.click {
            onPost()
            dismiss()
        }
    }
}