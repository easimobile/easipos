package com.easipos.easipos.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog
import com.easipos.easipos.R
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_delete_product_confirmation.*

class DeleteProductConfirmationAlertDialog(context: Context,
                                           private val onYes: () -> Unit) : AlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_delete_product_confirmation)
        setCancelable(false)
        setCanceledOnTouchOutside(false)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        button_no.click {
            dismiss()
        }

        button_yes.click {
            onYes.invoke()
            dismiss()
        }
    }
}