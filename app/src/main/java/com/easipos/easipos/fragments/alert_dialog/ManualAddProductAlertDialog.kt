package com.easipos.easipos.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.easipos.easipos.R
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.showKeyboard
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.alert_dialog_manual_add_product.*


class ManualAddProductAlertDialog(context: Context,
                                  private val onAdd: (String) -> Unit) : AlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_manual_add_product)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        text_input_edit_text_barcode.post {
            text_input_edit_text_barcode.requestFocus()
            context.showKeyboard(text_input_edit_text_barcode)
        }

        button_cancel.click {
            dismiss()
        }

        button_add.click {
            attemptManualAddProduct()
        }
    }

    private fun attemptManualAddProduct() {
        val barcodeValidation = Validation(text_input_layout_barcode)
            .add(NotBlankRule(R.string.error_field_required))

        Validator.with(context)
            .setListener(object : Validator.OnValidateListener {
                override fun onValidateFailed(errors: List<String>) {
                }

                override fun onValidateSuccess(values: List<String>) {
                    val barcode = values[0].trim()

                    onAdd.invoke(barcode)
                    dismiss()
                }
            }).validate(barcodeValidation)
    }
}