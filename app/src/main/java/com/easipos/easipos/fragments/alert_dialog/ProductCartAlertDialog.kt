package com.easipos.easipos.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog
import com.easipos.easipos.R
import com.easipos.easipos.models.ProductCart
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_product_cart.*

class ProductCartAlertDialog(context: Context,
                             private val productCart: ProductCart,
                             private val onRemove: () -> Unit,
                             private val onConfirm: (Int) -> Unit) : AlertDialog(context) {

    private var currentQuantity = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_product_cart)
        setCancelable(false)
        setCanceledOnTouchOutside(false)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        currentQuantity = productCart.quantity

        text_view_quantity.text = currentQuantity.toString()

        button_minus.click {
            if (currentQuantity > 1) {
                currentQuantity -= 1
                text_view_quantity.text = currentQuantity.toString()
            }
        }

        button_plus.click {
            if (currentQuantity < 99) {
                currentQuantity += 1
                text_view_quantity.text = currentQuantity.toString()
            }
        }

        card_view_remove.click {
            onRemove()
            dismiss()
        }

        button_cancel.click {
            dismiss()
        }

        button_confirm.click {
            onConfirm(currentQuantity)
            dismiss()
        }
    }
}