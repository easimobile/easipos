package com.easipos.easipos.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog
import com.easipos.easipos.R
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_yes_no.*

class YesNoAlertDialog(context: Context,
                       private val message: CharSequence,
                       private val onNo: () -> Unit,
                       private val onYes: () -> Unit) : AlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_yes_no)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        text_view_message.text = message

        button_no.click {
            onNo()
            dismiss()
        }

        button_yes.click {
            onYes()
            dismiss()
        }
    }
}