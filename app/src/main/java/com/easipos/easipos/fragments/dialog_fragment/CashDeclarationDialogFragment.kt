package com.easipos.easipos.fragments.dialog_fragment

import android.os.Bundle
import android.view.ViewGroup
import com.easipos.easipos.R
import com.easipos.easipos.activities.day_end.DayEndActivity
import com.easipos.easipos.base.CustomBaseDialogFragment
import com.easipos.easipos.bundle.ParcelData
import com.easipos.easipos.constant.PosLogStatus
import com.easipos.easipos.fragments.alert_dialog.CashDeclarationAlertDialog
import com.easipos.easipos.models.DrawerCashAmount
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.dialog_fragment_cash_declaration.*
import org.jetbrains.anko.displayMetrics

class CashDeclarationDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun newInstance(drawerCashAmount: DrawerCashAmount): CashDeclarationDialogFragment {
            return CashDeclarationDialogFragment().apply {
                this.arguments = Bundle().apply {
                    this.putParcelable(ParcelData.DRAWER_CASH_AMOUNT, drawerCashAmount)
                }
            }
        }
    }

    private val drawerCashAmount by argument<DrawerCashAmount>(ParcelData.DRAWER_CASH_AMOUNT)

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_cash_declaration

    override fun init() {
        super.init()

        text_input_edit_text_cash_in_drawer.post {
            text_input_edit_text_cash_in_drawer.requestFocus()
            context?.showKeyboard(text_input_edit_text_cash_in_drawer)
        }

        text_input_edit_text_cash_in_drawer.setText("0")

        button_cancel.click {
            dismiss()
        }

        button_continue.click {
            attemptPerformCashDeclaration()
        }
    }

    private fun attemptPerformCashDeclaration() {
        when (val drawerAmount = text_input_edit_text_cash_in_drawer.getDoubleOrNull()) {
            null -> {
                text_input_layout_cash_in_drawer.isErrorEnabled = true
                text_input_layout_cash_in_drawer.error =
                    getString(R.string.error_cash_in_drawer_wrong_format)
                return
            }
            else -> {
                text_input_layout_cash_in_drawer.isErrorEnabled = false

                Logger.d("$drawerCashAmount, $drawerAmount")

                val totalCashAmount = drawerCashAmount?.getTotalCashAmount() ?: 0.0
                if (drawerAmount != totalCashAmount) {
                    showCashDeclarationAlertDialog(drawerAmount)
                } else {
                    (activity as? DayEndActivity)?.completeCashDeclaration(drawerAmount, PosLogStatus.CASH_DECLARATION_TALLY.string)
                    dismissAllowingStateLoss()
                }
            }
        }
    }

    private fun showCashDeclarationAlertDialog(drawerAmount: Double) {
        withContext { context ->
            CashDeclarationAlertDialog(context, onRetry = {
            }, onPost = {
                val totalCashAmount = drawerCashAmount?.getTotalCashAmount() ?: 0.0
                if (totalCashAmount < drawerAmount) {
                    (activity as? DayEndActivity)?.completeCashDeclaration(drawerAmount, PosLogStatus.CASH_DECLARATION_EXCESS.string)
                } else {
                    (activity as? DayEndActivity)?.completeCashDeclaration(drawerAmount, PosLogStatus.CASH_DECLARATION_SHORT.string)
                }
                dismissAllowingStateLoss()
            }).show()
        }
    }
}