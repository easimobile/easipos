package com.easipos.easipos.fragments.dialog_fragment

import android.graphics.Color
import android.view.ViewGroup
import com.easipos.easipos.R
import com.easipos.easipos.activities.main.MainActivity
import com.easipos.easipos.base.CustomBaseDialogFragment
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.dialog_fragment_day_start.*
import org.jetbrains.anko.displayMetrics
import java.util.*

class DayStartDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun newInstance(): DayStartDialogFragment {
            return DayStartDialogFragment()
        }
    }

    private val currentCalendar = Calendar.getInstance()

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_day_start

    override fun init() {
        super.init()

        text_input_edit_text_add_float.post {
            text_input_edit_text_add_float.requestFocus()
            context?.showKeyboard(text_input_edit_text_add_float)
        }

        text_input_edit_text_business_date.setText(currentCalendar.timeInMillis.formatDate("dd/MM/yyyy"))
        text_input_edit_text_add_float.setText("0")

        button_change_business_date.click {
            showBusinessDatePicker()
        }

        button_cancel.click {
            dismiss()
        }

        button_continue.click {
            attemptDayStart()
        }
    }

    private fun showBusinessDatePicker() {
        context?.let { context ->
            val datePicker = DatePickerDialog.newInstance(
                { _, year, monthOfYear, dayOfMonth ->
                    currentCalendar.apply {
                        this.set(year, monthOfYear, dayOfMonth)
                    }
                    text_input_edit_text_business_date.setText(
                        currentCalendar.timeInMillis.formatDate(
                            "dd/MM/yyyy"
                        )
                    )
                },
                currentCalendar.get(Calendar.YEAR),
                currentCalendar.get(Calendar.MONTH),
                currentCalendar.get(Calendar.DAY_OF_MONTH)
            ).apply {
                this.version = DatePickerDialog.Version.VERSION_2
                this.minDate = currentCalendar
                this.maxDate = currentCalendar
                this.accentColor = context.findColor(R.color.colorPrimary)
                this.setCancelColor(Color.WHITE)
                this.setOkColor(Color.WHITE)
            }

            datePicker.show(childFragmentManager, "DatePickerDialog")
        }
    }

    private fun attemptDayStart() {
        when (val addFloat = text_input_edit_text_add_float.getDoubleOrNull()) {
            null -> {
                text_input_layout_add_float.isErrorEnabled = true
                text_input_layout_add_float.error =
                    getString(R.string.error_add_float_wrong_format)
                return
            }
            else -> {
                text_input_layout_add_float.isErrorEnabled = false

                (activity as? MainActivity)?.performDayStart(currentCalendar, addFloat)
                dismissAllowingStateLoss()
            }
        }
    }
}