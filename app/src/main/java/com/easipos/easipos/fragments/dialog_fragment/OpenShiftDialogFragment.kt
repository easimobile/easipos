package com.easipos.easipos.fragments.dialog_fragment

import android.view.ViewGroup
import com.easipos.easipos.R
import com.easipos.easipos.activities.day_end.DayEndActivity
import com.easipos.easipos.base.CustomBaseDialogFragment
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.showKeyboard
import io.github.anderscheow.library.kotlinExt.withActivity
import kotlinx.android.synthetic.main.dialog_fragment_open_shift.*
import org.jetbrains.anko.displayMetrics

class OpenShiftDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun newInstance(): OpenShiftDialogFragment {
            return OpenShiftDialogFragment()
        }
    }

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_open_shift

    override fun init() {
        super.init()

        text_input_edit_text_add_float.post {
            text_input_edit_text_add_float.requestFocus()
            context?.showKeyboard(text_input_edit_text_add_float)
        }

        button_cancel.click {
            dismiss()
        }

        button_continue.click {
            attemptDayStart()
        }
    }

    private fun attemptDayStart() {
        when (val addFloat = text_input_edit_text_add_float.getDoubleOrNull()) {
            null -> {
                text_input_layout_add_float.isErrorEnabled = true
                text_input_layout_add_float.error =
                    getString(R.string.error_add_float_wrong_format)
                return
            }
            else -> {
                text_input_layout_add_float.isErrorEnabled = false

                (activity as? DayEndActivity)?.performOpenShift(addFloat)
                dismissAllowingStateLoss()
            }
        }
    }
}