package com.easipos.easipos.fragments.dialog_fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.easipos.R
import com.easipos.easipos.activities.sales.SalesActivity
import com.easipos.easipos.adapters.ProductAdapter
import com.easipos.easipos.base.CustomBaseDialogFragment
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.tools.EqualSpacingItemDecoration
import com.easipos.easipos.use_cases.base.DefaultSingleObserver
import com.easipos.easipos.use_cases.sales.GetProductsBySearchUseCase
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.showKeyboard
import io.github.anderscheow.library.kotlinExt.withContext
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_fragment_search_product.*
import java.util.concurrent.TimeUnit

class SearchProductDialogFragment : CustomBaseDialogFragment(), ProductAdapter.OnGestureDetectedListener {

    companion object {
        fun newInstance(): SearchProductDialogFragment {
            return SearchProductDialogFragment()
        }
    }

    private val getProductsBySearchUseCase by lazy { GetProductsBySearchUseCase(kodein) }

    private val disposable = CompositeDisposable()
    private var adapter: ProductAdapter? = null

    override fun onActivityCreated(savedInstaceState: Bundle?) {
        super.onActivityCreated(savedInstaceState)
        dialog?.window?.attributes?.windowAnimations = R.style.DialogAnimation
    }

    override fun onDestroy() {
        getProductsBySearchUseCase.dispose()
        if (disposable.isDisposed.not()) {
            disposable.dispose()
        }
        super.onDestroy()
    }

    override fun getTheme(): Int {
        return R.style.FullScreenDialog
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_search_product

    override fun init() {
        super.init()

        text_input_edit_text_search.post {
            text_input_edit_text_search.requestFocus()
            activity?.showKeyboard(text_input_edit_text_search)
        }

        image_view_back.click {
            dismissAllowingStateLoss()
        }

        layout_root.click {
            dismiss()
        }

        setupRecyclerView()
        setupTextChangedObserver()
    }

    override fun onSelectItem(item: ProductCategoryPair) {
        (activity as? SalesActivity)?.addProductToCart(item)
    }

    override fun onLongSelectItem(item: ProductCategoryPair) {
    }

    private fun setupRecyclerView() {
        withContext { context ->
            adapter = ProductAdapter(context, this)
            recycler_view_product.apply {
                this.adapter = this@SearchProductDialogFragment.adapter
                this.layoutManager = LinearLayoutManager(context)
                this.addItemDecoration(EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.VERTICAL))
            }
        }
    }

    private fun setupTextChangedObserver() {
        val textChangeObservable = Observable.create<String> { emitter ->
            val textWatcher = object : TextWatcher {
                override fun afterTextChanged(s: Editable?) = Unit

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

                override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    emitter.onNext(s?.toString() ?: "")
                }
            }

            text_input_edit_text_search.addTextChangedListener(textWatcher)
        }

        disposable.add(
            textChangeObservable
                .debounce(200, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .toFlowable(BackpressureStrategy.BUFFER)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ searchable ->
                    if (searchable.isNotBlank()) {
                        searchProduct(searchable)
                    } else {
                        adapter?.items = mutableListOf()
                    }
                }, {
                    it.printStackTrace()
                })
        )
    }

    private fun searchProduct(searchable: String) {
        getProductsBySearchUseCase.execute(object : DefaultSingleObserver<List<ProductCategoryPair>>() {
            override fun onSuccess(value: List<ProductCategoryPair>) {
                adapter?.items = value.toMutableList()
            }
        }, GetProductsBySearchUseCase.Params.createQuery(searchable))
    }
}