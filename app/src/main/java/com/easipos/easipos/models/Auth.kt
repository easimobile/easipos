package com.easipos.easipos.models

data class Auth(val token: String)