package com.easipos.easipos.models

import androidx.lifecycle.MutableLiveData
import com.easipos.easipos.tools.Preference
import io.github.anderscheow.library.kotlinExt.mutation

class Cart {
    val products = arrayListOf<ProductCart>()
    var discount: Discount? = null
    var customerInfo: CustomerInfo? = null

    companion object {
        val cartLiveData = MutableLiveData<Cart>().apply {
            this.postValue(Cart())
        }

        fun insertProductToCart(productCategoryPair: ProductCategoryPair) {
            cartLiveData.mutation {
                it.value?.insertProduct(productCategoryPair)
            }
        }

        fun removeProductCart(position: Int) {
            cartLiveData.mutation {
                it.value?.removeProductCart(position)
            }
        }

        fun removeProduct(product: Product) {
            cartLiveData.mutation {
                it.value?.removeProduct(product)
            }
        }

        fun clearCart() {
            cartLiveData.mutation {
                it.value?.clearCart()
            }
        }

        fun changeProductCartQuantity(position: Int, quantity: Int) {
            cartLiveData.mutation {
                it.value?.changeProductCartQuantity(position, quantity)
            }
        }
    }

    fun hasProducts(): Boolean = products.isNotEmpty()

    fun insertProduct(product: ProductCategoryPair) {
        products.add(
            ProductCart(
                product = product,
                quantity = 1
            )
        )
    }

    fun removeProductCart(position: Int) {
        products.removeAt(position)
    }

    fun removeProduct(product: Product) {
        val filteredProducts = products.filter { it.product.product.productId != product.productId }

        products.clear()
        products.addAll(filteredProducts)
    }

    fun clearCart() {
        products.clear()
    }

    fun changeProductCartQuantity(position: Int, quantity: Int) {
        products.getOrNull(position)?.quantity = quantity
    }

    fun getSubtotalSellingPrice(): Double {
        if (hasProducts()) {
            // Use getTotalPrice() instead of getSubtotalPrice() since each ProductCart may have discount
            return products.map { it.getTotalSellingPrice() }.reduce { acc, d -> acc + d }
        }
        return 0.0
    }

    fun getTotalSellingPrice(): Double {
        return getSubtotalSellingPrice()
    }

    fun getSubtotalPrice(): Double {
        if (hasProducts()) {
            val subTotal = getTotalSellingPrice()
            return if (Preference.prefIsTaxExclusive) {
                 subTotal
            } else {
                subTotal - getTotalTaxAmount()
            }
        }
        return 0.0
    }

    fun getTotalPrice(): Double {
        // TODO: Use subtotal - discount amount
        if (hasProducts()) {
            return getSubtotalPrice() + getTotalTaxAmount()
        }
        return 0.0
    }

    fun getTotalTaxAmount(): Double {
        if (hasProducts()) {
            return products.map { it.getTaxAmount() }.reduce { acc, d -> acc + d }
        }
        return 0.0
    }

    fun getTotalQuantity(): Int {
        if (hasProducts()) {
            return products.map { it.quantity }.reduce { acc, i -> acc + i }
        }
        return 0
    }
}