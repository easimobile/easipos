package com.easipos.easipos.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CustomerInfo(
    val name: String,
    val phoneNumber: String,
    val email: String?,
    val address: String?
) : Parcelable