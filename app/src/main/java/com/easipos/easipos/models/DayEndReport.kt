package com.easipos.easipos.models

import com.easipos.easipos.constant.CashlessType
import com.easipos.easipos.constant.PaymentMethod
import com.easipos.easipos.tools.Preference
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.library.kotlinExt.formatDate
import java.util.*

data class DayEndReport(
    val businessDate: Long,
    val payments: List<Payment>,
    val totalFloat: Double,
    val numberOfShift: Int,
    val cashCollections: List<CashCollection>,
    val reportGeneratedDate: Calendar = Calendar.getInstance()
) {

    fun formatBusinessDate(): String {
        return businessDate.formatDate("dd/MM/yyyy")
    }

    fun formatDayEndDate(): String {
        return reportGeneratedDate.timeInMillis.formatDate("dd/MM/yyyy")
    }

    fun getPaymentsSize(): Int {
        return payments.size
    }

    fun getGrossSalesAmount(): String {
        if (payments.isEmpty()) return "0.00"

        return payments
            .map { it.getGrossAmount() }
            .reduce { acc, d -> acc + d }
            .formatAmount()
    }

    fun getNetSalesAmount(): String {
        if (payments.isEmpty()) return "0.00"

        return payments
            .map { it.getNetAmount() }
            .reduce { acc, d -> acc + d }
            .formatAmount()
    }

    fun getCashNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASH }
            .size
    }

    fun getCashlessNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS }
            .size
    }

    fun getGrabPayNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.GRAB_PAY }
            .size
    }

    fun getWechatPayNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.WECHAT_PAY }
            .size
    }

    fun getAlipayNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.ALIPAY }
            .size
    }

    fun getVoucherNumber(): Int {
        return payments
            .filter { it.voucher != null }
            .size
    }

    fun getCashAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASH }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getCashlessAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getGrabPayAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.GRAB_PAY }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getWechatPayAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.WECHAT_PAY }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getAlipayAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.ALIPAY }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getVoucherAmount(): String {
        return payments
            .filter { it.voucher != null }
            .takeIf { it.isNotEmpty() }
            ?.map { it.voucher!!.voucherAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun formatTotalFloatAmount(): String {
        return totalFloat.formatAmount()
    }

    fun getCashInDrawer(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASH }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.plus(totalFloat)
            ?.formatAmount() ?: totalFloat.formatAmount()
    }

    fun formatReportGenerateDate(): String {
        return reportGeneratedDate.timeInMillis.formatDate("dd/MM/yyyy HH:mm:ss")
    }

    fun formatReportGenerateDate2(): String {
        return reportGeneratedDate.timeInMillis.formatDate("dd-MM-yyyy HHmmss")
    }

    private fun getNetCashDeclaration(): Double {
        if (cashCollections.isEmpty()) return 0.0

        val totalCollect = cashCollections.map { it.getTotalCollect() }.reduce { acc, d -> acc + d }
        val totalDeclare = cashCollections.map { it.declare }.reduce { acc, d -> acc + d }

        return totalDeclare - totalCollect
    }

    fun formatNetCashDeclaration(): String {
        return getNetCashDeclaration().formatAmount()
    }

    fun getNetCashDeclarationStatus(): String {
        val net = getNetCashDeclaration()
        return when {
            net > 0.0 -> {
                "Excess"
            }
            net < 0.0 -> {
                "Shortage"
            }
            else -> {
                "Tally"
            }
        }
    }
}

data class CashCollection(
    val shift: Int,
    val collect: Double,
    val float: Double,
    val declare: Double
) {

    fun getTotalCollect(): Double {
        return if (Preference.prefFloatOnCashDeclaration) {
            collect + float
        } else {
            collect
        }
    }

    fun formatTotalCollect(): String {
        return getTotalCollect().formatAmount()
    }

    fun formatDeclare(): String {
        return declare.formatAmount()
    }

    fun formatDiff(): String {
        return (declare - getTotalCollect()).formatAmount()
    }
}