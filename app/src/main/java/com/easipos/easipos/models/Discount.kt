package com.easipos.easipos.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Discount(
    val discountType: String,
    val amount: Double
) : Parcelable