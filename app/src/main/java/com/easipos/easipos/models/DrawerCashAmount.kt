package com.easipos.easipos.models

import android.os.Parcelable
import com.easipos.easipos.tools.Preference
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DrawerCashAmount(
    val cashAmount: Double,
    val float: Double,
    val shift: Int
) : Parcelable {

    fun getTotalCashAmount(): Double {
        return if (Preference.prefFloatOnCashDeclaration) {
            cashAmount + float
        } else {
            float
        }
    }
}