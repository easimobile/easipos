package com.easipos.easipos.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.easipos.easipos.constant.CashlessType
import com.easipos.easipos.constant.PaymentMethod
import com.easipos.easipos.room.converters.*
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.util.gson
import com.google.gson.reflect.TypeToken
import io.github.anderscheow.library.kotlinExt.round
import kotlinx.android.parcel.Parcelize
import java.util.*

@Entity
@Parcelize
data class Payment(
    @PrimaryKey(autoGenerate = true)
    val paymentId: Long = 0,
    val products: String,
    @TypeConverters(DiscountConverter::class)
    val discount: Discount? = null,
    @TypeConverters(CustomerInfoConverter::class)
    val customerInfo: CustomerInfo? = null,
    @TypeConverters(TaxConverter::class)
    val tax: Tax? = null,
    val taxAmount: Double = 0.0,
    val subtotal: Double,
    val total: Double,
    @TypeConverters(PaymentMethodConverter::class)
    val paymentMethod: PaymentMethod,
    @TypeConverters(CashlessTypeConverter::class)
    val cashlessType: CashlessType?,
    val payableAmount: Double,
    val amountPaid: Double,
    @TypeConverters(VoucherConverter::class)
    val voucher: Voucher?,
    @TypeConverters(DateTimeConverter::class)
    val paymentDateTime: Calendar = Calendar.getInstance(),

    // for tracking purpose (day end)
    val userId: Long = Preference.prefUserId,
    val shift: Int = Preference.prefCurrentShift,
    val isDayEnded: Boolean = false
) : Parcelable {

    fun getProductCarts(): List<ProductCart> {
        val listType = object : TypeToken<List<ProductCart>>() {}.type
        return gson.fromJson(products, listType)
    }

    fun isAmountPaidLargerOrEqualTotalAmountWithTax(): Boolean {
        if (voucher != null) {
            return amountPaid + voucher.voucherAmount >= payableAmount
        }
        return amountPaid >= payableAmount
    }

    fun getGrossAmount(): Double {
        return total
    }

    fun getNetAmount(): Double {
        return payableAmount
    }

    fun getChange(): Double {
        return amountPaid - payableAmount
    }

    fun isChangeRequired(): Boolean = getChange() > 0
}