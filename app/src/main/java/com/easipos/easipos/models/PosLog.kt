package com.easipos.easipos.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PosLog(
    @PrimaryKey(autoGenerate = true)
    val logId: Long = 0L,
    val type: String,
    val shift: Int? = null,
    val amount: Double? = null,
    var status: String? = null
)