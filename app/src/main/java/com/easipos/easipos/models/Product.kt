package com.easipos.easipos.models

import android.content.Context
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.easipos.easipos.R
import com.easipos.easipos.util.getCurrency
import io.github.anderscheow.library.kotlinExt.formatAmount
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Product(
    @PrimaryKey(autoGenerate = true)
    val productId: Long = 0L,
    var productCode: String,
    var productName: String,
    var barcode: String,
    var capitalPrice: Double,
    var sellingPrice: Double,
    var availableStock: Long,
    var minimumStock: Long,
    var enableStockTracking: Boolean
) : Parcelable {

    companion object {
        fun getEmpty(): Product {
            return Product(0L, "", "", "",  0.0, 0.0, 0, 0, false)
        }
    }

    fun formatCapitalPrice(context: Context): String {
        return context.getString(R.string.label_amount_placeholder,
            getCurrency(), capitalPrice.formatAmount())
    }

    fun formatSellingPrice(context: Context): String {
        return context.getString(R.string.label_amount_placeholder,
            getCurrency(), sellingPrice.formatAmount())
    }
}