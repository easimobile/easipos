package com.easipos.easipos.models

import android.content.Context
import android.os.Parcelable
import com.easipos.easipos.R
import com.easipos.easipos.tools.Preference
import com.easipos.easipos.util.getCurrency
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.library.kotlinExt.round
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductCart(
    val product: ProductCategoryPair,
    var quantity: Int,
    var discount: Discount? = null
) : Parcelable {

    fun getSubtotalPrice(): Double {
        return product.product.sellingPrice * quantity
    }

    fun getTotalSellingPrice(): Double {
        // TODO: Use subtotal - discount amount
        return product.product.sellingPrice * quantity
    }

    fun getTaxAmount(): Double {
        return (getTotalSellingPrice() * (Preference.prefTaxAmount / 100)).round(2)
    }

    fun formatProductNameWithQuantity(): String {
        return "${product.product.productName} x $quantity"
    }

    fun formatPrice(context: Context): String {
        return product.product.formatSellingPrice(context)
    }

    fun formatTotalPrice(context: Context): String {
        return context.getString(R.string.label_amount_placeholder,
            getCurrency(), getTotalSellingPrice().formatAmount())
    }

    fun formatQuantityWithPrice(context: Context): String {
        return context.getString(R.string.label_quantity_price_placeholder,
            quantity, formatPrice(context))
    }
}