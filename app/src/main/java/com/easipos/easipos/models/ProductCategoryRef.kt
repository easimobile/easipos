package com.easipos.easipos.models

import androidx.room.Entity

@Entity(primaryKeys = ["productId", "categoryId"])
data class ProductCategoryRef(
    val productId: Long,
    val categoryId: Long
)