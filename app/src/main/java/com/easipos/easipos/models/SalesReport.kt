package com.easipos.easipos.models

import android.os.Parcelable
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.library.kotlinExt.formatDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class SalesReport(
    val fromDate: String,
    val toDate: String,
    val payments: List<Payment>,
    val reportGeneratedDate: Calendar = Calendar.getInstance()
) : Parcelable {

    val groupProducts: Map<Pair<String, String>, List<ProductCart>>
        get() = groupProductCart()

    fun formatFromDate(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(
                fromDate
            )
            date?.time.formatDate("dd/MM/yyyy")
        } catch (ex: Exception) {
            fromDate
        }
    }

    fun formatToDate(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(
                toDate
            )
            date?.time.formatDate("dd/MM/yyyy")
        } catch (ex: Exception) {
            toDate
        }
    }

    fun getProductSoldQuantity(pair: Pair<String, String>): Int {
        return groupProducts[pair]?.size ?: 0
    }

    fun getProductSoldTotalAmount(pair: Pair<String, String>): String {
        val products = groupProducts[pair]
        if (products != null && products.isNotEmpty()) {
            return products.map { it.getTotalSellingPrice() }
                .reduce { acc, d -> acc + d }
                .formatAmount()
        }
        return "0.00"
    }

    fun formatReportGenerateDate(): String {
        return reportGeneratedDate.timeInMillis.formatDate("dd/MM/yyyy HH:mm:ss")
    }

    fun formatReportGenerateDate2(): String {
        return reportGeneratedDate.timeInMillis.formatDate("dd-MM-yyyy HHmmss")
    }

    private fun groupProductCart(): Map<Pair<String, String>, List<ProductCart>> {
        val productCarts = arrayListOf<ProductCart>()
        payments.forEach { payment ->
            productCarts.addAll(payment.getProductCarts())
        }

        return productCarts.groupBy {
            val product = it.product.product
            Pair(product.productCode, product.productName)
        }
    }
}