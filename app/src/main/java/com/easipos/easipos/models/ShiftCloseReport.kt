package com.easipos.easipos.models

import com.easipos.easipos.constant.CashlessType
import com.easipos.easipos.constant.PaymentMethod
import io.github.anderscheow.library.kotlinExt.formatAmount
import io.github.anderscheow.library.kotlinExt.formatDate
import java.util.*

data class ShiftCloseReport(
    val businessDate: Long,
    val payments: List<Payment>,
    val float: Double,
    val shift: Int,
    val reportGeneratedDate: Calendar = Calendar.getInstance()
) {

    fun formatBusinessDate(): String {
        return businessDate.formatDate("dd/MM/yyyy")
    }

    fun getPaymentsSize(): Int {
        return payments.size
    }

    fun getGrossSalesAmount(): String {
        if (payments.isEmpty()) return "0.00"

        return payments
            .map { it.getGrossAmount() }
            .reduce { acc, d -> acc + d }
            .formatAmount()
    }

    fun getNetSalesAmount(): String {
        if (payments.isEmpty()) return "0.00"

        return payments
            .map { it.getNetAmount() }
            .reduce { acc, d -> acc + d }
            .formatAmount()
    }

    fun getCashNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASH }
            .size
    }

    fun getCashlessNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS }
            .size
    }

    fun getGrabPayNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.GRAB_PAY }
            .size
    }

    fun getWechatPayNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.WECHAT_PAY }
            .size
    }

    fun getAlipayNumber(): Int {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.ALIPAY }
            .size
    }

    fun getVoucherNumber(): Int {
        return payments
            .filter { it.voucher != null }
            .size
    }

    fun getCashAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASH }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getCashlessAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getGrabPayAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.GRAB_PAY }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getWechatPayAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.WECHAT_PAY }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getAlipayAmount(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASHLESS && it.cashlessType == CashlessType.ALIPAY }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun getVoucherAmount(): String {
        return payments
            .filter { it.voucher != null }
            .takeIf { it.isNotEmpty() }
            ?.map { it.voucher!!.voucherAmount }
            ?.reduce { acc, d -> acc + d }
            ?.formatAmount() ?: "0.00"
    }

    fun formatFloatAmount(): String {
        return float.formatAmount()
    }

    fun getCashInDrawer(): String {
        return payments
            .filter { it.paymentMethod == PaymentMethod.CASH }
            .takeIf { it.isNotEmpty() }
            ?.map { it.payableAmount }
            ?.reduce { acc, d -> acc + d }
            ?.plus(float)
            ?.formatAmount() ?: float.formatAmount()
    }

    fun formatReportGenerateDate(): String {
        return reportGeneratedDate.timeInMillis.formatDate("dd/MM/yyyy HH:mm:ss")
    }

    fun formatReportGenerateDate2(): String {
        return reportGeneratedDate.timeInMillis.formatDate("dd-MM-yyyy HHmmss")
    }
}