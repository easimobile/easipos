package com.easipos.easipos.models

import android.os.Parcelable
import com.easipos.easipos.tools.Preference
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tax(
    val taxType: String,
    val percent: Double
) : Parcelable {

    companion object {
        fun getTax(): Tax {
            return Tax(
                taxType = Preference.prefTaxType,
                percent = Preference.prefTaxAmount
            )
        }
    }
}