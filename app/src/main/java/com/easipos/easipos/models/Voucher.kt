package com.easipos.easipos.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Voucher(
    val voucherCode: String,
    val voucherAmount: Double
) : Parcelable {

    companion object {
        val voucher10 = Voucher("VOUCHER10", 10.0)
    }
}