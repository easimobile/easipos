package com.easipos.easipos.repositories.log

import com.easipos.easipos.datasource.DataFactory
import com.easipos.easipos.models.PosLog
import io.reactivex.Observable

class LogDataRepository(private val dataFactory: DataFactory) : LogRepository {

    override fun addLog(posLog: PosLog): Observable<Void> =
        dataFactory.createLogDataSource()
            .addLog(posLog)

    override fun addOpenShiftLog(shift: Int): Observable<Void> =
        dataFactory.createLogDataSource()
            .addOpenShiftLog(shift)

    override fun addCloseShiftLog(shift: Int): Observable<Void> =
        dataFactory.createLogDataSource()
            .addCloseShiftLog(shift)

    override fun clearLog(): Observable<Void> =
        dataFactory.createLogDataSource()
            .clearLog()
}
