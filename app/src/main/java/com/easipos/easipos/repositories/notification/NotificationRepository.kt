package com.easipos.easipos.repositories.notification

import com.easipos.easipos.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.easipos.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationRepository {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
