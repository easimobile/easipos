package com.easipos.easipos.repositories.precheck

import com.easipos.easipos.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easipos.datasource.DataFactory
import io.reactivex.Single

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
