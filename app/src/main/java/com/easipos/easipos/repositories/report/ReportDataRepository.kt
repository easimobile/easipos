package com.easipos.easipos.repositories.report

import com.easipos.easipos.datasource.DataFactory
import com.easipos.easipos.models.DayEndReport
import com.easipos.easipos.models.Payment
import com.easipos.easipos.models.SalesReport
import com.easipos.easipos.models.ShiftCloseReport
import io.reactivex.Single

class ReportDataRepository(private val dataFactory: DataFactory) : ReportRepository {

    override fun getShiftCloseReport(): Single<ShiftCloseReport> =
        dataFactory.createReportDataSource()
            .getShiftCloseReport()

    override fun getDayEndReport(): Single<DayEndReport> =
        dataFactory.createReportDataSource()
            .getDayEndReport()

    override fun getSalesReport(fromDate: String, toDate: String): Single<SalesReport> =
        dataFactory.createReportDataSource()
            .getSalesReport(fromDate, toDate)
}
