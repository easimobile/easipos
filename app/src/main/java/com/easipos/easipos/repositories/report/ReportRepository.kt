package com.easipos.easipos.repositories.report

import com.easipos.easipos.models.DayEndReport
import com.easipos.easipos.models.Payment
import com.easipos.easipos.models.SalesReport
import com.easipos.easipos.models.ShiftCloseReport
import io.reactivex.Single

interface ReportRepository {

    fun getShiftCloseReport(): Single<ShiftCloseReport>

    fun getDayEndReport(): Single<DayEndReport>

    fun getSalesReport(fromDate: String, toDate: String): Single<SalesReport>
}
