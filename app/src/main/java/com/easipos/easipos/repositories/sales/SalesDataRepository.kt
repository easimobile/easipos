package com.easipos.easipos.repositories.sales

import com.easipos.easipos.datasource.DataFactory
import com.easipos.easipos.models.*
import io.reactivex.Observable
import io.reactivex.Single

class SalesDataRepository(private val dataFactory: DataFactory) : SalesRepository {

    override fun addDefaultCategory(): Observable<Void> =
        dataFactory.createSalesDataSource()
            .addDefaultCategory()

    override fun addCategory(category: Category): Observable<Void> =
        dataFactory.createSalesDataSource()
            .addCategory(category)

    override fun getCategories(): Single<List<Category>> =
        dataFactory.createSalesDataSource()
            .getCategories()

    override fun addProduct(productCategoryPair: ProductCategoryPair): Observable<Void> =
        dataFactory.createSalesDataSource()
            .addProduct(productCategoryPair)

    override fun getProducts(): Single<List<ProductCategoryPair>> =
        dataFactory.createSalesDataSource()
            .getProducts()

    override fun getProductsByCategory(category: Category): Single<List<ProductCategoryPair>> =
        dataFactory.createSalesDataSource()
            .getProductsByCategory(category)

    override fun getProductsBySearch(searchable: String): Single<List<ProductCategoryPair>> =
        dataFactory.createSalesDataSource()
            .getProductsBySearch(searchable)

    override fun getProductByBarcode(barcode: String): Single<ProductCategoryPair> =
        dataFactory.createSalesDataSource()
            .getProductByBarcode(barcode)

    override fun updateProduct(product: Product): Observable<Void> =
        dataFactory.createSalesDataSource()
            .updateProduct(product)

    override fun updateProductCategory(productCategoryPair: ProductCategoryPair, category: Category): Observable<Void> =
        dataFactory.createSalesDataSource()
            .updateProductCategory(productCategoryPair, category)

    override fun updateProductsAvailableStock(productCarts: List<ProductCart>): Observable<Void> =
        dataFactory.createSalesDataSource()
            .updateProductsAvailableStock(productCarts)

    override fun deleteProduct(product: Product): Observable<Void> =
        dataFactory.createSalesDataSource()
            .deleteProduct(product)

    override fun addPayment(payment: Payment): Single<Long> =
        dataFactory.createSalesDataSource()
            .addPayment(payment)

    override fun getPayment(paymentId: Long): Single<Payment> =
        dataFactory.createSalesDataSource()
            .getPayment(paymentId)

    override fun getTodaySales(): Single<List<Payment>> =
        dataFactory.createSalesDataSource()
            .getTodaySales()

    override fun getTodaySalesByShift(): Single<List<Payment>> =
        dataFactory.createSalesDataSource()
            .getTodaySalesByShift()

    override fun getTotalDrawerCashAmountByShift(): Single<DrawerCashAmount> =
        dataFactory.createSalesDataSource()
            .getTotalDrawerCashAmountByShift()

    override fun updateTodaySalesDayEnded(): Observable<Void> =
        dataFactory.createSalesDataSource()
            .updateTodaySalesDayEnded()
}
