package com.easipos.easipos.repositories.user

import com.easipos.easipos.datasource.DataFactory
import com.easipos.easipos.models.User
import io.reactivex.Single

class UserDataRepository(private val dataFactory: DataFactory) : UserRepository {

    override fun login(username: String, password: String): Single<User> =
        dataFactory.createUserDataSource()
            .login(username, password)

    override fun register(user: User): Single<Long> =
        dataFactory.createUserDataSource()
            .register(user)
}
