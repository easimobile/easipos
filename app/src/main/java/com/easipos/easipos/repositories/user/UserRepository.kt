package com.easipos.easipos.repositories.user

import com.easipos.easipos.models.User
import io.reactivex.Single

interface UserRepository {

    fun login(username: String, password: String): Single<User>

    fun register(user: User): Single<Long>
}
