package com.easipos.easipos.room

import androidx.room.*
import com.easipos.easipos.models.Category

@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(category: Category): Long

    @Query("SELECT * FROM Category")
    fun findCategories(): List<Category>

    @Query("SELECT COUNT(*) FROM Category WHERE categoryId = 1")
    fun getDefaultCategoryCount(): Long

    @Delete
    fun deleteCategory(category: Category)
}