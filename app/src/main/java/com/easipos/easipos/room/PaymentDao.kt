package com.easipos.easipos.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.easipos.easipos.models.Payment

@Dao
interface PaymentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPayment(payment: Payment): Long

    @Query("SELECT * FROM Payment WHERE paymentId = :paymentId")
    fun findPaymentByPaymentId(paymentId: Long): Payment?

    @Query("SELECT * FROM Payment WHERE date(datetime(paymentDateTime / 1000 , 'unixepoch')) = date(:businessDate) AND isDayEnded = 0")
    fun findTodayPaymentsNotDayEnded(businessDate: String): List<Payment>

    @Query("SELECT * FROM Payment WHERE shift = :shift AND date(datetime(paymentDateTime / 1000 , 'unixepoch')) = date(:businessDate) AND isDayEnded = 0")
    fun findTodayPaymentsByShiftAndNotDayEnded(shift: Int, businessDate: String): List<Payment>

    @Query("SELECT SUM(payableAmount) FROM Payment WHERE paymentMethod = 'CASH' AND shift = :shift AND date(datetime(paymentDateTime / 1000 , 'unixepoch')) = date(:businessDate) AND isDayEnded = 0")
    fun findTodayPaymentsByShiftAndCashPaymentMethodAndNotDayEnded(shift: Int, businessDate: String): Double

    @Query("SELECT * FROM Payment WHERE date(datetime(paymentDateTime / 1000 , 'unixepoch')) BETWEEN date(:fromDate) AND date(:toDate)")
    fun findPaymentsByPaymentDateTimeRange(fromDate: String, toDate: String): List<Payment>

    @Query("UPDATE Payment SET isDayEnded = 1 WHERE date(datetime(paymentDateTime / 1000 , 'unixepoch')) = date(:businessDate) AND isDayEnded = 0")
    fun updatePaymentIsDayEnded(businessDate: String)
}