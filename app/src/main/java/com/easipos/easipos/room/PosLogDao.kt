package com.easipos.easipos.room

import androidx.room.*
import com.easipos.easipos.constant.PosLogType
import com.easipos.easipos.models.PosLog

@Dao
interface PosLogDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPosLog(posLog: PosLog): Long

    @Query("SELECT * FROM PosLog WHERE type = :type AND shift = :shift")
    fun findPosLogByShift(shift: Int, type: String = PosLogType.SHIFT.toString()): PosLog?

    @Query("SELECT * FROM PosLog WHERE type = :type AND shift = :shift")
    fun findPosLogByCashDeclarationTypeAndShift(shift: Int, type: String = PosLogType.CASH_DECLARATION.toString()): List<PosLog>

    @Query("SELECT * FROM PosLog WHERE type = :type")
    fun findPosLogByCashDeclarationType(type: String = PosLogType.CASH_DECLARATION.toString()): List<PosLog>

    @Query("SELECT SUM(amount) FROM PosLog WHERE type = :type")
    fun findTotalFloatByAddFloatType(type: String = PosLogType.ADD_FLOAT.toString()): Double

    @Query("SELECT COUNT(*) FROM PosLog WHERE type = :type")
    fun findTotalShiftByShiftType(type: String = PosLogType.SHIFT.toString()): Int

    @Query("SELECT amount FROM PosLog WHERE type = :type AND shift = :shift")
    fun findFloatAmountByShiftAndAddFloatType(shift: Int, type: String = PosLogType.ADD_FLOAT.toString()): Double

    @Update
    fun updatePosLog(posLog: PosLog)

    @Query("DELETE FROM PosLog")
    fun clearLog()
}