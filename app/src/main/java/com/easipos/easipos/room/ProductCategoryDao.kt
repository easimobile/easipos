package com.easipos.easipos.room

import androidx.room.*
import com.easipos.easipos.models.CategoryProductPair
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.models.ProductCategoryRef

@Dao
interface ProductCategoryDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(ref: ProductCategoryRef)

    @Transaction
    @Query("SELECT * FROM Product")
    fun findProducts(): List<ProductCategoryPair>

    @Transaction
    @Query("SELECT * FROM Product WHERE barcode = :barcode")
    fun findProductByBarcode(barcode: String): ProductCategoryPair?

    @Transaction
    @Query("SELECT * FROM Product WHERE productId IN (:productIds)")
    fun findProductByProductIds(productIds: List<Long>): List<ProductCategoryPair>

    @Transaction
    @Query("SELECT * FROM Product WHERE productName LIKE '%' || :searchable || '%'")
    fun findProductsBySearch(searchable: String): List<ProductCategoryPair>

    @Transaction
    @Query("SELECT * FROM Category WHERE categoryId = :categoryId")
    fun findProductsByCategory(categoryId: Long): CategoryProductPair?

    @Transaction
    @Query("UPDATE ProductCategoryRef SET categoryId = :toChangeCategoryId WHERE productId = :productId AND categoryId = :originalCategoryId")
    fun updateProductCategory(productId: Long, originalCategoryId: Long, toChangeCategoryId: Long)

    // Default category is categoryId = 1 which is General
    @Transaction
    @Query("UPDATE ProductCategoryRef SET categoryId = 1 WHERE categoryId = :categoryId")
    fun updateProductCategoryToDefault(categoryId: Long)

    @Transaction
    @Query("DELETE FROM ProductCategoryRef WHERE productId = :productId")
    fun removeProductById(productId: Long)
}