package com.easipos.easipos.room

import androidx.room.*
import com.easipos.easipos.models.Product

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(product: Product): Long

    @Query("SELECT * FROM Product WHERE productId = :productId")
    fun findProductByProductId(productId: Long): Product?

    @Query("SELECT * FROM Product WHERE productCode = :productCode")
    fun findProductByProductCode(productCode: String): Product?

    @Update
    fun updateProduct(product: Product)

    @Query("UPDATE PRODUCT SET availableStock = :newAvailableStock WHERE productId = :productId")
    fun updateProductAvailableStock(productId: Long, newAvailableStock: Long)

    @Delete
    fun deleteProduct(product: Product)
}