package com.easipos.easipos.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.easipos.easipos.models.*
import com.easipos.easipos.room.converters.*

@Database(
    entities = [
        User::class,
        Category::class,
        Product::class,
        ProductCategoryRef::class,
        Payment::class,
        PosLog::class,
        Notification::class
    ],
    version = 9,
    exportSchema = false
)
@TypeConverters(DiscountConverter::class, CustomerInfoConverter::class,
    PaymentMethodConverter::class, CashlessTypeConverter::class, VoucherConverter::class,
    TaxConverter::class, DateTimeConverter::class)
abstract class RoomService : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun categoryDao(): CategoryDao

    abstract fun productDao(): ProductDao

    abstract fun productCategoryDao(): ProductCategoryDao

    abstract fun paymentDao(): PaymentDao

    abstract fun posLogDao(): PosLogDao

    abstract fun notificationDao(): NotificationDao
}

val MIGRATION_2_3: Migration = object : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "ALTER TABLE Product " +
                    "ADD COLUMN productCode TEXT NOT NULL DEFAULT ''"
        )
    }
}

val MIGRATION_3_4: Migration = object : Migration(3, 4) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "CREATE TABLE Payment (" +
                    "paymentId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "products TEXT NOT NULL DEFAULT ''," +
                    "discount TEXT," +
                    "customerInfo TEXT," +
                    "subtotal REAL NOT NULL," +
                    "total REAL NOT NULL," +
                    "paymentMethod TEXT NOT NULL," +
                    "cashlessType TEXT," +
                    "payableAmount REAL NOT NULL," +
                    "amountPaid REAL NOT NULL," +
                    "voucher TEXT)"
        )
    }
}

val MIGRATION_4_5: Migration = object : Migration(4, 5) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "CREATE TABLE User (" +
                    "userId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "name TEXT NOT NULL," +
                    "username TEXT NOT NULL," +
                    "password TEXT NOT NULL," +
                    "shopName TEXT NOT NULL)"
        )

        database.execSQL(
            "ALTER TABLE Payment " +
                    "ADD COLUMN userId INTEGER NOT NULL DEFAULT -1"
        )
    }
}

val MIGRATION_5_6: Migration = object : Migration(5, 6) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "ALTER TABLE Payment " +
                    "ADD COLUMN tax TEXT"
        )

        database.execSQL(
            "ALTER TABLE Payment " +
                    "ADD COLUMN paymentDateTime INTEGER NOT NULL DEFAULT 0"
        )
    }
}

val MIGRATION_6_7: Migration = object : Migration(6, 7) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "CREATE TABLE PosLog (" +
                    "logId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "type TEXT NOT NULL," +
                    "shift INTEGER," +
                    "amount REAL," +
                    "status TEXT)"
        )

        database.execSQL(
            "ALTER TABLE Payment " +
                    "ADD COLUMN taxAmount REAL NOT NULL DEFAULT 0"
        )
    }
}

val MIGRATION_7_8: Migration = object : Migration(7, 8) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "ALTER TABLE Payment " +
                    "ADD COLUMN shift INTEGER NOT NULL DEFAULT 0"
        )
    }
}

val MIGRATION_8_9: Migration = object : Migration(8, 9) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "ALTER TABLE Payment " +
                    "ADD COLUMN isDayEnded INTEGER NOT NULL DEFAULT 0"
        )
    }
}
