package com.easipos.easipos.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.easipos.easipos.models.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

    @Query("SELECT * FROM User WHERE username = :username AND password = :password")
    fun findUser(username: String, password: String): User?

    @Query("SELECT * FROM User WHERE username = :username")
    fun findUser(username: String): User?
}