package com.easipos.easipos.room.converters

import androidx.room.TypeConverter
import com.easipos.easipos.constant.CashlessType
import com.easipos.easipos.constant.parseCashlessType

object CashlessTypeConverter {

    @TypeConverter
    @JvmStatic
    fun toString(cashlessType: CashlessType?): String? {
        if (cashlessType == null) return null

        return cashlessType.toString()
    }

    @TypeConverter
    @JvmStatic
    fun fromString(string: String?): CashlessType? {
        if (string == null) return null

        return parseCashlessType(string)
    }
}