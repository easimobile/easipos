package com.easipos.easipos.room.converters

import androidx.room.TypeConverter
import com.easipos.easipos.models.CustomerInfo
import com.easipos.easipos.util.gson
import com.google.gson.reflect.TypeToken

object CustomerInfoConverter {

    @TypeConverter
    @JvmStatic
    fun toString(customerInfo: CustomerInfo?): String? {
        if (customerInfo == null) return null

        return gson.toJson(customerInfo)
    }

    @TypeConverter
    @JvmStatic
    fun fromString(string: String?): CustomerInfo? {
        if (string == null) return null

        val listType = object : TypeToken<CustomerInfo>() {}.type
        return gson.fromJson(string, listType)
    }
}