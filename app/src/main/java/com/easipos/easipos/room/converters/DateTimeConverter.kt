package com.easipos.easipos.room.converters

import androidx.room.TypeConverter
import java.util.*

object DateTimeConverter {

    @TypeConverter
    @JvmStatic
    fun toLong(calendar: Calendar): Long {
        return calendar.timeInMillis
    }

    @TypeConverter
    @JvmStatic
    fun fromLong(long: Long): Calendar? {
       return Calendar.getInstance().apply {
           this.timeInMillis = long
       }
    }
}