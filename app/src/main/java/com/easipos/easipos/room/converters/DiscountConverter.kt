package com.easipos.easipos.room.converters

import androidx.room.TypeConverter
import com.easipos.easipos.models.Discount
import com.easipos.easipos.util.gson
import com.google.gson.reflect.TypeToken

object DiscountConverter {

    @TypeConverter
    @JvmStatic
    fun toString(discount: Discount?): String? {
        if (discount == null) return null

        return gson.toJson(discount)
    }

    @TypeConverter
    @JvmStatic
    fun fromString(string: String?): Discount? {
        if (string == null) return null

        val listType = object : TypeToken<Discount>() {}.type
        return gson.fromJson(string, listType)
    }
}