package com.easipos.easipos.room.converters

import androidx.room.TypeConverter
import com.easipos.easipos.constant.PaymentMethod
import com.easipos.easipos.constant.parsePaymentMethod

object PaymentMethodConverter {

    @TypeConverter
    @JvmStatic
    fun toString(paymentMethod: PaymentMethod?): String? {
        if (paymentMethod == null) return null

        return paymentMethod.toString()
    }

    @TypeConverter
    @JvmStatic
    fun fromString(string: String?): PaymentMethod? {
        if (string == null) return null

        return parsePaymentMethod(string)
    }
}