package com.easipos.easipos.room.converters

import androidx.room.TypeConverter
import com.easipos.easipos.models.Tax
import com.easipos.easipos.util.gson
import com.google.gson.reflect.TypeToken

object TaxConverter {

    @TypeConverter
    @JvmStatic
    fun toString(tax: Tax?): String? {
        if (tax == null) return null

        return gson.toJson(tax)
    }

    @TypeConverter
    @JvmStatic
    fun fromString(string: String?): Tax? {
        if (string == null) return null

        val listType = object : TypeToken<Tax>() {}.type
        return gson.fromJson(string, listType)
    }
}