package com.easipos.easipos.room.converters

import androidx.room.TypeConverter
import com.easipos.easipos.models.Voucher
import com.easipos.easipos.util.gson
import com.google.gson.reflect.TypeToken

object VoucherConverter {

    @TypeConverter
    @JvmStatic
    fun toString(voucher: Voucher?): String? {
        if (voucher == null) return null

        return gson.toJson(voucher)
    }

    @TypeConverter
    @JvmStatic
    fun fromString(string: String?): Voucher? {
        if (string == null) return null

        val listType = object : TypeToken<Voucher>() {}.type
        return gson.fromJson(string, listType)
    }
}