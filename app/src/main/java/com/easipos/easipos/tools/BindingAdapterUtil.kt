package com.easipos.easipos.tools

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easipos.adapters.CategoryAdapter
import com.easipos.easipos.adapters.ProductAdapter
import com.easipos.easipos.models.Category
import com.easipos.easipos.models.ProductCategoryPair

object BindingAdapterUtil {

    @BindingAdapter("populateProducts")
    @JvmStatic
    fun populateProducts(recyclerView: RecyclerView, products: List<ProductCategoryPair>?) {
        products?.let {
            (recyclerView.adapter as? ProductAdapter)?.items = products.toMutableList()
        }
    }

    @BindingAdapter("populateCategories")
    @JvmStatic
    fun populateCategories(recyclerView: RecyclerView, categories: List<Category>?) {
        categories?.let {
            (recyclerView.adapter as? CategoryAdapter)?.items = categories.toMutableList()
        }
    }
}