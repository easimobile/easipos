package com.easipos.easipos.tools

import com.easipos.easipos.constant.PosLogStatus
import com.pixplicity.easyprefs.library.Prefs

object Preference {

    private const val PREF_LANGUAGE_CODE = "PREF_LANGUAGE_CODE"
    private const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
    private const val PREF_IS_LOGGED_IN = "PREF_IS_LOGGED_IN"
    private const val PREF_IS_FCM_REGISTERED = "PREF_IS_FCM_REGISTERED"
    private const val PREF_FCM_TOKEN = "PREF_FCM_TOKEN"
    private const val PREF_NOTIFICATION_COUNT = "PREF_NOTIFICATION_COUNT"
    private const val PREF_USER_ID = "PREF_USER_ID"
    private const val PREF_CURRENCY = "PREF_CURRENCY"
    private const val PREF_TAX_TYPE = "PREF_TAX_TYPE"
    private const val PREF_TAX_AMOUNT = "PREF_TAX_AMOUNT"
    private const val PREF_DAY_START_BUSINESS_DATE = "PREF_DAY_START_BUSINESS_DATE"
    private const val PREF_CURRENT_FLOAT = "PREF_CURRENT_FLOAT"
    private const val PREF_CURRENT_SHIFT = "PREF_CURRENT_SHIFT"
    private const val PREF_CURRENT_SHIFT_STATUS = "PREF_CURRENT_SHIFT_STATUS"
    private const val PREF_IS_CASH_DECLARATION_DONE = "PREF_IS_CASH_DECLARATION_DONE"
    private const val PREF_IS_TAX_EXCLUSIVE = "PREF_IS_TAX_EXCLUSIVE"
    private const val PREF_RECEIPT_HEADER = "PREF_RECEIPT_HEADER"
    private const val PREF_FLOAT_ON_CASH_DECLARATION = "PREF_FLOAT_ON_CASH_DECLARATION"

    var prefLanguageCode: String
        get() = Prefs.getString(PREF_LANGUAGE_CODE, "en")
        set(languageCode) = Prefs.putString(PREF_LANGUAGE_CODE, languageCode)

    var prefAccessToken: String
        get() = Prefs.getString(PREF_ACCESS_TOKEN, "")
        set(accessToken) = Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    var prefIsLoggedIn: Boolean
        get() = Prefs.getBoolean(PREF_IS_LOGGED_IN, false)
        set(isLoggedIn) = Prefs.putBoolean(PREF_IS_LOGGED_IN, isLoggedIn)

    var prefIsFcmTokenRegistered: Boolean
        get() = Prefs.getBoolean(PREF_IS_FCM_REGISTERED, false)
        set(isRegistered) = Prefs.putBoolean(PREF_IS_FCM_REGISTERED, isRegistered)

    var prefFcmToken: String
        get() = Prefs.getString(PREF_FCM_TOKEN, "")
        set(token) = Prefs.putString(PREF_FCM_TOKEN, token)

    var prefNotificationCount: Int
        get() = Prefs.getInt(PREF_NOTIFICATION_COUNT, 0)
        set(count) = Prefs.putInt(PREF_NOTIFICATION_COUNT, count)

    var prefUserId: Long
        get() = Prefs.getLong(PREF_USER_ID, -1L)
        set(userId) = Prefs.putLong(PREF_USER_ID, userId)

    var prefCurrency: String
        get() = Prefs.getString(PREF_CURRENCY, "$")
        set(currency) = Prefs.putString(PREF_CURRENCY, currency)

    var prefTaxType: String
        get() = Prefs.getString(PREF_TAX_TYPE, "GST")
        set(taxType) = Prefs.putString(PREF_TAX_TYPE, taxType)

    var prefTaxAmount: Double
        get() = Prefs.getDouble(PREF_TAX_AMOUNT, 7.0)
        set(taxAmount) = Prefs.putDouble(PREF_TAX_AMOUNT, taxAmount)

    var prefDayStartBusinessDate: Long
        get() = Prefs.getLong(PREF_DAY_START_BUSINESS_DATE,  0L)
        set(date) = Prefs.putLong(PREF_DAY_START_BUSINESS_DATE, date)

    var prefCurrentFloat: Double
        get() = Prefs.getDouble(PREF_CURRENT_FLOAT,  0.0)
        set(currentFloat) = Prefs.putDouble(PREF_CURRENT_FLOAT, currentFloat)

    var prefCurrentShift: Int
        get() = Prefs.getInt(PREF_CURRENT_SHIFT,  0)
        set(currentShift) = Prefs.putInt(PREF_CURRENT_SHIFT, currentShift)

    var prefCurrentShiftStatus: String
        get() = Prefs.getString(PREF_CURRENT_SHIFT_STATUS, PosLogStatus.SHIFT_OPEN.string)
        set(status) = Prefs.putString(PREF_CURRENT_SHIFT_STATUS, status)

    var prefIsCashDeclarationDone: Boolean
        get() = Prefs.getBoolean(PREF_IS_CASH_DECLARATION_DONE, false)
        set(status) = Prefs.putBoolean(PREF_IS_CASH_DECLARATION_DONE, status)

    var prefIsTaxExclusive: Boolean
        get() = Prefs.getBoolean(PREF_IS_TAX_EXCLUSIVE, false)
        set(status) = Prefs.putBoolean(PREF_IS_TAX_EXCLUSIVE, status)

    var prefReceiptHeader: String
        get() = Prefs.getString(PREF_RECEIPT_HEADER, "")
        set(header) = Prefs.putString(PREF_RECEIPT_HEADER, header)

    var prefFloatOnCashDeclaration: Boolean
        get() = Prefs.getBoolean(PREF_FLOAT_ON_CASH_DECLARATION, false)
        set(status) = Prefs.putBoolean(PREF_FLOAT_ON_CASH_DECLARATION, status)

    fun logout() {
        prefIsLoggedIn = false
        prefIsFcmTokenRegistered = false
        prefNotificationCount = 0
        prefUserId = -1L

        Prefs.remove(PREF_ACCESS_TOKEN)
    }
}