package com.easipos.easipos.use_cases.base

import com.easipos.easipos.executor.PostExecutionThread
import com.easipos.easipos.executor.ThreadExecutor
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

abstract class BaseUseCase protected constructor(kodein: Kodein) {

    protected val threadExecutor: ThreadExecutor by kodein.instance()
    protected val postExecutionThread: PostExecutionThread by kodein.instance()

    private val disposables: CompositeDisposable = CompositeDisposable()

    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }

    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }
}
