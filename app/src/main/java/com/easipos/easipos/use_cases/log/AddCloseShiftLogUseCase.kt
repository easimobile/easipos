package com.easipos.easipos.use_cases.log

import com.easipos.easipos.repositories.log.LogRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class AddCloseShiftLogUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, AddCloseShiftLogUseCase.Params>(kodein) {

    private val repository: LogRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.addCloseShiftLog(params.shift)

    class Params private constructor(val shift: Int) {
        companion object {
            fun createQuery(shift: Int): Params {
                return Params(shift)
            }
        }
    }
}
