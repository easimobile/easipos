package com.easipos.easipos.use_cases.log

import com.easipos.easipos.models.PosLog
import com.easipos.easipos.repositories.log.LogRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class AddLogUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, AddLogUseCase.Params>(kodein) {

    private val repository: LogRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.addLog(params.posLog)

    class Params private constructor(val posLog: PosLog) {
        companion object {
            fun createQuery(posLog: PosLog): Params {
                return Params(posLog)
            }
        }
    }
}
