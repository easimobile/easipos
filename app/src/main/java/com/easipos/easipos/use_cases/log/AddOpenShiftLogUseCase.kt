package com.easipos.easipos.use_cases.log

import com.easipos.easipos.repositories.log.LogRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class AddOpenShiftLogUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, AddOpenShiftLogUseCase.Params>(kodein) {

    private val repository: LogRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.addOpenShiftLog(params.shift)

    class Params private constructor(val shift: Int) {
        companion object {
            fun createQuery(shift: Int): Params {
                return Params(shift)
            }
        }
    }
}
