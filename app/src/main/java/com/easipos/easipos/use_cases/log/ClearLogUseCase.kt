package com.easipos.easipos.use_cases.log

import com.easipos.easipos.repositories.log.LogRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class ClearLogUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, ClearLogUseCase.Params>(kodein) {

    private val repository: LogRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.clearLog()

    class Params private constructor() {
        companion object {
            fun createQuery(): Params {
                return Params()
            }
        }
    }
}
