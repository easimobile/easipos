package com.easipos.easipos.use_cases.notification

import com.easipos.easipos.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.easipos.repositories.notification.NotificationRepository
import com.easipos.easipos.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class RegisterFcmTokenUseCase(kodein: Kodein)
    : AbsRxCompletableUseCase<RegisterFcmTokenUseCase.Params>(kodein) {

    private val repository: NotificationRepository by kodein.instance()

    override fun createCompletable(params: Params): Completable =
        repository.registerFcmToken(params.model)

    class Params private constructor(val model: RegisterFcmTokenRequestModel) {
        companion object {
            fun createQuery(model: RegisterFcmTokenRequestModel): Params {
                return Params(model)
            }
        }
    }
}
