package com.easipos.easipos.use_cases.notification

import com.easipos.easipos.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.easipos.repositories.notification.NotificationRepository
import com.easipos.easipos.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class RemoveFcmTokenUseCase(kodein: Kodein)
    : AbsRxCompletableUseCase<RemoveFcmTokenUseCase.Params>(kodein) {

    private val repository: NotificationRepository by kodein.instance()

    override fun createCompletable(params: Params): Completable =
        repository.removeFcmToken(params.model)

    class Params private constructor(val model: RemoveFcmTokenRequestModel) {
        companion object {
            fun createQuery(model: RemoveFcmTokenRequestModel): Params {
                return Params(model)
            }
        }
    }
}
