package com.easipos.easipos.use_cases.precheck

import com.easipos.easipos.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easipos.repositories.precheck.PrecheckRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class CheckVersionUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<Boolean, CheckVersionUseCase.Params>(kodein) {

    private val repository: PrecheckRepository by kodein.instance()

    override fun createSingle(params: Params): Single<Boolean> =
        repository.checkVersion(params.model)

    class Params private constructor(val model: CheckVersionRequestModel) {
        companion object {
            fun createQuery(model: CheckVersionRequestModel): Params {
                return Params(model)
            }
        }
    }
}
