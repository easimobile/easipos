package com.easipos.easipos.use_cases.report

import com.easipos.easipos.models.Payment
import com.easipos.easipos.models.SalesReport
import com.easipos.easipos.repositories.report.ReportRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetSalesReportUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<SalesReport, GetSalesReportUseCase.Params>(kodein) {

    private val repository: ReportRepository by kodein.instance()

    override fun createSingle(params: Params): Single<SalesReport> =
        repository.getSalesReport(params.fromDate, params.toDate)

    class Params private constructor(val fromDate: String, val toDate: String) {
        companion object {
            fun createQuery(fromDate: String, toDate: String): Params {
                return Params(fromDate, toDate)
            }
        }
    }
}
