package com.easipos.easipos.use_cases.report

import com.easipos.easipos.models.ShiftCloseReport
import com.easipos.easipos.repositories.report.ReportRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetShiftCloseReportUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<ShiftCloseReport, GetShiftCloseReportUseCase.Params>(kodein) {

    private val repository: ReportRepository by kodein.instance()

    override fun createSingle(params: Params): Single<ShiftCloseReport> =
        repository.getShiftCloseReport()

    class Params private constructor() {
        companion object {
            fun createQuery(): Params {
                return Params()
            }
        }
    }
}
