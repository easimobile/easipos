package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.Category
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class AddCategoryUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, AddCategoryUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.addCategory(params.category)

    class Params private constructor(val category: Category) {
        companion object {
            fun createQuery(category: Category): Params {
                return Params(category)
            }
        }
    }
}
