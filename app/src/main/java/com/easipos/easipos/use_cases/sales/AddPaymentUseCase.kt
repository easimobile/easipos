package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.Payment
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class AddPaymentUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<Long, AddPaymentUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createSingle(params: Params): Single<Long> =
        repository.addPayment(params.payment)

    class Params private constructor(val payment: Payment) {
        companion object {
            fun createQuery(payment: Payment): Params {
                return Params(payment)
            }
        }
    }
}
