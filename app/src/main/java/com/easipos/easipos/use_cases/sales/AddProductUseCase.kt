package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class AddProductUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, AddProductUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.addProduct(params.productCategoryPair)

    class Params private constructor(val productCategoryPair: ProductCategoryPair) {
        companion object {
            fun createQuery(productCategoryPair: ProductCategoryPair): Params {
                return Params(productCategoryPair)
            }
        }
    }
}
