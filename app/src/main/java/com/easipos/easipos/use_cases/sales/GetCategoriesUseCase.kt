package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.Category
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetCategoriesUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<Category>, GetCategoriesUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createSingle(params: Params): Single<List<Category>> =
        repository.getCategories()

    class Params private constructor() {
        companion object {
            fun createQuery(): Params {
                return Params()
            }
        }
    }
}
