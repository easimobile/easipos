package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.Payment
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetPaymentByPaymentIdUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<Payment, GetPaymentByPaymentIdUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createSingle(params: Params): Single<Payment> =
        repository.getPayment(params.paymentId)

    class Params private constructor(val paymentId: Long) {
        companion object {
            fun createQuery(paymentId: Long): Params {
                return Params(paymentId)
            }
        }
    }
}
