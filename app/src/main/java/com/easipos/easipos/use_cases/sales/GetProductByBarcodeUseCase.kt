package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetProductByBarcodeUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<ProductCategoryPair, GetProductByBarcodeUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createSingle(params: Params): Single<ProductCategoryPair> =
        repository.getProductByBarcode(params.barcode)

    class Params private constructor(val barcode: String) {
        companion object {
            fun createQuery(barcode: String): Params {
                return Params(barcode)
            }
        }
    }
}
