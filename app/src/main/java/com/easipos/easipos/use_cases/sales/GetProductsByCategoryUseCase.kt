package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.Category
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetProductsByCategoryUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<ProductCategoryPair>, GetProductsByCategoryUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createSingle(params: Params): Single<List<ProductCategoryPair>> =
        repository.getProductsByCategory(params.category)

    class Params private constructor(val category: Category) {
        companion object {
            fun createQuery(category: Category): Params {
                return Params(category)
            }
        }
    }
}
