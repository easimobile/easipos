package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class GetProductsBySearchUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<List<ProductCategoryPair>, GetProductsBySearchUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createSingle(params: Params): Single<List<ProductCategoryPair>> =
        repository.getProductsBySearch(params.searchable)

    class Params private constructor(val searchable: String) {
        companion object {
            fun createQuery(searchable: String): Params {
                return Params(searchable)
            }
        }
    }
}
