package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.Category
import com.easipos.easipos.models.ProductCategoryPair
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class UpdateProductCategoryUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, UpdateProductCategoryUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.updateProductCategory(params.productCategoryPair, params.category)

    class Params private constructor(val productCategoryPair: ProductCategoryPair,
                                     val category: Category) {
        companion object {
            fun createQuery(productCategoryPair: ProductCategoryPair, category: Category): Params {
                return Params(productCategoryPair, category)
            }
        }
    }
}
