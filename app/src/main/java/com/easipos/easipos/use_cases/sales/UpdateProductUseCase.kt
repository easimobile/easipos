package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.Product
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class UpdateProductUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, UpdateProductUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.updateProduct(params.product)

    class Params private constructor(val product: Product) {
        companion object {
            fun createQuery(product: Product): Params {
                return Params(product)
            }
        }
    }
}
