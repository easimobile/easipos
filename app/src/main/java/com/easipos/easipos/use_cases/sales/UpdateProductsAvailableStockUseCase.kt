package com.easipos.easipos.use_cases.sales

import com.easipos.easipos.models.ProductCart
import com.easipos.easipos.repositories.sales.SalesRepository
import com.easipos.easipos.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class UpdateProductsAvailableStockUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, UpdateProductsAvailableStockUseCase.Params>(kodein) {

    private val repository: SalesRepository by kodein.instance()

    override fun createObservable(params: Params): Observable<Void> =
        repository.updateProductsAvailableStock(params.productCarts)

    class Params private constructor(val productCarts: List<ProductCart>) {
        companion object {
            fun createQuery(productCarts: List<ProductCart>): Params {
                return Params(productCarts)
            }
        }
    }
}
