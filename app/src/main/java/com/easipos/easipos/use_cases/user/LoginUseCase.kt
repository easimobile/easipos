package com.easipos.easipos.use_cases.user

import com.easipos.easipos.models.User
import com.easipos.easipos.repositories.user.UserRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class LoginUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<User, LoginUseCase.Params>(kodein) {

    private val repository: UserRepository by kodein.instance()

    override fun createSingle(params: Params): Single<User> =
        repository.login(params.username, params.password)

    class Params private constructor(val username: String, val password: String) {
        companion object {
            fun createQuery(username: String, password: String): Params {
                return Params(username, password)
            }
        }
    }
}
