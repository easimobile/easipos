package com.easipos.easipos.use_cases.user

import com.easipos.easipos.models.User
import com.easipos.easipos.repositories.user.UserRepository
import com.easipos.easipos.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class RegisterUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<Long, RegisterUseCase.Params>(kodein) {

    private val repository: UserRepository by kodein.instance()

    override fun createSingle(params: Params): Single<Long> =
        repository.register(params.user)

    class Params private constructor(val user: User) {
        companion object {
            fun createQuery(user: User): Params {
                return Params(user)
            }
        }
    }
}
