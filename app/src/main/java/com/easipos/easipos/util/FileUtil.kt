package com.easipos.easipos.util

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import androidx.core.net.toUri
import com.easipos.easipos.BuildConfig
import de.siegmar.fastcsv.writer.CsvWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets

fun Context.saveBitmap(fileName: String, mimeType: String, bitmap: Bitmap): Uri? {
    var savedUri: Uri? = null
    val fos: OutputStream?
    fos = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        val resolver = contentResolver
        val contentValues = ContentValues()
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, mimeType)
        contentValues.put(
            MediaStore.MediaColumns.RELATIVE_PATH,
            "Download/"
        )
        resolver.insert(MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL), contentValues)?.let { uri ->
            savedUri = uri
            resolver.openOutputStream(uri)
        }
    } else {
        val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
        val file = File(dir, fileName)

        // Create directory if not exist
        File(dir).apply {
            if (this.exists().not()) {
                this.mkdirs()
            }
        }

        // Create file if not exist otherwise delete
        if (file.exists().not()) {
            file.createNewFile()
        } else {
            file.delete()
        }

        savedUri = file.toUri()

        FileOutputStream(file)
    }

    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)

    fos?.flush()
    fos?.close()

    return savedUri?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            it
        } else {
            FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", it.toFile())
        }
    } ?: run {
        null
    }
}

fun Context.saveCsv(fileName: String, csvWriter: CsvWriter, data: List<Array<String>>): Uri? {
//    val uri: Uri? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//        val resolver = contentResolver
//        val contentValues = ContentValues()
//        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
//        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "text/csv")
//        contentValues.put(
//            MediaStore.MediaColumns.RELATIVE_PATH,
//            "Download/"
//        )
//        resolver.insert(
//            MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL),
//            contentValues
//        )?.apply {
//            resolver.openOutputStream(this)
//        }
//    } else {
//        val dir =
//            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//                .toString()
//        File(dir, fileName).apply {
//            // Create directory if not exist
//            File(dir).apply {
//                if (this.exists().not()) {
//                    this.mkdirs()
//                }
//            }
//
//            // Create file if not exist otherwise delete
//            if (this.exists().not()) {
//                this.createNewFile()
//            } else {
//                this.delete()
//            }
//        }.toUri()
//    }

    val dir =
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            .toString()
    val file = File(dir, fileName).apply {
        // Create directory if not exist
        File(dir).apply {
            if (this.exists().not()) {
                this.mkdirs()
            }
        }

        // Create file if not exist otherwise delete
        if (this.exists().not()) {
            this.createNewFile()
        } else {
            this.delete()
        }
    }

    val uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        file.toUri()
    } else {
        FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file)
    }

    csvWriter.write(uri.toFile(), StandardCharsets.UTF_8, data)
    return uri
}