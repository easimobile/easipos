package com.easipos.easipos.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.PopupWindow
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.easipos.easipos.Easi
import com.easipos.easipos.R
import com.easipos.easipos.managers.PushNotificationManager
import com.easipos.easipos.managers.PushNotificationManager.Companion.FCM_BODY
import com.easipos.easipos.managers.PushNotificationManager.Companion.FCM_TITLE
import com.easipos.easipos.tools.Preference
import com.google.gson.Gson
import com.tapadoo.alerter.Alerter
import io.github.anderscheow.library.kotlinExt.findColor
import org.json.JSONObject
import java.math.BigInteger
import java.security.MessageDigest
import java.util.*
import kotlin.math.round

val gson = Gson()

val languages = mapOf("zh" to "中文", "en" to "English")

fun defaultRequestOption(): RequestOptions {
    return RequestOptions()
}

fun ImageView.loadImage(imageUrl: String) {
    Glide.with(this.context)
            .load(imageUrl)
            .apply(defaultRequestOption())
            .transition(DrawableTransitionOptions().crossFade(500))
            .into(this)
}

fun changeLanguage(activity: Activity, language: String) {
    if (Preference.prefLanguageCode != language) {
        Preference.prefLanguageCode = language
        Easi.localeManager.setNewLocale(activity, language)
        activity.recreate()
    }
}

fun showAlerter(easi: Easi, pushNotificationManager: PushNotificationManager, jsonObject: JSONObject) {
    jsonObject.takeIf {
        jsonObject.has(FCM_TITLE) && jsonObject.has(FCM_BODY)
    }.run {
        this?.let { data ->
            easi.currentActivity?.let { activity ->
                Alerter.create(activity)
                    .setTitle(data.getString(FCM_TITLE) ?: "")
                    .setText(data.getString(FCM_BODY) ?: "")
                    .setBackgroundColorInt(activity.findColor(R.color.colorPrimary))
                    .setDuration(5000)
                    .setIconColorFilter(Color.WHITE)
                    .enableSwipeToDismiss()
                    .enableVibration(true)
                    .setOnClickListener(View.OnClickListener {
                        Alerter.hide()

                        pushNotificationManager.openNotification(easi, jsonObject, true)
                    })
                    //.setOnShowListener { Tracker.trackScreen(this@CustomLifecycleActivity, Screen.IN_APP_NOTIFICATION) }
                    .show()
            }
        }
    }
}

fun getCurrency(): String = Preference.prefCurrency